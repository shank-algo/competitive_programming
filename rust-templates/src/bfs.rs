#![allow(dead_code)]

use std::collections::{HashMap, HashSet, VecDeque};

type Map = HashMap<u64, HashSet<u64>>;

fn bfs(map: &Map, start: u64) -> HashMap<u64, u64> {
    let mut result = HashMap::new();
    for k in map.keys() {
        result.insert(*k, std::u64::MAX);
    }
    *result.get_mut(&start).unwrap() = 0;

    // (node, distance at which node was found)
    let mut to_visit = VecDeque::<(u64, u64)>::new();
    to_visit.push_back((start, 0));

    while !to_visit.is_empty() {
        let (from, mut distance) = to_visit.pop_front().unwrap();
        distance += 1;

        for to in &map[&from] {
            // skip nodes that have already been visited
            if result[&to] < std::u64::MAX {
                continue;
            }

            *result.get_mut(&to).unwrap() = distance;
            to_visit.push_back((*to, distance));
        }
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bfs() {
        let mut m = HashMap::new();
        let n1: HashSet<u64> = vec![2, 3].iter().cloned().collect();
        let n2: HashSet<u64> = vec![1].iter().cloned().collect();
        let n3: HashSet<u64> = vec![1, 4].iter().cloned().collect();
        let n4: HashSet<u64> = vec![3].iter().cloned().collect();
        m.insert(1, n1);
        m.insert(2, n2);
        m.insert(3, n3);
        m.insert(4, n4);

        let result = bfs(&m, 1);
        assert_eq!(result[&1], 0);
        assert_eq!(result[&2], 1);
        assert_eq!(result[&3], 1);
        assert_eq!(result[&4], 2);
        let result = bfs(&m, 4);
        assert_eq!(result[&1], 2);
        assert_eq!(result[&2], 3);
        assert_eq!(result[&3], 1);
        assert_eq!(result[&4], 0);
    }
}
