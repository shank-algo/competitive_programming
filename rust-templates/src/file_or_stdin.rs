use std::fs;
use std::io::{self, BufRead, Read};
use std::path::Path;

/// Wraps around the possible input sources (a file and the stdin)
#[allow(dead_code)]
enum Input<'a> {
    File(io::BufReader<fs::File>),
    Stdin(io::StdinLock<'a>),
}

impl<'a> Input<'a> {
    #[allow(dead_code)]
    fn from_file<P: AsRef<Path>>(path: P) -> Self {
        let path = path.as_ref();
        let file = fs::File::open(path).unwrap();
        let reader = io::BufReader::new(file);
        Self::File(reader)
    }
}

impl<'a> Read for Input<'a> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        match self {
            Input::File(reader) => reader.read(buf),
            Input::Stdin(guard) => guard.read(buf),
        }
    }
}

impl<'a> BufRead for Input<'a> {
    fn fill_buf(&mut self) -> io::Result<&[u8]> {
        match self {
            Input::File(reader) => reader.fill_buf(),
            Input::Stdin(guard) => guard.fill_buf(),
        }
    }
    fn consume(&mut self, amt: usize) {
        match self {
            Input::File(reader) => reader.consume(amt),
            Input::Stdin(guard) => guard.consume(amt),
        }
    }
}
