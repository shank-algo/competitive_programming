#![allow(dead_code)]

/// Depth-First Traversal

const MAX: usize = std::usize::MAX;

#[derive(Debug)]
struct DFS {
    tree: Vec<Vec<usize>>,
    parent: Vec<usize>,
    depth: Vec<usize>,
    tin: Vec<usize>,
    tout: Vec<usize>,
    t: usize,
}

impl DFS {
    fn new(tree: Vec<Vec<usize>>) -> Self {
        let n = tree.len();
        Self {
            tree,
            parent: vec![MAX; n],
            depth: vec![MAX; n],
            tin: vec![MAX; n],
            tout: vec![MAX; n],
            t: 0,
        }
    }

    fn dfs(&mut self) {
        self.dfs_helper(0, 0, 0);
    }

    fn dfs_helper(&mut self, curr: usize, par: usize, dep: usize) {
        self.parent[curr] = par;
        self.depth[curr] = dep;
        self.tin[curr] = self.t;
        self.t += 1;
        for &next in &self.tree[curr].clone() {
            if next == par {
                continue;
            }
            self.dfs_helper(next, curr, dep + 1);
        }
        self.tout[curr] = self.t;
        self.t += 1;
    }
}

#[test]
fn test_dfs_tree() {
    let mut tree = vec![vec![]; 3];
    tree[0].push(1);
    tree[0].push(2);

    dbg!(&tree);

    let mut sol = DFS::new(tree);
    sol.dfs();

    dbg!(&sol);
}
