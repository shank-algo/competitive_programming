#![allow(dead_code)]

use std::collections::HashMap;
use std::hash::Hash;

fn fib_rec(arg: u32) -> u32 {
    match arg {
        0 => 0,
        1 => 1,
        n => fib_rec(n - 1) + fib_rec(n - 2),
    }
}

fn fib_memo(cache: &mut HashMap<u32, u32>, arg: u32) -> u32 {
    match cache.get(&arg) {
        Some(val) => *val,
        None => {
            let result = match arg {
                0 => 0,
                1 => 1,
                _ => fib_memo(cache, arg - 1) + fib_memo(cache, arg - 2),
            };
            cache.insert(arg, result);
            result
        }
    }
}

fn memoize<F, A, R>(cache: &mut HashMap<A, R>, func: F, arg: A) -> R
where
    A: Hash + Eq + Clone,
    R: Clone,
    F: Fn(&mut HashMap<A, R>, A) -> R,
{
    match cache.get(&arg).map(|x| x.clone()) {
        Some(val) => val,
        None => {
            let result = func(cache, arg.clone());
            cache.insert(arg, result.clone());
            result
        }
    }
}

fn fib_memo2(cache: &mut HashMap<u32, u32>, arg: u32) -> u32 {
    match arg {
        0 => 0,
        1 => 1,
        _ => {
            let result = memoize(cache, fib_memo2, arg - 1) + memoize(cache, fib_memo2, arg - 2);
            cache.insert(arg, result);
            result
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_fib_native() {
        assert_eq!(fib_rec(0), 0);
        assert_eq!(fib_rec(1), 1);
        assert_eq!(fib_rec(2), 1);
        assert_eq!(fib_rec(3), 2);
        assert_eq!(fib_rec(4), 3);
    }
    #[test]
    fn test_fib_memo() {
        assert_eq!(fib_memo(&mut HashMap::new(), 1), 1);
        assert_eq!(fib_memo(&mut HashMap::new(), 2), 1);
        assert_eq!(fib_memo(&mut HashMap::new(), 3), 2);
        assert_eq!(fib_memo(&mut HashMap::new(), 4), 3);
        assert_eq!(fib_memo(&mut HashMap::new(), 0), 0);
    }
    #[test]
    fn test_memoize_with_cache() {
        let cache = &mut HashMap::new();
        assert_eq!(memoize(cache, fib_memo2, 1), 1);
        assert_eq!(memoize(cache, fib_memo2, 2), 1);
        assert_eq!(memoize(cache, fib_memo2, 3), 2);
        assert_eq!(memoize(cache, fib_memo2, 4), 3);
        assert_eq!(memoize(cache, fib_memo2, 0), 0);
    }
}
