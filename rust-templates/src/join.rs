#[allow(dead_code)]
fn join<S, T, I>(v: I, sep: S) -> String
where
    T: ToString,
    S: ToString,
    I: IntoIterator<Item = T>,
{
    let iterator = v.into_iter();
    let sep = sep.to_string();
    iterator.fold(String::new(), |mut acc, item| {
        if acc.len() > 0 {
            acc.push_str(sep.as_str());
        }
        acc.push_str(item.to_string().as_str());
        acc
    })
}

trait Join {
    fn join<S: ToString>(self, sep: S) -> String;
}

impl<'a, T, I> Join for I
where
    T: ToString + 'a,
    I: Iterator<Item = &'a T>,
{
    fn join<S: ToString>(self, sep: S) -> String {
        let sep = sep.to_string();
        self.fold(String::new(), |mut acc, item| {
            if acc.len() > 0 {
                acc.push_str(sep.as_str());
            };
            acc.push_str(item.to_string().as_str());
            acc
        })
    }
}

// #[cfg(test)]
// mod tests {
//     use super::*;

//     #[test]
//     fn join_trait() {
//         let a = vec![1, 2, 3];
//         let expected = "1, 2, 3";
//         assert_eq!(a.iter().join(", "), expected);
//     }

//     #[test]
//     fn join() {
//         let a: Vec<i32> = vec![1, 2, 3];
//         let expected = String::from("1, 2, 3");
//         assert_eq!(join(a, ", ".to_string()), expected);
//     }
// }
