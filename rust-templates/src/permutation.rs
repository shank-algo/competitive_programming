use std::collections::HashSet;

/// Generate all permutations of 1..N, inclusive
pub fn permutation_rec_hashmap(n: i32) -> Vec<String> {
    perm_helper(n)
        .into_iter()
        .map(|v| v.iter().map(|&i| i.to_string()).collect::<String>())
        .collect()
}

fn perm_helper(n: i32) -> HashSet<Vec<i32>> {
    if n == 1 {
        return [vec![1]].iter().cloned().collect();
    }

    let mut result = HashSet::new();
    for perm in perm_helper(n - 1) {
        for i in 0..n - 1 {
            let mut perm = perm.clone();
            perm.insert(i as usize, n);
            result.insert(perm);
        }
    }
    result
}
