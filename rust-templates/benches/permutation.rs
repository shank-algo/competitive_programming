#![allow(unused_variables)]

fn main() {
    use criterion::{black_box, criterion_group, criterion_main, Criterion};
    use rust_templates::permutation::permutation_rec_hashmap;

    pub fn criterion_benchmark(c: &mut Criterion) {
        c.bench_function("permutation_rec_hashmap", |b| {
            b.iter(|| permutation_rec_hashmap(10))
        });
    }

    criterion_group!(benches, criterion_benchmark);
    criterion_main!(benches);
}
