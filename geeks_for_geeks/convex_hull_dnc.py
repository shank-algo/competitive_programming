import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================

from collections import namedtuple

Point = namedtuple("Point", ["x", "y"])


# [Point] -> ([Point], [Point])
def divide(points):
    mid_x = sum(p.x for p in points) / len(points)
    left = [p for p in points if p.x <= mid_x]
    right = [p for p in points if p.x > mid_x]
    return left, right


# [Point], [Point] -> [Point]
def merge(left_hull, right_hull):
    pass


from math import atan2, pi


# find the angle that a point makes
# Point -> float
def angle(point):
    ang = atan2(point.y, point.x)
    if ang < 0: ang += 2 * pi
    return ang


# [Point] -> Point
def centroid(points):
    n = len(points)
    mx = sum(p.x for p in points) / n
    my = sum(p.y for p in points) / n
    return Point(mx, my)


# [Point] -> [Point]
def sort_counter_clockwise(points):
    c = centroid(points)
    shifted = [Point(p.x - c.x, p.y - c.y) for p in points]
    shifted.sort(key=angle)
    unshifted = [Point(p.x + c.x, p.y + c.y) for p in shifted]
    return unshifted


def test_sort_counter_clockwise():
    print("test_sort_counter_clockwise...", end=" ")
    points = [Point(1, 1), Point(1, 2), Point(2, 0), Point(2, 3), Point(3, 1)]
    sorted_points = sort_counter_clockwise(points)
    correct = [
        Point(x=2.0, y=3.0),
        Point(x=1.0, y=2.0),
        Point(x=1.0, y=1.0),
        Point(x=2.0, y=0.0),
        Point(x=3.0, y=1.0)
    ]
    assert sorted_points == correct
    print("pass")


test_sort_counter_clockwise()


# do the points lie on the same side of p1-p2 edge
# [Point], Point, Point -> bool
def same_side(points, p1, p2):
    if p1.x == p2.x:
        sides = set()
        for p in points:
            if p == p1 or p == p2: continue
            elif p.x < p1.x: sides.add("left")
            elif p.x == p1.x: sides.add("collinear")
            elif p.x > p1.x: sides.add("right")

            if len(sides) > 1: return False
        return True

    else:
        # find the slope of p1-p2
        slope = (p2.y - p1.y) / (p2.x - p1.x)
        sides = set()
        for p in points:
            if p == p1 or p == p2: continue
            y_exp = p1.y + slope * (p.x - p1.x)
            if y_exp > p.y: sides.add("below")
            elif y_exp == p.y: sides.add("collinear")
            elif y_exp < p.y: sides.add("above")

            if len(sides) > 1: return False
        return True


def test_same_side():
    print("test_same_side...", end=" ")
    points = [Point(1, 1), Point(1, 2), Point(2, 2), Point(2, 3), Point(3, 1)]
    # assert same_side(points, Point(1, 1), Point(1, 2)) == True
    assert same_side(points, Point(1, 2), Point(2, 3)) == True
    assert same_side(points, Point(1, 1), Point(2, 2)) == False
    assert same_side(points, Point(2, 3), Point(3, 1)) == True
    assert same_side(points, Point(3, 1), Point(2, 2)) == False
    assert same_side(points, Point(3, 1), Point(1, 1)) == True
    assert same_side(points, Point(1, 1), Point(1, 2)) == True
    print("pass")


test_same_side()


# [Point] -> [Point]
def brute_convex_hull(points):
    result = set()
    for i in range(len(points) - 1):
        for j in range(i + 1, len(points)):
            # if all the points lie on the same side of the edge i-j,
            # then the edge i-j lies on the convex hull
            hull_edge = same_side(points, points[i], points[j])
            if hull_edge:
                result.add(points[i])
                result.add(points[j])
    result = sort_counter_clockwise(result)
    return result


# test
def test_brute_convex_hull():
    print("test_brute_convex_hull...", end=" ")
    points = [Point(1, 1), Point(1, 2), Point(2, 2), Point(2, 3), Point(3, 1)]
    result = brute_convex_hull(points)
    correct_hull = [Point(1, 1), Point(1, 2), Point(2, 3), Point(3, 1)]
    assert all(p in correct_hull for p in result)
    assert len(correct_hull) == len(result)
    print("pass")


test_brute_convex_hull()


# [Point] -> [Point]
def convex_hull(points):
    if len(points) <= 5: return brute_convex_hull(points)
    left, right = divide(points)
    left_hull = convex_hull(left)
    right_hull = convex_hull(right)
    result = merge(left_hull, right_hull)
    return result


def main():
    # correct answer:
    # convex hull:
    # -5 -3
    # -1 -5
    # 1 -4
    # 0 0
    # -1 1

    pass


main()

# ==============================================================

if debug_mode:
    inf.close()
