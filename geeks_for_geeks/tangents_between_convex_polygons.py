from math import atan2, pi
import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================

# COMMON TYPES
# point: [x: int, y: int]

ABOVE, INLINE, BELOW = 1, 0, -1


# a: List[point], b: List[point] -> (point, point)
def upper_tangent(a, b):
    a = counter_clockwise(a)
    b = counter_clockwise(b)

    a, b = left_right(a, b)

    # inda -> index of right most point of a
    (inda, _point) = max(enumerate(a), key=lambda x: x[1][0])
    # indb -> index of left most point of b
    (indb, _point) = min(enumerate(b), key=lambda x: x[1][0])

    # while line inda-indb crosses b, move indb to next pt (clockwise)
    # while line inda-indb crosses a, move inda to next pt (counter-clockwise)
    na, nb = len(a), len(b)
    done = False
    while not done:
        done = True
        while relative_position(a[inda], b[indb], b[(indb - 1) % nb]) > 0:
            indb = (indb - 1) % nb
        while relative_position(a[inda], b[indb], a[(inda + 1) % na]) > 0:
            inda = (inda + 1) % na
            done = False

    return a[inda], b[indb]


# quadrant in which the point lies
# point -> 1 | 2 | 3 | 4
def quad(point):
    [x, y] = point
    if x >= 0 and y >= 0:
        return 1
    if x <= 0 and y <= 0:
        return 2
    if x <= 0 and y <= 0:
        return 3
    if x >= 0 and y <= 0:
        return 4


# centroid of the polygon
# List[point] -> point
def centroid(polygon):
    x, y, n = 0, 0, len(polygon)
    for point in polygon:
        x += point[0]
        y += point[1]
    x, y = x / n, y / n
    return [x, y]


# angle of a point from origin
# point -> float
def angle(point):
    [x, y] = point
    ang = atan2(y, x)
    if ang < 0: ang += 2 * pi
    return ang


# arrange the point on a and b in counter-clockwise order
# List[point] -> List[point]
def counter_clockwise(polygon):
    # shift origin to centroid
    # sort the points as per their position (coutner-clockwise)
    [cx, cy] = centroid(polygon)
    points = [[x - cx, y - cy] for [x, y] in polygon]
    points.sort(key=angle)
    # shift the origin back to 0,0
    points = [[int(x + cx), int(y + cy)] for [x, y] in points]
    return points


def test_counter_clockwise():
    print("test counter_clockwise()... ", end="")
    a = [[2, 2], [3, 1], [3, 3], [5, 2], [4, 0]]
    b = [[0, 1], [1, 0], [0, -2], [-1, 0]]

    cc_a = counter_clockwise(a)
    cc_b = counter_clockwise(b)

    assert cc_a == [[5, 2], [3, 3], [2, 2], [3, 1], [4, 0]]
    assert cc_b == [[1, 0], [0, 1], [-1, 0], [0, -2]]

    print("passed")


# a should be the one on the left side, b on the right
# a: List[point], b: List[point] -> (List[point], List[point])
def left_right(a, b):
    maxa = max(x for [x, y] in a)
    minb = min(x for [x, y] in b)
    if maxa <= minb:
        return a, b
    else:
        return b, a


# whether c is above the line a-b
# if c is above -> ABOVE
# if c is in line -> INLINE
# if c is below -> BELOW
# a: point, b: point, c: point -> ABOVE | INLINE | BELOW
def relative_position(a, b, c):
    [ax, ay], [bx, by], [cx, cy] = a, b, c
    slope = (by - ay) / (bx - ax)
    cy_exp = ay + slope * (cx - ax)
    if cy_exp == cy: return INLINE
    elif cy_exp > cy: return BELOW
    else: return ABOVE


def test_relative_position():
    print("test relative_position()... ", end="")
    a, b, c = [0, 0], [1, 1], [0, 1]
    assert relative_position(a, b, c) == ABOVE
    print("passed")


# a: List[point], b: List[point] -> (point, point)
def lower_tangent(a, b):
    pass


def main():
    a = [[2, 2], [3, 1], [3, 3], [5, 2], [4, 0]]
    b = [[0, 1], [1, 0], [0, -2], [-1, 0]]

    utangent = upper_tangent(a, b)
    print(f"utangent: {utangent}")

    ltangent = lower_tangent(a, b)
    print("ltangent: {ltangent}")


def test():
    print("running tests...")
    test_relative_position()
    test_counter_clockwise()


# run tests
if len(sys.argv) > 1 and sys.argv[1] == "-t":
    test()
else:
    main()

# ==============================================================

if debug_mode:
    inf.close()