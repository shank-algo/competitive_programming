defmodule CandidateCode do
  def run do
    t = IO.read(:line) |> String.trim() |> String.to_integer()

    Enum.each(1..t, &solve/1)
  end

  def solve(_) do
    _ = IO.read(:line) |> String.trim() |> String.to_integer()

    villian_strengths =
      IO.read(:line)
      |> String.trim()
      |> String.split()
      |> Enum.map(&String.to_integer/1)
      |> Enum.sort()

    player_energies =
      IO.read(:line)
      |> String.trim()
      |> String.split()
      |> Enum.map(&String.to_integer/1)
      |> Enum.sort()

    win? =
      Enum.zip(player_energies, villian_strengths)
      # |> IO.inspect()
      |> Enum.all?(fn {p, v} -> p > v end)

    if win? do
      IO.puts("WIN")
    else
      IO.puts("LOSE")
    end
  end
end

CandidateCode.run()
