import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    n, m = list(map(int, input().strip().split()))
    a = input().strip().split()
    b = "".join(input().strip().split())

    for i in range(n):
        a[i] = "1" if a[i] == "0" else "0"

    a = "".join(a)

    c = sum(1 for i in range(m) if b.startswith(a, i))
    print(c)


main()

# ==============================================================

if debug_mode:
    inf.close()
