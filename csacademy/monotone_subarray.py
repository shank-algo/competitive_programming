import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input().strip())
    a = list(map(int, input().strip().split()))

    mi = [0] * n
    md = [0] * n

    for i in range(n):
        if i == 0:
            mi[i] = 1
            md[i] = 1
            continue

        if a[i] > a[i - 1]:
            mi[i] = mi[i - 1] + 1
            md[i] = 1

        elif a[i] < a[i - 1]:
            mi[i] = 1
            md[i] = md[i - 1] + 1

        else:
            mi[i] = mi[i - 1] + 1
            md[i] = md[i - 1] + 1

    m = max(max(mi), max(md))
    print(m)


main()

# ==============================================================

if debug_mode:
    inf.close()
