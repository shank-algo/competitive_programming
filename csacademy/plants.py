import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    n, m = list(map(int, input().split()))
    t = list(map(int, input().split()))
    ab = [[0, 0]] * n
    for i in range(n):
        ab[i] = list(map(int, input().split()))

    for aabb in ab:
        c = 0
        for tt in t:
            if aabb[0] <= tt and tt <= aabb[1]:
                c += 1
        print(c)


main()

# ==============================================================

if debug_mode:
    inf.close()
