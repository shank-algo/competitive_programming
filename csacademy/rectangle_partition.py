import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    h, w, n, m = map(int, input().strip().split())
    yy = list(map(int, input().strip().split()))
    xx = list(map(int, input().strip().split()))

    yy.sort()
    yy.insert(0, 0)
    xx.sort()
    xx.insert(0, 0)

    xxs = [t - s for s, t in zip(xx, xx[1:])]
    yys = [t - s for s, t in zip(yy, yy[1:])]

    squares = sum(yys.count(x) for x in )
    print(squares)


main()

# ==============================================================

if debug_mode:
    inf.close()