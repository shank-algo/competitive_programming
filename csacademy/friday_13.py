import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    days = [
        "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday",
        "Sunday"
    ]
    day_cnt = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    day = days.index(input().strip())
    res = 0
    for m in range(12):
        for d in range(day_cnt[m]):
            # if date is 13 and day is Friday
            if d == 12 and day == 4:
                res += 1
            day += 1
            day %= 7

    print(res)


main()

# ==============================================================

if debug_mode:
    inf.close()