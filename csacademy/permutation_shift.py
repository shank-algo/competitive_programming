import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input().strip())
    pp = list(map(int, input().strip().split()))

    max_cnt = 0
    for _ in range(n):
        pp.insert(0, pp.pop(-1))
        cnt = 0
        for i in range(n):
            if pp[i] == i + 1:
                cnt += 1
        max_cnt = max(max_cnt, cnt)

    print(max_cnt)


main()

# ==============================================================

if debug_mode:
    inf.close()