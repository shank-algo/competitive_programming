import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input().strip())
    v = list(map(int, input().strip().split()))

    even0 = sum(1 for i, n in enumerate(v) if i % 2 == 0 and n == 0)
    odd0 = sum(1 for i, n in enumerate(v) if i % 2 != 0 and n == 0)

    res = 0
    if even0 > odd0:
        res = n - even0
    else:
        res = n - odd0

    print(res)


main()

# ==============================================================

if debug_mode:
    inf.close()
