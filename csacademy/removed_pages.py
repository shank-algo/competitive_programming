import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def main():
    n = int(input().strip())
    pg = list(map(int, input().strip().split()))

    removed = set()

    for p in pg:
        if p % 2 == 0:
            removed.add((p - 2) // 2)
        else:
            removed.add((p - 1) // 2)

    print(len(removed))


main()

# ==============================================================

if debug_mode:
    inf.close()
