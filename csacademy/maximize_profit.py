import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    s, q, k = list(map(int, input().strip().split()))
    p = list(map(int, input().strip().split()))

    p.sort()

    for pp in p:
        if k > s * pp / 100:
            s += k
        else:
            s += s * pp / 100

    print(s)


main()

# ==============================================================

if debug_mode:
    inf.close()
