import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================

from collections import defaultdict


def main():
    N = int(input().strip())
    destinations = [list(map(int, input().strip().split())) for _ in range(N)]

    stn = 0
    max_passengers = 0
    curr_passengers = 0
    exit_at = defaultdict(int)

    for _ in range(2 * N):  # 2 rounds max
        curr_passengers -= exit_at[stn]
        exit_at[stn] = 0
        for dest, cnt in enumerate(destinations[stn]):
            exit_at[dest] += cnt
            curr_passengers += cnt
        max_passengers = max(max_passengers, curr_passengers)
        stn += 1
        stn %= N

    print(max_passengers)


main()

# ==============================================================

if debug_mode:
    infile.close()
