import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================


def main():
    A, L, H = [int(x) for x in input().strip().split()]
    chickens = (4 * A - L) // 2
    rams = chickens - A + H
    unicorns = H - 2 * rams
    print(unicorns)


main()

# ==============================================================

if debug_mode:
    infile.close()
