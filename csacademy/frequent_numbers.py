import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    n, k = [int(x) for x in input().split()]
    a = [int(x) for x in input().split()]

    c = [0] * 1001
    for i in a:
        c[i] += 1

    res = 0
    for idx in range(1001):
        if c[idx] >= k:
            res += 1

    print(res)


main()

# ==============================================================

if debug_mode:
    inf.close()
