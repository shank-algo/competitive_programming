import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input())
    ints = [int(x) for x in input().strip().split()]

    pairs = 0
    for i in range(n - 1):
        for j in range(i + 1, n):
            if (ints[i] + ints[j]) % 3 == 0:
                pairs += 1

    print(pairs)


def main2():
    n = int(input())
    nums = [int(x) for x in input().strip().split()]
    mod = [0, 0, 0]
    for i in nums:
        mod[i % 3] += 1

    res = (mod[0] * (mod[0] - 1)) / 2 + mod[1] * mod[2]
    print(int(res))


main2()

# ==============================================================

if debug_mode:
    inf.close()
