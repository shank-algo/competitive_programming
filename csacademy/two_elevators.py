import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n, e1, e2 = map(int, input().strip().split())
    pp = [list(map(int, input().strip().split())) for _ in range(n)]

    e1_up = sum(1 for a, b in pp if b > a and a >= e1 and b > e1)
    e1_down = sum(1 for a, b in pp if b < a and a <= e1 and b < e1)

    e2_up = sum(1 for a, b in pp if b > a and a >= e2 and b > e2)
    e2_down = sum(1 for a, b in pp if b < a and a <= e2 and b < e2)

    m = max(e1_up + e2_down, e1_down + e2_up)
    print(m)


main()

# ==============================================================

if debug_mode:
    inf.close()