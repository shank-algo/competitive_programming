import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    c = int(input())
    f = c * 1.8 + 32
    from math import trunc
    print(trunc(f))


main()

# ==============================================================

if debug_mode:
    inf.close()
