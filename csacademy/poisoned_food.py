import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input().strip())
    tp = [list(map(int, input().strip().split())) for _ in range(n)]

    for i in range(n):
        tp[i].append(tp[i][1] / tp[i][0])
        tp[i].append(i + 1)

    from operator import itemgetter
    tp.sort(key=itemgetter(2, 3))

    print(tp[0][3])


main()

# ==============================================================

if debug_mode:
    inf.close()
