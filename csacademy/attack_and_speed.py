import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    a, s, k, x, y = list(map(int, input().strip().split()))

    if (s - a + k * y) % (x + y) != 0:
        print(-1)

    else:
        n = (s - a + k * y) // (x + y)
        if n <= k:
            print(n)
        else:
            print(-1)


main()

# ==============================================================

if debug_mode:
    inf.close()
