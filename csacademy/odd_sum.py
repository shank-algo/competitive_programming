import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input())
    a = [int(x) for x in input().strip().split()]
    e = [0] * n
    o = [0] * n

    for idx, i in enumerate(a):
        prev_e = 0 if idx == 0 else e[idx - 1]
        prev_o = 0 if idx == 0 else o[idx - 1]
        inc_e = 1 if i % 2 == 0 else 0
        inc_o = 0 if i % 2 == 0 else 1
        e[idx] = prev_e + inc_e
        o[idx] = prev_o + inc_o

    c = 0
    for idx in range(0, n - 1):
        if a[idx] % 2 != 0:
            c += e[-1] - e[idx]
        else:
            c += o[-1] - o[idx]

    print(c)


def main2():
    n = int(input())
    a = [int(x) for x in input().strip().split()]
    ce, co = 0, 0

    for i in a:
        if i % 2 == 0:
            ce += 1
        else:
            co += 1

    c = ce * co
    print(c)


main2()

# ==============================================================

if debug_mode:
    inf.close()
