#include <cassert>

#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

typedef long long int64;

struct SegTreeNode {
    int num_one;
    int mx_element;
    int mx_skip;
    int mx_pos;

    SegTreeNode() : num_one(0), mx_element(-1), mx_skip(0), mx_pos(-1) {
    }
    SegTreeNode(int val, int pos) : num_one(1), mx_element(val), mx_skip(0), mx_pos(pos) {
    }
    SegTreeNode(const SegTreeNode& a, const SegTreeNode& b) : num_one(a.num_one + b.num_one) {
        if (a.mx_element >= b.mx_element) {
            mx_element = a.mx_element;
            mx_skip = a.mx_skip;
            mx_pos = a.mx_pos;
        } else {
            mx_element = b.mx_element;
            mx_skip = b.mx_skip + a.num_one;
            mx_pos = b.mx_pos;
        }
    }
};

/*
 * The basic outline of the code is the following.
 * Start with the initial array.
 * Get the first max element from the interval [0, min(k, n)]
 * Erase that element from the array and put it in the solution.
 * Substract from k the position of that element.
 * Repeat N - 1 times (when k = 0, the first element will be chosen)
 */

/*
 * To do this, we need to erase that element lazy, using a Segment Tree
 * Lets say that all the elements that are in the array are marked with 1
 * and an element which was erased lazy is marked with 0.
 * When refering to the interval [0, k] we need to find the k+1th value of 1
 * and query the segment [0, that position] discarding the elements which were
 * erased lazy.
 * In this query we need to find the first maximum value and how many 1s are
 * in from of that position.
 * Extract from K the number of ones (that is the number of swaps required
 * to move that element to the first position) and add the maximum value
 * to the answer.
 */

struct SegTree {
    int n;
    vector<SegTreeNode> e;

    SegTree(const vector<int>& els) : n(els.size()), e(4 * n, SegTreeNode()) {
        InitSegTree(1, 0, n - 1, els);
    }

    SegTree() : n(0) {
    }

    void InitSegTree(int node, int left, int right, const vector<int>& els) {
        if (left == right) {
            e[node] = SegTreeNode(els[left], left);
            return;
        }

        int mid = (left + right) / 2;
        InitSegTree(2 * node, left, mid, els);
        InitSegTree(2 * node + 1, mid + 1, right, els);
        e[node] = SegTreeNode(e[2 * node], e[2 * node + 1]);
    }

    int GetMxElemenet(int64& k) {
        SegTreeNode node = GetMxElemenet(1, 0, n - 1, k);
        SetEmptyElement(1, 0, n - 1, node.mx_pos);
        k -= node.mx_skip;

        return node.mx_element;
    }

    SegTreeNode GetMxElemenet(int node, int left, int right, int64 k) {
        if (k < 0) {
            return SegTreeNode();
        }
        if (e[node].mx_skip <= k) {
            return e[node];
        }

        int mid = (left + right) / 2;
        return SegTreeNode(GetMxElemenet(2 * node, left, mid, k),
                        GetMxElemenet(2 * node + 1, mid + 1, right, k - e[2 * node].num_one));
    }

    void SetEmptyElement(int node, int left, int right, int pos) {
        if (pos < left) {
            return;
        }

        if (right < pos) {
            return;
        }

        if (left == right) {
            assert(left == pos);
            e[node] = SegTreeNode();
            return;
        }

        int mid = (left + right) / 2;
        SetEmptyElement(2 * node, left, mid, pos);
        SetEmptyElement(2 * node + 1, mid + 1, right, pos);
        e[node] = SegTreeNode(e[2 * node], e[2 * node + 1]);
    }
} segment_tree;

int main() {
    int n;
    int64 k;
    cin >> n >> k;

    vector<int> els(n);
    for (auto& itr : els) {
        cin >> itr;
    }

    segment_tree = SegTree(els);

    vector<int> answer = {};
    for (int i = 0; i < n; i += 1) {
        answer.push_back(segment_tree.GetMxElemenet(k));
    }

    for (auto itr : answer) {
        cout << itr << ' ';
    }
    cout << '\n';
    return 0;
}
