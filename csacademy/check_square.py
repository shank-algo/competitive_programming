import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    t = int(input().strip())

    from operator import itemgetter

    for _ in range(1, t + 1):
        pts = []
        for _ in range(4):
            pts.append(list(map(int, input().strip().split())))
        pts.sort(key=itemgetter(0, 1))

        p0p1 = (pts[0][0] - pts[1][0])**2 + (pts[0][1] - pts[1][1])**2
        p0p2 = (pts[0][0] - pts[2][0])**2 + (pts[0][1] - pts[2][1])**2
        p0p3 = (pts[0][0] - pts[3][0])**2 + (pts[0][1] - pts[3][1])**2
        p1p2 = (pts[1][0] - pts[2][0])**2 + (pts[1][1] - pts[2][1])**2

        if p0p1 == p0p2 and p0p3 == p1p2:
            print(1)
        else:
            print(0)


main()

# ==============================================================

if debug_mode:
    inf.close()
