import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n, rt, k = list(map(int, input().strip().split()))
    bt = n - rt

    # min_rt
    min_rt = rt - k if rt >= k else k - rt

    # max_rt
    min_bt = bt - k if bt >= k else k - bt
    max_rt = n - min_bt

    print(min_rt, max_rt)


main()

# ==============================================================

if debug_mode:
    inf.close()
