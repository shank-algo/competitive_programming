import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input())
    g = list(map(int, input().strip().split()))

    g.sort()
    grade = sum(g[1:-1]) / (n - 2)
    from math import floor
    grade = floor(grade)
    print(grade)


main()

# ==============================================================

if debug_mode:
    inf.close()
