import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def main():
    n, m = list(map(int, input().strip().split()))
    a = []
    for _ in range(n):
        a.append(list(map(int, input().strip().split())))

    down = [[0 for _ in range(m)] for _ in range(n)]
    for r in range(n - 1, -1, -1):
        for c in range(m - 1, -1, -1):
            d = 0
            if a[r][c] == 1:
                d = 1 if r == n - 1 else down[r + 1][c] + 1

            down[r][c] = d

    left = [[0 for _ in range(m)] for _ in range(n)]
    for r in range(n):
        for c in range(m):
            l = 0
            if a[r][c] == 1:
                l = 1 if c == 0 else left[r][c - 1] + 1

            left[r][c] = l

    max_cnt = 0
    for r in range(n):
        for c in range(m):
            if a[r][c] == 1 and down[r][c] > 1 and left[r][c] > 1:
                max_cnt = max(max_cnt, down[r][c] + left[r][c] - 1)

    print(max_cnt)


main()

# ==============================================================

if debug_mode:
    inf.close()
