import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def main():
    n = int(input().strip())
    aa = list(map(int, input().strip().split()))

    aa.sort(reverse=True)
    aa = aa[:(n // 2) * 2]

    m = 0
    for i in range(0, len(aa), 2):
        if aa[i] < 0 and aa[i + 1] < 0:
            break
        m = max(m, m + aa[i] + aa[i + 1])

    print(m)


main()

# ==============================================================

if debug_mode:
    inf.close()
