# ####### SEGMENT TREE ############################################

from math import log, floor


class SegmentTree:
    def __init__(self, arr, node_cls):
        self.arr = arr
        self.node_cls = node_cls
        self.size = 2 * (2**floor(log(len(arr)) / log(2) + 1))
        self.heap = []
        for _ in range(self.size):
            self.heap.append(self.node_cls())
        self.__build(1, 0, len(arr))

    def get_value(self, fr, to):
        result_node = self.__get_value(1, fr, to)
        return result_node.get_value()

    def update(self, fr, to, value):
        self.__update(1, fr, to, value)

    def __update(self, v, fr, to, value):
        if 2 * v >= self.size:
            self.heap[v].assign_leaf(value)
            return

        n = self.heap[v]  # represents [n.fr, n.to]

        if self.__contains(fr, to, n.fr, n.to):
            self.__change(n, value)

        if n.size == 1:
            return

        if self.__intersects(fr, to, n.fr, n.to):
            self.__propagate(v)

            # print(str(2 * v))
            self.__update(2 * v, fr, to, value)
            # print(str(2 * v + 1))
            self.__update(2 * v + 1, fr, to, value)

            n.merge(self.heap[2 * v], self.heap[2 * v + 1])

    def __propagate(self, v):
        """Propogate temporal values to children"""
        n = self.heap[v]

        if n.pending_value != None:
            if 2 * v < self.size:
                self.__change(self.heap[2 * v], n.pending_value)
                self.__change(self.heap[2 * v + 1], n.pending_value)
            else:
                n.assign_leaf(n.pending_value)
            n.pending_value = None

    def __change(self, n, value):
        """Save the temporal values that will be propagated lazily"""
        n.pending_value = value
        n.assign_summary_value(value)
        self.arr[n.fr] = value

    def __get_value(self, v, fr, to):
        n = self.heap[v]

        if self.__contains(fr, to, n.fr, n.to):
            return self.heap[v]

        if self.__intersects(fr, to, n.fr, n.to):
            left = self.__get_value(2 * v, fr, to)
            right = self.__get_value(2 * v + 1, fr, to)

            if left is None:
                return right
            elif right is None:
                return left
            else:
                result = self.node_cls()
                result.merge(left, right)
                return result

        return None

    def __contains(self, fr1, to1, fr2, to2):
        return fr2 >= fr1 and to2 <= to1

    def __intersects(self, fr1, to1, fr2, to2):
        return (fr1 <= fr2 and to1 >= fr2) or (fr1 >= fr2 and fr1 <= to2)

    def __build(self, v, fr, size):
        self.heap[v] = self.node_cls()
        self.heap[v].fr = fr
        self.heap[v].to = fr + size - 1

        if size == 1:
            self.heap[v].assign_leaf(self.arr[fr])
        else:
            # build childs
            self.__build(2 * v, fr, size // 2)
            self.__build(2 * v + 1, fr + size // 2, size - size // 2)

            self.heap[v].merge(self.heap[2 * v], self.heap[2 * v + 1])


from abc import abstractmethod


class Node:
    @abstractmethod
    def __init__(self):
        # create instance with default properties
        # must contain the fields titled `pending_value` and `size`
        #
        # self.pending_value = None
        # self.size = 0
        # self.fr = -1
        # self.to = -1
        pass

    @abstractmethod
    def assign_leaf(self, value):
        # assign the various properties for this leaf node based on the value
        pass

    @abstractmethod
    def assign_summary_value(self, value):
        # assign the various temporal property for this leaf node
        # based on the value
        pass

    @abstractmethod
    def merge(self, left, right):
        # calculate the properties for this node on the basis
        # of the left anf right child nodes
        pass

    @abstractmethod
    def get_value(self):
        # return the value of the required aggregate statistic
        # associated with this node
        pass


# ####### SEGMENT TREE END ########################################
