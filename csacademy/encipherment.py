import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    s = input()
    p = input()
    orda = ord("a")

    res = []
    for c in s:
        i = ord(c) - orda
        res.append(p[i])

    print("".join(res))


main()

# ==============================================================

if debug_mode:
    inf.close()
