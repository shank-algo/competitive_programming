import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================

from math import log2, ceil


class Node:
    def __init__(self, max_element, max_pos, ones, swaps_to_max):
        self.max_element = max_element
        self.max_pos = max_pos
        self.ones = ones
        self.swaps_to_max = swaps_to_max

    @classmethod
    def empty_node(cls):
        return cls(-1, -1, 0, 0)

    @classmethod
    def leaf_node(cls, idx, value):
        return cls(value, idx, 1, 0)

    @classmethod
    def merge(cls, left, right):
        ones = left.ones + right.ones
        if left.max_element >= right.max_element:
            max_element = left.max_element
            max_pos = left.max_pos
            swaps_to_max = left.swaps_to_max
        else:
            max_element = right.max_element
            max_pos = right.max_pos
            swaps_to_max = left.ones + right.swaps_to_max
        return cls(max_element, max_pos, ones, swaps_to_max)

    def __str__(self):
        return "max_element:{} max_pos:{} ones:{} swaps_to_max:{}".format(
            self.max_element, self.max_pos, self.ones, self.swaps_to_max)


class SegmentTree:
    def __init__(self, arr):
        self.N = 2**ceil(log2(len(arr)))
        self.arr = arr
        self.arr_len = len(arr)
        self.heap = []
        for _ in range(2 * self.N):
            self.heap.append(Node.empty_node())
        self.__build(1, 0, self.N - 1)

    def __str__(self):
        for node in self.heap:
            print(node)
        return ""

    def get_max_element(self, k):
        # find the max element that is at max k swaps away
        # and hasnt been deleted yet
        max_node = self.__get_max_element(1, 0, self.N - 1, k)

        # mark the element as deleted
        self.__mark_element_as_deleted(1, 0, self.N - 1, max_node.max_pos)

        # return the element and the number of swaps required for it
        return max_node.max_element, max_node.swaps_to_max

    def __get_max_element(self, node, start, end, k):
        if k < 0:
            return Node.empty_node()

        if self.heap[node].swaps_to_max <= k:
            return self.heap[node]

        left, right = 2 * node, 2 * node + 1
        mid = (start + end) // 2
        left_result = self.__get_max_element(left, start, mid, k)
        right_result = self.__get_max_element(right, mid + 1, end,
                                              k - left_result.ones)
        return Node.merge(left_result, right_result)

    def __mark_element_as_deleted(self, node, start, end, idx):
        if idx < start or end < idx:
            return

        if start == end:
            if start == idx:
                self.heap[node] = Node.empty_node()
            return

        left, right = 2 * node, 2 * node + 1
        mid = (start + end) // 2
        self.__mark_element_as_deleted(left, start, mid, idx)
        self.__mark_element_as_deleted(right, mid + 1, end, idx)
        self.heap[node] = Node.merge(self.heap[left], self.heap[right])

    def __build(self, node, start, end):
        if start == end:
            if start < self.arr_len:
                self.heap[start + self.N] = Node.leaf_node(
                    start, self.arr[start])
            return

        left, right = 2 * node, 2 * node + 1
        mid = (start + end) // 2
        self.__build(left, start, mid)
        self.__build(right, mid + 1, end)
        self.heap[node] = Node.merge(self.heap[left], self.heap[right])


def main():
    n, k = list(map(int, input().strip().split()))
    arr = list(map(int, input().strip().split()))

    sg = SegmentTree(arr)

    answer = []
    for _ in range(n):
        # print("==================================")
        # print("answer:", answer, " k:", k)
        # print(sg)
        elem, swaps = sg.get_max_element(k)
        # print("elem:{} swaps:{}".format(elem, swaps))
        answer.append(elem)
        k -= swaps

    print(" ".join(str(x) for x in answer))


main()

# ==============================================================

if debug_mode:
    inf.close()