import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input().strip())
    ws = []
    for _ in range(n):
        ws.append(input().strip())

    from collections import defaultdict
    f = defaultdict(int)
    for w in ws:
        f[w[0]] += 1

    c = 0
    for w in ws:
        acrnm = True
        for ci, ch in enumerate(w):
            if ch not in f:
                acrnm = False
                break

            elif ci == 0 and f[ch] - 1 < w.count(ch):
                acrnm = False
                break

            elif f[ch] < w.count(ch):
                acrnm = False
                break

        if acrnm:
            c += 1

    print(c)


main()

# ==============================================================

if debug_mode:
    inf.close()
