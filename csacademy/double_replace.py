import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    s = input().strip()
    a = input().strip()
    b = input().strip()

    ssa = s.split(a)
    for i in range(len(ssa)):
        ssa[i] = ssa[i].replace(b, a)

    sf = b.join(ssa)
    print(sf)



main()

# ==============================================================

if debug_mode:
    inf.close()
