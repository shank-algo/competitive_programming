import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================

from collections import defaultdict

n, m = list(map(int, input().strip().split()))
pp = list(map(int, input().strip().split()))

order = defaultdict(int)
for i, node_num in enumerate(pp):
    order[node_num] = i

nodes = defaultdict(list)
for _ in range(m):
    a, b = list(map(int, input().strip().split()))
    nodes[a].append(b)
    nodes[b].append(a)


def by_order(a):
    return order[a]


visited = set()
dfs_order = []


def dfs(node_num):
    dfs_order.append(node_num)
    visited.add(node_num)
    for nn in nodes[node_num]:
        if nn not in visited:
            dfs(nn)


for node_num in nodes:
    nodes[node_num].sort(key=by_order)

dfs(1)

if dfs_order == pp:
    print(1)
else:
    print(0)

# ==============================================================

if debug_mode:
    inf.close()
