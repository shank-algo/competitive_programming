import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def main():
    n = int(input().strip())
    a = list(map(int, input().strip().split()))
    wins = [0] * n

    w_idx = 0
    for i in range(1, n):
        if a[w_idx] < a[i]:
            w_idx = i

        wins[w_idx] += 1

    print(" ".join(str(x) for x in wins))


main()

# ==============================================================

if debug_mode:
    inf.close()