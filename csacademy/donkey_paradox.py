import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n, m = list(map(int, input().strip().split()))
    hs1 = list(map(int, input().strip().split()))
    hs2 = list(map(int, input().strip().split()))

    hs1 = [hs1[0] - 1, hs1[1] - 1]
    hs2 = [hs2[0] - 1, hs2[1] - 1]

    cnt = 0
    for r in range(n):
        for c in range(m):
            d1 = abs(hs1[0] - r) + abs(hs1[1] - c)
            d2 = abs(hs2[0] - r) + abs(hs2[1] - c)
            if d1 == d2:
                cnt += 1

    print(cnt)


main()

# ==============================================================

if debug_mode:
    inf.close()
