import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input().strip())
    zl = list(map(int, input().strip().split()))
    zr = list(map(int, input().strip().split()))

    a = [-1] * n

    for i in range(0, n - 1):
        if zl[i] < zl[i + 1]:
            a[i] = 0
        else:
            a[i] = 1

    if zr[-2] == 1:
        a[n - 1] = 0
    else:
        a[n - 1] = 1

    print("".join(str(i) for i in a))


main()

# ==============================================================

if debug_mode:
    inf.close()
