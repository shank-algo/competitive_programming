import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input())
    v = list(map(int, input().strip().split()))

    s = set()
    for i in range(n - 1):
        for j in range(i + 1, n):
            s.add(v[i] + v[j])

    print(len(s))


main()

# ==============================================================

if debug_mode:
    inf.close()
