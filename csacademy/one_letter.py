import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input().strip())
    cc = []
    for _ in range(n):
        w = input().strip()
        cc.append(min(w))

    cc.sort()
    print("".join(cc))


main()

# ==============================================================

if debug_mode:
    inf.close()