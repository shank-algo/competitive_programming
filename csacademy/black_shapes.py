import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os

    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


else:
    inf = sys.stdin

# ==============================================================

from collections import deque, defaultdict
from typing import DefaultDict

dx = [-1, 0, 1, 0]
dy = [0, -1, 0, 1]


def expand(matrix, group, group_size, r, c, group_id, visited):
    to_visit = deque([[r, c]])
    rows, cols = len(matrix), len(matrix[0])
    while to_visit:
        [x, y] = to_visit.popleft()

        if visited[x][y]:
            continue

        visited[x][y] = True
        group[x][y] = group_id
        group_size[group_id] += 1

        for i in range(4):
            nx, ny = x + dx[i], y + dy[i]
            if nx < 0 or nx > rows - 1 or ny < 0 or ny > cols - 1:
                continue
            if matrix[nx][ny] == 1:
                to_visit.append([nx, ny])


def read_matrix(n, m):
    matrix = []
    for _ in range(n):
        row = [int(x) for x in input().strip().split()]
        matrix.append(row)
    return matrix


def main():
    n, m = map(int, input().strip().split())
    matrix = read_matrix(n, m)
    group = [[0 for _ in range(m)] for _ in range(n)]
    group_size: DefaultDict = defaultdict(int)
    visited = [[False for _ in range(m)] for _ in range(n)]

    group_id = 1
    for r in range(n):
        for c in range(m):
            if not visited[r][c] and matrix[r][c] == 1:
                expand(matrix, group, group_size, r, c, group_id, visited)
                group_id += 1

    # print(f"group: {group}")

    max_size = 0
    for r in range(n):
        for c in range(m):
            if matrix[r][c] == 1:
                continue

            neighbours = set()
            for i in range(4):
                nx, ny = r + dx[i], c + dy[i]
                if nx < 0 or nx > n - 1 or ny < 0 or ny > m - 1:
                    continue
                if matrix[nx][ny] == 1:
                    neighbours.add(group[nx][ny])

            size = 1
            for id in neighbours:
                size += group_size[id]

            max_size = max(size, max_size)

    print(max_size)


main()

# ==============================================================

if debug_mode:
    inf.close()
