import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input().strip())
    lr = []
    for _ in range(n):
        lr.append(list(map(int, input().strip().split())))

    c = 0
    for i in range(n):
        seg = lr[i]
        for j in range(n):
            other = lr[j]
            if seg[0] > other[0] and seg[1] < other[1]:
                c += 1
                break

    print(c)


main()

# ==============================================================

if debug_mode:
    inf.close()
