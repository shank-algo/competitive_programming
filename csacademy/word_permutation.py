import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input().strip())
    wl = list((input().strip(), i + 1) for i in range(n))

    wl.sort()

    from operator import itemgetter
    sigma = map(str, map(itemgetter(1), wl))
    sigma = " ".join(sigma)

    print(sigma)


main()

# ==============================================================

if debug_mode:
    inf.close()
