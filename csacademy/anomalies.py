import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n, k = list(map(int, input().strip().split()))
    a = list(map(int, input().strip().split()))

    anom = 0
    for i in range(n):
        isanom = True
        for j in range(n):
            if i != j and abs(a[j] - a[i]) <= k:
                isanom = False
                break

        if isanom:
            anom += 1

    print(anom)


main()

# ==============================================================

if debug_mode:
    inf.close()
