import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    s = input().strip()

    sp = s.split("B")
    for i in range(len(sp)):
        if sp[i]:
            sp[i] = "A"

    rs = "B".join(sp)
    print(rs)


main()

# ==============================================================

if debug_mode:
    inf.close()
