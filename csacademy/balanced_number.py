import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    strn = input()
    c = [-1] * 10
    for i in range(10):
        ic = strn.count(str(i))
        c[i] = ic if ic > 0 else -1

    res = 1 if all(c[i] == c[0] for i in range(10)) else 0
    
    print(res)

main()

# ==============================================================

if debug_mode:
    inf.close()
