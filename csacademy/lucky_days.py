import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input().strip())
    a = list(map(int, input().strip().split()))

    m = -1
    lucky = 0
    for i in range(n):
        if a[i] > m:
            lucky += 1
            m = a[i]

    print(lucky)


main()

# ==============================================================

if debug_mode:
    inf.close()
