import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input().strip())
    ss = []
    for _ in range(n):
        ss.append(input().strip())

    ccss = set()
    for i in range(n):
        for j in range(n):
            if i == j:
                continue
            ccss.add(ss[i] + ss[j])

    res = []
    for i, s in enumerate(ss):
        if s in ccss:
            res.append(i + 1)

    print(" ".join(str(x) for x in res))


main()

# ==============================================================

if debug_mode:
    inf.close()