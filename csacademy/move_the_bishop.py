import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    r1, c1, r2, c2 = list(map(int, input().strip().split()))

    from collections import defaultdict
    d = defaultdict(int)

    if (r1 + c1) % 2 != (r2 + c2) % 2:
        print(-1)

    elif r1 == r2 and c1 == c2:
        print(0)

    elif abs(r1 - r2) == abs(c1 - c2):
        print(1)

    else:
        print(2)


main()

# ==============================================================

if debug_mode:
    inf.close()
