import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def main():
    n = int(input().strip())
    bb = []
    for _ in range(n):
        bb.append(list(map(int, input().strip().split())))

    m = 10**7
    for b in bb:
        t = max(b[0], b[1])
        t += b[2]
        m = min(m, t)

    print(m)


main()

# ==============================================================

if debug_mode:
    inf.close()
