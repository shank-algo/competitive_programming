import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    n, m = list(map(int, input().strip().split()))
    a = list(map(int, input().strip().split()))
    b = list(map(int, input().strip().split()))

    a.sort(reverse=True)
    b.sort(reverse=True)

    ac, bc = 0, 0
    for i in range(min(n, m)):
        if a[i] > b[i]:
            ac += 1
        else:
            bc += 1

    print(ac, bc)


main()

# ==============================================================

if debug_mode:
    inf.close()
