import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input())
    a = list(map(int, input().split()))

    from collections import defaultdict
    f = defaultdict(int)
    v = defaultdict(int)
    s = 0
    for i in a:
        f[i] += 1
        v[i] += i
        s += i

    res = s - max(v.values())
    print(res)


main()

# ==============================================================

if debug_mode:
    inf.close()
