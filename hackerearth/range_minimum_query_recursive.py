import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


class SegmentTree:
    def __init__(self, arr):
        self.size = self.__get_sgt_size(len(arr))
        self.N = self.size // 2
        self.heap = [None] * self.size
        for idx, v in enumerate(arr):
            self.heap[idx + self.N] = v
        self.__build(1)

    def query(self, fr, to):
        return self.__query(1, 0, self.N - 1, fr, to)

    def update(self, index, value):
        self.__update(1, 0, self.N - 1, index, value)

    def __update(self, node, start, end, index, value):
        # range represented by this node does not contain the index
        if start > index or end < index:
            return

        # this node is a leaf
        if start == end:
            self.heap[node] = value
            return

        # range represented by this node contains the index
        left, right = 2 * node, 2 * node + 1
        mid = (start + end) // 2
        self.__update(left, start, mid, index, value)
        self.__update(right, mid + 1, end, index, value)

        if self.heap[right] is None:
            self.heap[node] = self.heap[left]
        elif self.heap[left] is None:
            self.heap[node] = self.heap[right]
        else:
            self.heap[node] = min(self.heap[left], self.heap[right])

    def __query(self, node, start, end, fr, to):
        # range represented by this node is completely outside the query range
        if start > to or end < fr:
            return None

        # range represented by this node is completely inside the query range
        if start >= fr and end <= to:
            return self.heap[node]

        # range represented by this node is partially inside and partially
        # outside the query range
        left, right = 2 * node, 2 * node + 1
        mid = (start + end) // 2
        left_result = self.__query(left, start, mid, fr, to)
        right_result = self.__query(right, mid + 1, end, fr, to)
        if right_result is None:
            return left_result
        elif left_result is None:
            return right_result
        else:
            return min(left_result, right_result)

    def __get_sgt_size(self, n):
        size = 1
        while size < n:
            size *= 2
        return 2 * size

    def __build(self, node):
        if node >= self.N:
            return

        left = 2 * node
        right = 2 * node + 1
        self.__build(2 * node)
        self.__build(2 * node + 1)

        if self.heap[right] is None:
            self.heap[node] = self.heap[left]
        else:
            self.heap[node] = min(self.heap[left], self.heap[right])


def main():
    n, q = list(map(int, input().strip().split()))
    arr = list(map(int, input().strip().split()))

    sg = SegmentTree(arr)

    # print("sg:", sg.heap)

    # sg.update(1, 0)

    # print("sg:", sg.heap)

    # print("[0, 4]:", sg.query(0, 4))
    # print("[1, 3]:", sg.query(1, 3))
    # print("[2, 2]:", sg.query(2, 2))
    # print("[3, 4]:", sg.query(3, 4))

    for _ in range(q):
        op, x_str, y_str = input().strip().split()
        x, y = int(x_str), int(y_str)
        if op == "q":
            print(sg.query(x - 1, y - 1))
        elif op == "u":
            sg.update(x - 1, y)


main()

# ==============================================================

if debug_mode:
    inf.close()
