import sys


def solve(s, k):
    suffixes = list(s[i:] for i in range(len(s)))
    suffixes.sort()
    return suffixes[k - 1]


if __name__ == "__main__":
    debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
    if debug_mode:
        import os
        f = open(os.path.abspath(__file__).replace(".py", ".in"))

        def input():
            return f.readline()

    s, k = input().strip().split()
    k = int(k)
    result = solve(s, k)
    print(result)

    if debug_mode:
        f.close()
