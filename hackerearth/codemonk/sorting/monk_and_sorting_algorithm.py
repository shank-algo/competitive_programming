import sys
from collections import defaultdict


def digit_at_i(n, i):
    return n // 10**(i - 1) % 10


def solve(n, arr):
    # 1 based indexing
    num_str = {k: str(k) for k in arr}
    i = 1
    while True:
        ei = 1 + 5 * (i - 1)
        weights = []
        for pos, k in enumerate(arr):
            si = min(5 * i, len(num_str[k]))
            w_str = "".join(
                str(digit_at_i(k, idx)) for idx in range(si, ei - 1, -1))
            w_str = "0" if w_str == "" else w_str
            w = int(w_str)
            weights.append((w, pos))
        if sum(w for w, _ in weights) == 0:
            break
        weights.sort()
        tmp = []
        for _, pos in weights:
            tmp.append(arr[pos])
        arr = tmp
        print(" ".join(str(num) for num in arr))
        i += 1


if __name__ == "__main__":
    debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
    if debug_mode:
        import os
        f = open(os.path.abspath(__file__).replace(".py", ".in"))

        def input():
            return f.readline()

    n = int(input().strip())
    arr = list(map(int, input().strip().split()))
    solve(n, arr)

    if debug_mode:
        f.close()
