import sys


def solve(n, k, arr):
    return sorted(arr, key=lambda x: x % k)


if __name__ == "__main__":
    debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
    if debug_mode:
        import os
        f = open(os.path.abspath(__file__).replace(".py", ".in"))

        def input():
            return f.readline()

    n, k = map(int, input().strip().split())
    arr = map(int, input().strip().split())
    result = solve(n, k, arr)
    print(" ".join(map(str, result)))

    if debug_mode:
        f.close()
