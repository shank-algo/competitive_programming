import sys


def solve(n, strs):
    strs_idx = [(s, i) for i, s in enumerate(strs)]
    sorted_strs_idx = sorted(strs_idx)
    result = []
    for i, s in enumerate(strs):
        niceness = 0
        for other_s, idx in sorted_strs_idx:
            if other_s == s:
                break
            if idx < i:
                niceness += 1
        result.append(niceness)
    return result


if __name__ == "__main__":
    debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
    if debug_mode:
        import os
        f = open(os.path.abspath(__file__).replace(".py", ".in"))

        def input():
            return f.readline()

    n = int(input().strip())
    strs = []
    for line in range(n):
        s = input().strip()
        strs.append(s)
    result = solve(n, strs)
    print("\n".join(map(str, result)))

    if debug_mode:
        f.close()
