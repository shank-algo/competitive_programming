import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================

from math import inf as infinite


class SegmentTree:
    def __init__(self, arr):
        self.size = self.__get_sgt_size(len(arr))
        self.N = self.size // 2

        self.heap = [None] * self.size
        for i, v in enumerate(arr):
            self.heap[i + self.N] = v

        self.__build()

    def query(self, fr, to):
        """query the range [left, right] (inclusive)"""
        fr, to = fr + self.N, to + self.N

        if fr == to:
            return self.heap[fr]

        res = infinite
        while fr <= to:
            if fr % 2 != 0:
                res = min(res, self.heap[fr])
                fr += 1
            if to % 2 == 0:
                res = min(res, self.heap[to])
                to -= 1
            fr, to = fr // 2, to // 2
        return res

    def update(self, index, value):
        index += self.N
        self.heap[index] = value

        # update the parents, all the way to the root
        idx = index // 2
        while idx > 0:
            left, right = 2 * idx, 2 * idx + 1
            if self.heap[right] is None:
                self.heap[idx] = self.heap[left]
            else:
                self.heap[idx] = min(self.heap[left], self.heap[right])
            idx //= 2

    def __get_sgt_size(self, n):
        size = 1
        while size < n:
            size *= 2
        return 2 * size

    def __build(self):
        for idx in range(self.N - 1, 0, -1):
            left, right = 2 * idx, 2 * idx + 1
            if self.heap[right] is None:
                # this covers the case when both left and right are None as well
                self.heap[idx] = self.heap[left]
            else:
                self.heap[idx] = min(self.heap[left], self.heap[right])


def main():
    n, q = list(map(int, input().strip().split()))
    arr = list(map(int, input().strip().split()))

    sg = SegmentTree(arr)

    for _ in range(q):
        op, x_str, y_str = input().strip().split()
        x, y = int(x_str), int(y_str)
        if op == "q":
            print(sg.query(x - 1, y - 1))
        elif op == "u":
            sg.update(x - 1, y)


main()

# ==============================================================

if debug_mode:
    inf.close()
