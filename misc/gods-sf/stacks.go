package main

type StackInterface interface {
	Push(value interface{})
	Pop() (value interface{}, ok bool)
	Peek() (value interface{}, ok bool)

	ContainerInterface
	// Empty() bool
	// Size() int
	// Clear()
	// Values() []interface{}
}
