package main

type SetInterface interface {
	Add(elements ...interface{})
	Remove(elements ...interface{})
	Contains(elements ...interface{}) bool

	ContainerInterface
	// Empty() bool
	// Size() int
	// Clear()
	// Values() []interface{}
}
