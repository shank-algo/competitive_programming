package main

type MapInterface interface {
	Put(key interface{}, value interface{})
	Get(key interface{}) (value interface{}, found bool)
	Remove(key interface{})
	Keys() []interface{}

	ContainerInterface
	// Empty() bool
	// Size() int
	// Clear()
	// Values() []interface{}
}
