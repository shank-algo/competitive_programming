package main

// ****************************************************************************
// TREE MAP *******************************************************************
// ****************************************************************************

func assertTreeMapInterfaceImplementation() {
	var _ MapInterface = (*TreeMap)(nil)
}

type TreeMap struct {
	tree *RBTree
}

// Instantiates a tree map with the custom comparator.
func NewTreeMapWith(comparator Comparator) *TreeMap {
	return &TreeMap{tree: NewRBTreeWith(comparator)}
}

// Instantiates a tree map with the IntComparator, i.e. keys are of type int.
func NewTreeMapWithIntComparator() *TreeMap {
	return &TreeMap{tree: NewRBTreeWithIntComparator()}
}

// Instantiates a tree map with the StringComparator, i.e. keys are of type string.
func NewTreeMapWithStringComparator() *TreeMap {
	return &TreeMap{tree: NewRBTreeWithStringComparator()}
}

// Inserts key-value pair into the map.
// Key should adhere to the comparator's type assertion, otherwise method panics.
func (m *TreeMap) Put(key interface{}, value interface{}) {
	m.tree.Put(key, value)
}

// Searches the element in the map by key and returns its value or nil if key is not found in tree.
// Second return parameter is true if key was found, otherwise false.
// Key should adhere to the comparator's type assertion, otherwise method panics.
func (m *TreeMap) Get(key interface{}) (value interface{}, found bool) {
	return m.tree.Get(key)
}

// Remove the element from the map by key.
// Key should adhere to the comparator's type assertion, otherwise method panics.
func (m *TreeMap) Remove(key interface{}) {
	m.tree.Remove(key)
}

// Returns true if map does not contain any elements
func (m *TreeMap) Empty() bool {
	return m.tree.Empty()
}

// Returns number of elements in the map.
func (m *TreeMap) Size() int {
	return m.tree.Size()
}

// Returns all keys in-order
func (m *TreeMap) Keys() []interface{} {
	return m.tree.Keys()
}

// Returns all values in-order based on the key.
func (m *TreeMap) Values() []interface{} {
	return m.tree.Values()
}

// Removes all elements from the map.
func (m *TreeMap) Clear() {
	m.tree.Clear()
}

func (m *TreeMap) String() string {
	str := "TreeMap\n"
	str += m.tree.String()
	return str
}

// ****************************************************************************
// TREE MAP END ***************************************************************
// ****************************************************************************
