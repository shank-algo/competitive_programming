package main

type ListInterface interface {
	Get(index int) (interface{}, bool)
	Remove(index int)
	Add(elements ...interface{})
	Contains(elements ...interface{}) bool
	Sort(comparator Comparator)
	Swap(index1, index2 int)

	ContainerInterface
	// Empty() bool
	// Size() int
	// Clear()
	// Values() []interface{}
}
