package main

import (
	"fmt"
	"strings"
)

// ****************************************************************************
// DOUBLY LINKED LIST *********************************************************
// ****************************************************************************

func assertLLListInterfaceImplementation() {
	var _ ListInterface = (*DLList)(nil)
}

type DLList struct {
	first *dllElement
	last  *dllElement
	size  int
}

type dllElement struct {
	value interface{}
	prev  *dllElement
	next  *dllElement
}

// Instantiates a new empty list
func NewDLList() *DLList {
	return &DLList{}
}

// Appends a value (one or more) at the end of the list (same as Append())
func (list *DLList) Add(values ...interface{}) {
	for _, value := range values {
		newElement := &dllElement{value: value, prev: list.last}
		if list.size == 0 {
			list.first = newElement
			list.last = newElement
		} else {
			list.last.next = newElement
			list.last = newElement
		}
		list.size++
	}
}

// Appends a value (one or more) at the end of the list (same as Add())
func (list *DLList) Append(values ...interface{}) {
	list.Add(values...)
}

// Prepends a values (or more)
func (list *DLList) Prepend(values ...interface{}) {
	// in reverse to keep passed order i.e. ["c","d"] -> Prepend(["a","b"]) -> ["a","b","c",d"]
	for v := len(values) - 1; v >= 0; v-- {
		newElement := &dllElement{value: values[v], next: list.first}
		if list.size == 0 {
			list.first = newElement
			list.last = newElement
		} else {
			list.first.prev = newElement
			list.first = newElement
		}
		list.size++
	}
}

// Returns the element at index.
// Second return parameter is true if index is within bounds of the array and array is not empty, otherwise false.
func (list *DLList) Get(index int) (interface{}, bool) {

	if !list.withinRange(index) {
		return nil, false
	}

	// determine traveral direction, last to first or first to last
	if list.size-index < index {
		element := list.last
		for e := list.size - 1; e != index; e, element = e-1, element.prev {
		}
		return element.value, true
	} else {
		element := list.first
		for e := 0; e != index; e, element = e+1, element.next {
		}
		return element.value, true
	}

}

// Removes one or more elements from the list with the supplied indices.
func (list *DLList) Remove(index int) {

	if !list.withinRange(index) {
		return
	}

	if list.size == 1 {
		list.Clear()
		return
	}

	var element *dllElement
	// determine traversal direction, last to first or first to last
	if list.size-index < index {
		element = list.last
		for e := list.size - 1; e != index; e, element = e-1, element.prev {
		}
	} else {
		element = list.first
		for e := 0; e != index; e, element = e+1, element.next {
		}
	}

	if element == list.first {
		list.first = element.next
	}
	if element == list.last {
		list.last = element.prev
	}
	if element.prev != nil {
		element.prev.next = element.next
	}
	if element.next != nil {
		element.next.prev = element.prev
	}

	element = nil

	list.size--
}

// Check if values (one or more) are present in the set.
// All values have to be present in the set for the method to return true.
// Performance time complexity of n^2.
// Returns true if no arguments are passed at all, i.e. set is always super-set of empty set.
func (list *DLList) Contains(values ...interface{}) bool {

	if len(values) == 0 {
		return true
	}
	if list.size == 0 {
		return false
	}
	for _, value := range values {
		found := false
		for element := list.first; element != nil; element = element.next {
			if element.value == value {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}
	return true
}

// Returns all elements in the list.
func (list *DLList) Values() []interface{} {
	values := make([]interface{}, list.size, list.size)
	for e, element := 0, list.first; element != nil; e, element = e+1, element.next {
		values[e] = element.value
	}
	return values
}

// Returns true if list does not contain any elements.
func (list *DLList) Empty() bool {
	return list.size == 0
}

// Returns number of elements within the list.
func (list *DLList) Size() int {
	return list.size
}

// Removes all elements from the list.
func (list *DLList) Clear() {
	list.size = 0
	list.first = nil
	list.last = nil
}

// Sorts values (in-place) using timsort.
func (list *DLList) Sort(comparator Comparator) {

	if list.size < 2 {
		return
	}

	values := list.Values()
	Sort(values, comparator)

	list.Clear()

	list.Add(values...)

}

// Swaps values of two elements at the given indices.
func (list *DLList) Swap(i, j int) {
	if list.withinRange(i) && list.withinRange(j) && i != j {
		var element1, element2 *dllElement
		for e, currentElement := 0, list.first; element1 == nil || element2 == nil; e, currentElement = e+1, currentElement.next {
			switch e {
			case i:
				element1 = currentElement
			case j:
				element2 = currentElement
			}
		}
		element1.value, element2.value = element2.value, element1.value
	}
}

func (list *DLList) String() string {
	str := "DoublyLinkedList\n"
	values := []string{}
	for element := list.first; element != nil; element = element.next {
		values = append(values, fmt.Sprintf("%v", element.value))
	}
	str += strings.Join(values, ", ")
	return str
}

// Check that the index is withing bounds of the list
func (list *DLList) withinRange(index int) bool {
	return index >= 0 && index < list.size && list.size != 0
}

// ****************************************************************************
// DOUBLY LINKED LIST END *****************************************************
// ****************************************************************************
