package main

import (
	"fmt"
	"strings"
)

// ****************************************************************************
// LINKED LIST STACK **********************************************************
// ****************************************************************************

func assertLLSTackInterfaceImplementation() {
	var _ StackInterface = (*LLStack)(nil)
}

type LLStack struct {
	list *SLList
}

// Instantiates a new empty stack
func NewLLStack() *LLStack {
	return &LLStack{list: &SLList{}}
}

// Pushes a value onto the top of the stack
func (stack *LLStack) Push(value interface{}) {
	stack.list.Prepend(value)
}

// Pops (removes) top element on stack and returns it, or nil if stack is empty.
// Second return parameter is true, unless the stack was empty and there was nothing to pop.
func (stack *LLStack) Pop() (value interface{}, ok bool) {
	value, ok = stack.list.Get(0)
	stack.list.Remove(0)
	return
}

// Returns top element on the stack without removing it, or nil if stack is empty.
// Second return parameter is true, unless the stack was empty and there was nothing to peek.
func (stack *LLStack) Peek() (value interface{}, ok bool) {
	return stack.list.Get(0)
}

// Returns true if stack does not contain any elements.
func (stack *LLStack) Empty() bool {
	return stack.list.Empty()
}

// Returns number of elements within the stack.
func (stack *LLStack) Size() int {
	return stack.list.Size()
}

// Removes all elements from the stack.
func (stack *LLStack) Clear() {
	stack.list.Clear()
}

// Returns all elements in the stack (LIFO order).
func (stack *LLStack) Values() []interface{} {
	return stack.list.Values()
}

func (stack *LLStack) String() string {
	str := "LinkedListStack\n"
	values := []string{}
	for _, value := range stack.list.Values() {
		values = append(values, fmt.Sprintf("%v", value))
	}
	str += strings.Join(values, ", ")
	return str
}

// ****************************************************************************
// LINKED LIST STACK END ******************************************************
// ****************************************************************************
