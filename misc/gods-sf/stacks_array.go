package main

import (
	"fmt"
	"strings"
)

// ****************************************************************************
// ARRAY STACK ****************************************************************
// ****************************************************************************

func assertArrayStackInterfaceImplementation() {
	var _ StackInterface = (*ArrayStack)(nil)
}

type ArrayStack struct {
	list *ArrayList
}

// Instantiates a new empty stack
func NewArrayStack() *ArrayStack {
	return &ArrayStack{list: NewArrayList()}
}

// Pushes a value onto the top of the stack
func (stack *ArrayStack) Push(value interface{}) {
	stack.list.Add(value)
}

// Pops (removes) top element on stack and returns it, or nil if stack is empty.
// Second return parameter is true, unless the stack was empty and there was nothing to pop.
func (stack *ArrayStack) Pop() (value interface{}, ok bool) {
	value, ok = stack.list.Get(stack.list.Size() - 1)
	stack.list.Remove(stack.list.Size() - 1)
	return
}

// Returns top element on the stack without removing it, or nil if stack is empty.
// Second return parameter is true, unless the stack was empty and there was nothing to peek.
func (stack *ArrayStack) Peek() (value interface{}, ok bool) {
	return stack.list.Get(stack.list.Size() - 1)
}

// Returns true if stack does not contain any elements.
func (stack *ArrayStack) Empty() bool {
	return stack.list.Empty()
}

// Returns number of elements within the stack.
func (stack *ArrayStack) Size() int {
	return stack.list.Size()
}

// Removes all elements from the stack.
func (stack *ArrayStack) Clear() {
	stack.list.Clear()
}

// Returns all elements in the stack (LIFO order).
func (stack *ArrayStack) Values() []interface{} {
	size := stack.list.Size()
	elements := make([]interface{}, size, size)
	for i := 1; i <= size; i++ {
		elements[size-i], _ = stack.list.Get(i - 1) // in reverse (LIFO)
	}
	return elements
}

func (stack *ArrayStack) String() string {
	str := "ArrayStack\n"
	values := []string{}
	for _, value := range stack.list.Values() {
		values = append(values, fmt.Sprintf("%v", value))
	}
	str += strings.Join(values, ", ")
	return str
}

// ****************************************************************************
// ARRAY STACK END ************************************************************
// ****************************************************************************
