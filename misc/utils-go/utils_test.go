package utils

import (
	"reflect"
	"testing"
)

func TestIntSet(t *testing.T) {
	type testpair struct {
		arr []int
		res map[int]bool
	}

	var tests = []testpair{
		{[]int{1, 1, 3, 4}, map[int]bool{1: true, 3: true, 4: true}},
	}

	for _, pair := range tests {
		res := intSet(pair.arr)
		if !reflect.DeepEqual(res, pair.res) {
			t.Error(
				"For", pair.arr,
				"expected", pair.res,
				"got", res,
			)
		}
	}
}

func TestSet(t *testing.T) {
	type testpair struct {
		arr []int
		res map[int]bool
	}

	var tests = []testpair{
		{[]int{1, 1, 3, 4}, map[int]bool{1: true, 3: true, 4: true}},
	}

	for _, pair := range tests {
		res := set(pair.arr).(map[int]bool)
		if !reflect.DeepEqual(res, pair.res) {
			t.Error(
				"For", pair.arr,
				"expected", pair.res,
				"got", res,
			)
		}
	}
}

func TestRemoveDuplicates(t *testing.T) {
	type testpair struct {
		arr []int
		res []int
	}

	var tests = []testpair{
		{[]int{1, 1, 3, 4}, []int{1, 3, 4}},
	}

	for _, pair := range tests {
		res := removeDuplicates(pair.arr)
		if !reflect.DeepEqual(res, pair.res) {
			t.Error(
				"For", pair.arr,
				"expected", pair.res,
				"got", res,
			)
		}
	}
}

func TestPrimes(t *testing.T) {
	type testpair struct {
		n   int64
		res []int64
	}

	var tests = []testpair{
		{0, []int64{}},
		{2, []int64{2}},
		{7, []int64{2, 3, 5, 7}},
		{10, []int64{2, 3, 5, 7}},
	}

	var isEq = func(a, b []int64) bool {
		if len(a) != len(b) {
			return false
		}
		for i, v := range a {
			if v != b[i] {
				return false
			}
		}
		return true
	}

	for _, pair := range tests {
		res := primes(pair.n)
		if !isEq(res, pair.res) {
			t.Error(
				"For", pair.n,
				"expected", pair.res,
				"got", res,
			)
		}
	}
}

func TestIsPrime(t *testing.T) {
	type testpair struct {
		n   int64
		res bool
	}

	var tests = []testpair{
		{int64(2), true},
		{int64(3), true},
		{int64(4), false},
		{int64(10), false},
	}

	for _, pair := range tests {
		res := isPrime(pair.n)
		if res != pair.res {
			t.Error(
				"For", pair.n,
				"expected", pair.res,
				"got", res,
			)
		}
	}
}

func TestFactorial(t *testing.T) {
	type testpair struct {
		n   int
		res int64
	}

	var tests = []testpair{
		{1, 1},
		{2, 2},
		{5, 120},
	}

	for _, pair := range tests {
		res := factorial(pair.n)
		if res != pair.res {
			t.Error(
				"For", pair.n,
				"expected", pair.res,
				"got", res,
			)
		}
	}
}

func TestCombinations(t *testing.T) {
	type testpair struct {
		n   int
		r   int
		res int64
	}

	var tests = []testpair{
		{1, 0, 1},
		{2, 0, 1},
		{5, 1, 5},
		{5, 3, 10},
	}

	for _, pair := range tests {
		res := combinations(pair.n, pair.r)
		if res != pair.res {
			t.Error(
				"For", pair.n, ",", pair.r,
				"expected", pair.res,
				"got", res,
			)
		}
	}
}

func TestPartIndex(t *testing.T) {
	type testpair struct {
		a   []int
		b   []int
		res int
	}

	var tests = []testpair{
		{[]int{1, 2, 3, 4}, []int{2, 3}, 1},
	}

	for _, pair := range tests {
		res := partIndex(pair.a, pair.b, 0)
		if res != pair.res {
			t.Error(
				"For", pair.a, ",", pair.b,
				"expected", pair.res,
				"got", res,
			)
		}
	}
}

func TestPartIndexAll(t *testing.T) {
	type testpair struct {
		a   []int
		b   []int
		res []int
	}

	var tests = []testpair{
		{[]int{1, 2, 3, 4, 3, 4}, []int{3, 4}, []int{2, 4}},
	}

	for _, pair := range tests {
		res := partIndexAll(pair.a, pair.b, 0)
		if !reflect.DeepEqual(res, pair.res) {
			t.Error(
				"For", pair.a, ",", pair.b,
				"expected", pair.res,
				"got", res,
			)
		}
	}
}

func TestBinarySearch(t *testing.T) {
	type testpair struct {
		v   int
		arr []int
		res int
	}

	var tests = []testpair{
		{2, []int{2}, 0},
		{3, []int{2, 3}, 1},
	}

	for _, pair := range tests {
		res := binarySearch(pair.v, pair.arr)
		if res != pair.res {
			t.Error(
				"For", pair.v, pair.arr,
				"expected", pair.res,
				"got", res,
			)
		}
	}
}

func TestFindSide(t *testing.T) {
	type testpair struct {
		start, end, p *point
		res           int
	}

	var tests = []testpair{
		{&point{1, 1}, &point{2, 2}, &point{3, 3}, 0},
		{&point{1, 1}, &point{2, 2}, &point{3, 2}, 1},
		{&point{1, 1}, &point{2, 2}, &point{2, 3}, -1},
		{&point{1, 1}, &point{1, 2}, &point{2, 3}, 1},
		{&point{10, 10}, &point{10, 0}, &point{9, 1}, 1},
		{&point{10, 0}, &point{10, 10}, &point{9, 1}, -1},
	}

	for _, pair := range tests {
		res := findSide(pair.start, pair.end, pair.p)
		if res != pair.res {
			t.Error(
				"For", pair.start, pair.end, pair.p,
				"expected", pair.res,
				"got", res,
			)
		}
	}
}

func TestFindGcd(t *testing.T) {
	type testpair struct {
		a, b int64
		res  int64
	}
	var tests = []testpair{
		{2, 3, 1},
		{2, 2, 2},
		{10, 2, 2},
		{10, 3, 1},
	}

	for _, pair := range tests {
		res := gcd(pair.a, pair.b)
		if res != pair.res {
			t.Error(
				"For", pair.a, pair.b,
				"expected", pair.res,
				"got", res,
			)
		}
	}
}

func TestConvexHull(t *testing.T) {
	type testpair struct {
		points []*point
		res    []*point
	}

	tests := []testpair{}
	tests = append(tests, testpair{
		[]*point{&point{1, 1}, &point{2, 2}, &point{3, 1}, &point{2, 0}},
		[]*point{&point{1, 1}, &point{2, 2}, &point{3, 1}, &point{2, 0}},
	})
	tests = append(tests, testpair{
		[]*point{&point{1, 1}, &point{2, 2}, &point{3, 1}, &point{2, 1}},
		[]*point{&point{1, 1}, &point{2, 2}, &point{3, 1}, &point{2, 1}},
	})
	tests = append(tests, testpair{
		[]*point{&point{1, 1}, &point{2, 3}, &point{3, 1}, &point{2, 2}},
		[]*point{&point{1, 1}, &point{2, 3}, &point{3, 1}},
	})
	tests = append(tests, testpair{
		[]*point{&point{0, 0}, &point{10, 0}, &point{10, 10}, &point{9, 1}},
		[]*point{&point{0, 0}, &point{10, 10}, &point{10, 0}},
	})
	tests = append(tests, testpair{
		[]*point{&point{0, 0}, &point{10, 0}, &point{10, 10}, &point{0, 10}, &point{5, 5}},
		[]*point{&point{0, 0}, &point{0, 10}, &point{10, 10}, &point{10, 0}},
	})
	tests = append(tests, testpair{
		[]*point{&point{0, 0}, &point{5, 0}, &point{10, 0}, &point{0, 5}, &point{5, 5}, &point{10, 5}, &point{0, 10}, &point{5, 10}, &point{10, 10}},
		[]*point{&point{0, 0}, &point{0, 5}, &point{0, 10}, &point{5, 10}, &point{10, 10}, &point{10, 5}, &point{10, 0}, &point{5, 0}},
	})

	for _, pair := range tests {
		res := convexHull(pair.points)
		if !reflect.DeepEqual(res, pair.res) {
			t.Error(
				"For", strPoints(pair.points),
				"expected", strPoints(pair.res),
				"got", strPoints(res),
			)
		}
	}
}
