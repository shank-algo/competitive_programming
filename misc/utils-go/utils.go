package utils

import (
	"fmt"
	"math"
	"sort"
	"strconv"
	"strings"
)

// CUSTOM TYPE DEFINITIONS START////////////////////////////////////////////////

type pi [2]int     // pair of int
type pi64 [2]int64 // pair of int64

type vi []int                     //vector of int
func (vec vi) Less(i, j int) bool { return vec[i] < vec[j] }
func (vec vi) Swap(i, j int)      { vec[i], vec[j] = vec[j], vec[i] }
func (vec vi) Len() int           { return len(vec) }

type vi64 []int64                   // vector of int64
func (vec vi64) Less(i, j int) bool { return vec[i] < vec[j] }
func (vec vi64) Swap(i, j int)      { vec[i], vec[j] = vec[j], vec[i] }
func (vec vi64) Len() int           { return len(vec) }

type E interface{} // the empty interface

// INTSET START ////////////////////////////////////////////////////////////

type IntSet map[int]struct{}

func IntSetFromArr(arr []int) IntSet {
	res := make(IntSet)
	for _, i := range arr {
		res.Add(i)
	}
	return res
}

func (s *IntSet) Any() int {
	for i, _ := range *s {
		return i
	}
	panic("calling Any() on empty set")
}

func (s *IntSet) Keys() []int {
	res := []int{}
	for i, _ := range *s {
		res = append(res, i)
	}
	return res
}

func (s *IntSet) Len() int {
	return len(s.Keys())
}

func (s *IntSet) Add(i int) {
	(*s)[i] = struct{}{}
}

func (s *IntSet) Contains(i int) bool {
	if _, found := (*s)[i]; found {
		return true
	}
	return false
}

func (s *IntSet) Remove(i int) {
	if _, found := (*s)[i]; found {
		delete(*s, i)
	}
}

func (s *IntSet) Diff(s2 *IntSet) IntSet {
	res := make(IntSet)
	for i, _ := range *s {
		if !s2.Contains(i) {
			res[i] = struct{}{}
		}
	}
	return res
}

// INTSET END //////////////////////////////////////////////////////////////

// CUSTOM TYPE DEFINITIONS END /////////////////////////////////////////////////

// MISC UTILS

// PRIMES

// `primes` returns the primes numbers less than or equal to `n`
// using Sieve of Eratosthenes
func primes(N int64) []int64 {
	_ = "breakpoint"

	if N <= 1 {
		return []int64{}
	}

	// create the numbers array
	num := []int64{}
	isPrime := []bool{}
	for i := int64(0); i < N+1; i++ {
		num = append(num, i)
		isPrime = append(isPrime, true)
	}
	isPrime[0] = false
	isPrime[1] = false

	res := []int64{}
	// iterate over the numbers
	// for each number that is prime, mark all its multiples as non-prime
	for i, n := range num {
		if isPrime[i] {
			res = append(res, n)
			for cn := 2 * n; cn < N+1; cn += n {
				isPrime[cn] = false
			}
		}
	}

	return res
}

// checks if the given number is prime
func isPrime(n interface{}) bool {
	switch n.(type) {
	case int, int32:
		v := n.(int)
		if v == 2 || v == 3 {
			return true
		}
		root := int(math.Floor(math.Sqrt(float64(v))))
		for d := 2; d <= root; d++ {
			if v%d == 0 {
				return false
			}
		}
		return true
	case int64:
		v := n.(int64)
		if v == int64(2) || v == int64(3) {
			return true
		}
		root := int64(math.Floor(math.Sqrt(float64(v))))
		for d := int64(2); d <= root; d++ {
			if v%d == 0 {
				return false
			}
		}
		return true
	default:
		panic("unknown type for isPrime()")
	}
}

// FACTORIAL

// `factorial` returns the factorial of `n`
func factorial(n int) int64 {
	res := int64(1)
	for i := int64(2); i <= int64(n); i++ {
		res *= i
	}
	return res
}

// COMBINATIONS

// `combinations` returns the number of combinations for given `n` and `r`
func combinations(n, r int) int64 {
	nr := int64(1)
	for i := int64(n); i > int64(n-r); i-- {
		nr *= i
	}
	dr := factorial(r)
	return nr / dr
}

// MISC

// `pp2DStrArr` pretty prints a 2D string array
func pp2DStrArr(data [][]string) {
	for i := 0; i < len(data); i++ {
		fmt.Println(data[i])
	}

}

// `pp2DIntArr` pretty prints a 2D int array
func pp2DIntArr(data [][]int) {
	for i := 0; i < len(data); i++ {
		fmt.Println(data[i])
	}
}

// max value from array
func max(arr []int) int {
	res := arr[0]
	for _, v := range arr {
		res = maxOf(res, v)
	}
	return res
}

func maxOf(a, b int) int {
	if a < b {
		return b
	}
	return a
}

// min value from array
func min(arr []int) int {
	res := arr[0]
	for _, v := range arr {
		res = minOf(res, v)
	}
	return res
}

func minOf(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func twoDimArr(n, m int, def interface{}) interface{} {
	switch def.(type) {
	case int:
		res := [][]int{}
		v := def.(int)
		for i := 0; i < n; i++ {
			row := []int{}
			for j := 0; j < m; j++ {
				row = append(row, v)
			}
			res = append(res, row)
		}
		return res
	case int64:
		res := [][]int64{}
		v := def.(int64)
		for i := 0; i < n; i++ {
			row := []int64{}
			for j := 0; j < m; j++ {
				row = append(row, v)
			}
			res = append(res, row)
		}
		return res

	case string:
		res := [][]string{}
		v := def.(string)
		for i := 0; i < n; i++ {
			row := []string{}
			for j := 0; j < m; j++ {
				row = append(row, v)
			}
			res = append(res, row)
		}
		return res

	default:
		panic("type not suppported for twoDimArr")
	}
}

func intSet(arr []int) map[int]bool {
	res := make(map[int]bool)
	for _, v := range arr {
		if _, found := res[v]; !found {
			res[v] = true
		}
	}
	return res
}

func set(arr interface{}) interface{} {
	switch arr.(type) {
	case []int:
		res := make(map[int]bool)
		arr := arr.([]int)
		for _, v := range arr {
			res[v] = true
		}
		return res
	default:
		panic("type not supported for set")
	}
}

func removeDuplicates(arr []int) []int {
	res := []int{}
	set := intSet(arr)
	for k, _ := range set {
		res = append(res, k)
	}
	return res
}

func convertSliceIntToStr(arr []int) []string {
	res := []string{}
	for _, i := range arr {
		res = append(res, strconv.Itoa(i))
	}
	return res
}

func partIndex(row, part []int, startIndex int) int {
	res := -1

	for i := startIndex; i < len(row); i++ {
		if len(row)-i < len(part) {
			// no point in searching the balance
			return -1
		}
		if row[i] == part[0] {
			match := true
			for j := 1; j < len(part); j++ {
				if row[i+j] != part[j] {
					match = false
					break
				}
			}
			if match {
				return i
			}
		}
	}

	return res
}

func partIndexAll(row, part []int, startIndex int) []int {
	res := []int{}

	for i := startIndex; i < len(row); i++ {
		if len(row)-i < len(part) {
			// no point in searching the balance
			return res
		}
		if row[i] == part[0] {
			match := true
			for j := 1; j < len(part); j++ {
				if row[i+j] != part[j] {
					match = false
					break
				}
			}
			if match {
				res = append(res, i)
			}
		}
	}

	return res
}

func contains(arr []int, val int) bool {
	for _, v := range arr {
		if v == val {
			return true
		}
	}
	return false
}

func reverse(s string) string {
	r := []rune(s)
	for i, j := 0, len(r)-1; i < j; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	return string(r)
}

func strCharSet(s string) map[string]struct{} {
	chars := strings.Split(s, "")
	m := make(map[string]struct{})
	for _, ch := range chars {
		m[ch] = struct{}{}
	}
	return m
}

func toStrArr(arr []int) []string {
	res := []string{}
	for _, i := range arr {
		res = append(res, strconv.Itoa(i))
	}
	return res
}

func binarySearch(v int, arr []int) (index int, found bool) {
	low := 0
	high := len(arr) - 1
	for low <= high {
		mid := (low + high) / 2
		switch {
		case v == arr[mid]:
			return mid, true
		case v < arr[mid]:
			high = mid - 1
		case v > arr[mid]:
			low = mid + 1
		}
	}
	return high, false
}

// GEOMETRY START //////////////////////////////////////////////////////////////

type point struct{ x, y float64 }

// for sorting points by x and y
type byXY []*point

func (points byXY) Len() int { return len(points) }
func (points byXY) Less(i, j int) bool {
	if points[i].x == points[j].x {
		return points[i].y < points[j].y
	}
	return points[i].x < points[j].x
}
func (points byXY) Swap(i, j int) {
	points[i], points[j] = points[j], points[i]
}

// returns
// -1 if p is to left (counterclockwise)
// 0 if p is in line with start-end
// 1 if p is to right (clockwise)
func findSide(start, end, p *point) int {
	// p is to left of start-end if slope(end-p) > slope(start-end)
	// slopeStartEnd = (y2-y1) / (x2-x1)
	// slopeEndPoint = (y3-y2) / (x3-x2)
	v := (end.y-start.y)*(p.x-end.x) - (p.y-end.y)*(end.x-start.x)
	switch {
	case v == 0:
		return 0
	case v < 0:
		return -1
	case v > 0:
		return 1
	}

	return int(v)
}

func strPoints(points []*point) string {
	s := []string{}
	for _, p := range points {
		s = append(s, fmt.Sprintf("%v", p))
	}
	return "[" + strings.Join(s, ", ") + "]"
}

// Uses the Monotone Chain Convex Hull algorithm to find the convex hull for
// the set of points provided
func convexHull(points []*point) []*point {
	// fmt.Println("points >>")
	// fmt.Println(strPoints(points))
	n := len(points)

	// there must be atleast 3 points
	if n <= 3 {
		return points
	}

	sort.Sort(byXY(points))
	// fmt.Println("sorted >>")
	// fmt.Println(strPoints(points))

	uHull := []*point{points[0], points[1]} // upper hull
	for i := 2; i < len(points); i++ {
		// fmt.Println("considering", points[i])
		uHull = append(uHull, points[i])
		// fmt.Println("uHull", strPoints(uHull))
		for len(uHull) >= 3 {
			side := findSide(uHull[len(uHull)-3], uHull[len(uHull)-2], uHull[len(uHull)-1])
			// fmt.Println("side", side)
			if side == -1 {
				// fmt.Println("removing", uHull[len(uHull)-2])
				uHull[len(uHull)-2] = uHull[len(uHull)-1]
				uHull[len(uHull)-1] = nil // or the zero value of T
				uHull = uHull[:len(uHull)-1]
			} else {
				break
			}
		}
	}
	// fmt.Println("Upper Hull >>")
	// fmt.Println(strPoints(uHull))

	lHull := []*point{points[len(points)-1], points[len(points)-2]} // lower hull
	for i := len(points) - 3; i >= 0; i-- {
		// fmt.Println("considering", points[i])
		lHull = append(lHull, points[i])
		// fmt.Println("lHull", strPoints(lHull))
		for len(lHull) >= 3 {
			side := findSide(lHull[len(lHull)-3], lHull[len(lHull)-2], lHull[len(lHull)-1])
			// fmt.Println("side", side)
			if side == -1 {
				// fmt.Println("removing", lHull[len(lHull)-2])
				lHull[len(lHull)-2] = lHull[len(lHull)-1]
				lHull[len(lHull)-1] = nil // or the zero value of T
				lHull = lHull[:len(lHull)-1]
			} else {
				break
			}
		}
	}
	// fmt.Println("Lower Hull")
	// fmt.Println(strPoints(lHull))

	return append(uHull, lHull[1:len(lHull)-1]...)
}
func containsPoint(points []*point, p *point) bool {
	for _, pt := range points {
		if pt.x == p.x && pt.y == p.y {
			return true
		}
	}
	return false
}

func distance(p1, p2 *point) float64 {
	return math.Sqrt(math.Pow(p2.x-p1.x, float64(2)) + math.Pow(p2.y-p1.y, 2))
}

// GEOMETRY END ////////////////////////////////////////////////////////////////

// returns -1 if f1 is less than f2
// returns 0 if f1 is less than f2
// returns 1 if f1 is greater than f2
func cmp(f1, f2, eps float64) int {
	// fmt.Printf("comparing %f and %f\n", f1, f2)
	if f1 == f2 || math.Abs(f1-f2) < eps {
		return 0
	}
	if f1 < f2 {
		return -1
	} else {
		return 1
	}
}

func digits(n int) []int {
	d := []int{}
	for n > 0 {
		d = append([]int{n % 10}, d...)
		n /= 10
	}
	return d
}

func gcd(a, b int64) int64 {
	if b > a {
		return gcd(b, a)
	} else if a%b == 0 {
		return b
	} else {
		return gcd(b, a%b)
	}
}

func powInt(x, y int) int {
	return int(math.Pow(float64(x), float64(y)))
}

func powInt64(x, y int64) int64 {
	return int64(math.Pow(float64(x), float64(y)))
}

func absInt(i int) int {
	if i < 0 {
		return -i
	} else {
		return i
	}
}

func absInt64(i int64) int64 {
	if i < int64(0) {
		return -i
	} else {
		return i
	}
}

func permutations(a []int) [][]int {
	// fmt.Println("finding permutations for", a)
	res := [][]int{}
	if len(a) == 1 {
		res = append(res, a)
		// fmt.Println("result", res)
		return res
	}
	last := a[len(a)-1]
	// put last at all the places for each permutation of a[:len(a)]
	prevPerms := permutations(a[:len(a)-1])
	var newPerm []int
	for _, prevPerm := range prevPerms {
		// fmt.Println("prevPerm", prevPerm)
		for i := 0; i < len(a); i++ {
			// fmt.Println("i", i)
			if i == 0 {
				newPerm = append([]int{last}, prevPerm...)
			} else if i == len(a)-1 {
				newPerm = append(prevPerm, last)
			} else {
				tmp := append(append([]int{}, prevPerm[:i]...), last)
				newPerm = append(tmp, prevPerm[i:]...)
			}
			// fmt.Println("newPerm", newPerm)
			res = append(res, newPerm)
		}
	}
	// fmt.Println("result", res)
	return res
}

func round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func toFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(round(num*output)) / output
}
