package stack

import (
	"reflect"
	"testing"
)

var resultStackGeneric int

func BenchmarkStackGeneric(b *testing.B) {
	st := New(reflect.TypeOf(1))
	var v int
	for i := 0; i < b.N; i++ {
		st.Push(1)
		v = st.Pop().(int)
	}
	resultStackGeneric = v
}

var resultStackInt int

func BenchmarkStackInt(b *testing.B) {
	st := make(StackInt, 0)
	var v int
	for i := 0; i < b.N; i++ {
		st.Push(1)
		v = st.Pop()
	}
	resultStackInt = v
}
