package stack

import (
	"errors"
	"reflect"
)

type Stack struct {
	T   reflect.Type
	arr []interface{}
}

func New(T reflect.Type) *Stack {
	return &Stack{T: T}
}

var TypeMismatchError = errors.New("new value does not match the type of the stack")
var EmptyStackError = errors.New("unable to Peek/Pop and empty stack")

// API
// Empty()
// Peek()
// Pop()
// Push(e)

func (s *Stack) Empty() bool {
	return len(s.arr) == 0
}

func (s *Stack) Peek() (interface{}, error) {
	if s.Empty() {
		return nil, EmptyStackError
	}
	return s.arr[len(s.arr)-1], nil
}

func (s *Stack) Pop() interface{} {
	var val interface{}
	val, s.arr = s.arr[len(s.arr)-1], s.arr[:len(s.arr)-1]
	return val
}

func (s *Stack) Push(e interface{}) (*Stack, bool, error) {
	if reflect.TypeOf(e) != s.T {
		return s, false, TypeMismatchError
	}
	s.arr = append(s.arr, e)
	return s, true, nil
}

type StackInt []int

func (s *StackInt) Empty() bool { return len(*s) == 0 }

func (s *StackInt) Peek() (int, error) {
	if s.Empty() {
		return 0, EmptyStackError
	}
	return (*s)[len(*s)-1], nil
}

func (s *StackInt) Pop() int {
	var res int
	res, (*s) = (*s)[len(*s)-1], (*s)[:len(*s)-1]
	return res
}

func (s *StackInt) Push(v int) (*StackInt, bool, error) {
	*s = append(*s, v)
	return s, true, nil
}
