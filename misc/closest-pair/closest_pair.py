import sys
from collections import namedtuple
from operator import itemgetter


Point = namedtuple('Point', ['x', 'y'])


infinity = float('inf')


def closest_pair(points):
    N = len(points)

    if N is 1:
        return (infinity, None, None)

    if N is 2:
        y = abs(points[1].y - points[0].y)
        x = abs(points[1].x - points[0].x)
        return (y**2 + x**2, points[0], points[1])

    res = (infinity, None, None)

    # in case all the points have the same x value
    if all(p.x == points[0].x for p in points):
        # find the shortest distance
        # this is possible as the points are already sorted by y.
        # so effectively this could be considered as a 1 D problem
        for i in range(N - 1):
            if abs(points[i].y - points[i + 1].y)**2 < res[0]:
                res = (
                    abs(points[i].y - points[i + 1].y)**2,
                    points[i],
                    points[i + 1]
                )
        return res

    x_mid = (points[0].x + points[-1].x) // 2
    left = [p for p in points if p.x <= x_mid]
    right = [p for p in points if p.x > x_mid]

    lmin = closest_pair(left)
    rmin = closest_pair(right)

    res = lmin if lmin[0] <= rmin[0] else rmin

    left_within_dmin = [p for p in left if (x_mid - p.x)**2 <= res[0]]
    right_within_dmin = [p for p in right if (p.x - x_mid)**2 <= res[0]]

    # for each point in left, only consider the points in right_within_dmin
    # also no need to consider points which have a y which differs by more
    # than dmin
    for p in left_within_dmin:
        for pr in right_within_dmin:
            if abs(pr.y - p.y)**2 > res[0]:
                continue
            ydiff = abs(pr.y - p.y)
            xdiff = abs(pr.x - p.x)
            d = ydiff**2 + xdiff**2
            if d < res[0]:
                res = (d, p, pr)

    return res

if __name__ == '__main__':
    points = []

    fn = sys.argv[1] if len(sys.argv) > 1 else 'data.txt'

    # read the data
    # with open('data_small.txt') as f:
    with open(fn) as f:
        N = int(f.readline().strip())

        for i in range(N):
            s = [int(v) for v in f.readline().strip().split()]
            points.append(Point(s[0], s[1]))

    # print('\n'.join('({}, {})'.format(p.x, p.y) for p in points))
    points.sort(key=itemgetter(0, 1))
    # print("Sorted")
    # print('\n'.join('({}, {})'.format(p.x, p.y) for p in points))

    print(closest_pair(points))
