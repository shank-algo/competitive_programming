import sys
from operator import itemgetter
from collections import namedtuple
import matplotlib.pyplot as plt

Point = namedtuple('Point', ['x', 'y'])


def orientation(p1, p2, p3):
    '''Returns the orientation of the triplet
    returns 1 in case the orientation is clockwise
    returns 0 in case the points are collinear
    returns -1 in case the orientation is counterclockwise
    '''
    # collinear
    if p1.x == p2.x == p3.x or p1.y == p2.y == p3.y:
        return 0

    if (p2.y - p1.y) * (p3.x - p2.x) - (p3.y - p2.y) * (p2.x - p1.x) > 0:
        return 1
    else:
        return -1


def next_in_hull(points, curr):
    next = None
    for op in points:
        if op == curr:
            continue
        if next is None:
            next = op
            continue
        if orientation(curr, next, op) is 1:  # counterclockwise
            next = op
            continue
    return next


def convex_hull(points):
    if len(points) < 3:
        raise AttributeError('Insufficient number of points to create a hull')

    if all((p.x == points[0].x for p in points)):
        raise ValueError(
            'All points are in a single line and cant form a covex hull')

    hull = []
    start = points[0]

    # add the first point to the hull
    hull.append(start)
    # find the first point with a x greater than

    # the leftmost and rightmost points are definitely in the hull
    curr = start
    while True:
        next = next_in_hull(points, curr)
        hull.append(next)
        curr = next
        if next == start:
            break

    return hull


if __name__ == '__main__':
    points = []

    fn = sys.argv[1] if len(sys.argv) > 1 else 'data.txt'

    # read the data
    with open(fn) as f:
        N = int(f.readline().strip())

        for i in range(N):
            s = [int(v) for v in f.readline().strip().split()]
            points.append(Point(s[0], s[1]))

    points.sort(key=itemgetter(0, 1))

    hull = convex_hull(points)

    plt.plot([p.x for p in points], [p.y for p in points], 'ro')
    plt.plot([p.x for p in hull], [p.y for p in hull], '-')
    plt.plot([p.x for p in [hull[0], hull[-1]]],
             [p.y for p in [hull[0], hull[-1]]], '-')

    plt.axis(
        [-2, max((p.x for p in points)) + 2, -2, max((p.y for p in points)) + 2])
    plt.show()
