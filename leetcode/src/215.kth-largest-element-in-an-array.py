#
# @lc app=leetcode id=215 lang=python3
#
# [215] Kth Largest Element in an Array
#
# https://leetcode.com/problems/kth-largest-element-in-an-array/description/
#
# algorithms
# Medium (46.65%)
# Total Accepted:    343.2K
# Total Submissions: 735.7K
# Testcase Example:  '[3,2,1,5,6,4]\n2'
#
# Find the kth largest element in an unsorted array. Note that it is the kth
# largest element in the sorted order, not the kth distinct element.
#
# Example 1:
#
#
# Input: [3,2,1,5,6,4] and k = 2
# Output: 5
#
#
# Example 2:
#
#
# Input: [3,2,3,1,2,4,5,5,6] and k = 4
# Output: 4
#
# Note:
# You may assume k is always valid, 1 ≤ k ≤ array's length.
#
#
class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:

        # [T] -> ([T], [T], [T])
        def partition(seq):
            pivot, arr = seq[0], seq[1:]
            lo = [x for x in arr if x < pivot]
            pivots = [x for x in arr if x == pivot]
            pivots.append(pivot)
            hi = [x for x in arr if x > pivot]
            return lo, pivots, hi

        # [T], int -> T
        def select(seq, k):
            lo, pivots, hi = partition(seq)
            m = len(hi)
            n = len(pivots)
            if m < k and k <= m + n:
                return pivots[0]
            elif k <= m:
                return select(hi, k)
            else:
                return select(lo, k - m - n)

        return select(nums, k)
