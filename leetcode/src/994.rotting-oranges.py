class Solution:
    def orangesRotting(self, grid: List[List[int]]) -> int:
        from collections import defaultdict, deque

        visited = set()
        to_visit = deque()
        to_visit_set = set()

        # create the graph
        g = defaultdict(set)
        m = len(grid)
        n = len(grid[0])
        fresh = 0
        for r in range(m):
            for c in range(n):
                if grid[r][c] == 0:
                    continue
                if grid[r][c] == 1:
                    fresh += 1

                for (dx, dy) in [(-1, 0), (1, 0), (0, 1), (0, -1)]:
                    x, y = c+dx, r+dy
                    if x >=0 and x < n and y >= 0 and y < m and grid[y][x] != 0:
                        g[(r, c)].add((y, x))
                        g[(y, x)].add((r, c))

        if fresh == 0:
            return 0

        # the initial rotten ones as the starting point
        for r in range(m):
            for c in range(n):
                if grid[r][c] == 2:
                    to_visit.append(((r, c), 0))
                    to_visit_set.add((r, c))



        # simulate using breadth first traversal
        while(fresh > 0 and len(to_visit) > 0):
            (cr, cc), step = to_visit.popleft()
            last_turn = step
            to_visit_set.remove((cr, cc))
            visited.add((cr, cc))
            if grid[cr][cc] == 1:
                fresh -= 1
            for (adjr, adjc) in g[(cr, cc)]:
                if (adjr, adjc) in visited or (adjr, adjc) in to_visit_set:
                    continue
                to_visit_set.add((adjr, adjc))
                to_visit.append(((adjr, adjc), step + 1))

        if fresh:
            return -1
        else:
            return last_turn
