use std::collections::HashSet;

impl Solution {
    pub fn longest_common_prefix(strs: Vec<String>) -> String {
        let mut result = "".to_string();
        let mut i = 0;
        loop{
            if strs.iter().all(|s| s.len() > i) && strs.iter().map(|s| s.chars().nth(i).unwrap()).collect::<HashSet<char>>().len() == 1{
                result.push(strs[0].chars().nth(i).unwrap());
                i += 1;
            } else {
                break;
            }
        }       
        return result;
    }

}