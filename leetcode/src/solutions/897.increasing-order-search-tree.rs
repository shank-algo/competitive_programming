/*
 * @lc app=leetcode id=897 lang=rust
 *
 * [897] Increasing Order Search Tree
 */

// @lc code=start
// Definition for a binary tree node.
// #[derive(Debug, PartialEq, Eq)]
// pub struct TreeNode {
//   pub val: i32,
//   pub left: Option<Rc<RefCell<TreeNode>>>,
//   pub right: Option<Rc<RefCell<TreeNode>>>,
// }
//
// impl TreeNode {
//   #[inline]
//   pub fn new(val: i32) -> Self {
//     TreeNode {
//       val,
//       left: None,
//       right: None
//     }
//   }
// }
use std::cell::RefCell;
use std::rc::Rc;
impl Solution {
    pub fn increasing_bst(root: Option<Rc<RefCell<TreeNode>>>) -> Option<Rc<RefCell<TreeNode>>> {
        if let Some(node) = root.clone() {
            let mut vals = Solution::walk(root.clone());
            vals.reverse();
            let fst = vals.pop().unwrap();
            *node.borrow_mut() = TreeNode::new(fst);
            node.borrow_mut().right = Solution::inc_bst(vals)
        }
        root
    }

    fn inc_bst(mut vals: Vec<i32>) -> Option<Rc<RefCell<TreeNode>>> {
        if vals.is_empty() {
            return None;
        }
        let v = vals.pop().unwrap();
        let node = Rc::new(RefCell::new(TreeNode::new(v)));
        node.borrow_mut().right = Solution::inc_bst(vals);
        Some(node)
    }

    fn walk(root: Option<Rc<RefCell<TreeNode>>>) -> Vec<i32> {
        let mut vals: Vec<i32> = vec![];
        if let Some(node) = root {
            let node = node.borrow();
            vals.extend(Solution::walk(node.left.clone()));
            vals.push(node.val);
            vals.extend(Solution::walk(node.right.clone()));
        }
        vals
    }
}
// @lc code=end
