#![allow(dead_code)]

impl Solution {
    pub fn next_permutation(nums: &mut Vec<i32>) {
        if nums.is_empty() {
            return;
        }

        let mut suffix_idx = nums.len() - 1;
        for i in (1..nums.len()).rev() {
            if nums[i - 1] >= nums[i] {
                suffix_idx = i - 1;
            } else {
                break;
            }
        }

        if suffix_idx != 0 {
            let pivot_idx = suffix_idx - 1;
            let mut to_swap_idx = nums.len();
            for i in (suffix_idx..nums.len()).rev() {
                if nums[pivot_idx] < nums[i] {
                    to_swap_idx = i;
                    break;
                }
            }
            nums.swap(pivot_idx, to_swap_idx);
        }

        nums[suffix_idx..].reverse();
    }
}

struct Solution;

#[test]
fn test_next_permutation() {
    let tests = [
        ("00001", "00010"),
        ("00100", "01000"),
        ("12345", "12354"),
        ("1000", "0001"),
    ];

    for (arg, expected) in tests.iter() {
        let mut arg: Vec<i32> = arg
            .chars()
            .map(|c| c.to_digit(10).unwrap() as i32)
            .collect();
        let expected: Vec<i32> = expected
            .chars()
            .map(|c| c.to_digit(10).unwrap() as i32)
            .collect();
        Solution::next_permutation(&mut arg);
        assert_eq!(arg, expected);
    }
}
