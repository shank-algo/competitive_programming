use std::cmp::max;

impl Solution {
    pub fn rob(nums: Vec<i32>) -> i32 {
        if nums.len() == 0 {
            return 0;
        }

        let mut cache: Vec<Option<i32>> = vec![None; nums.len()];
        Solution::best_memo(&mut cache[..], &nums, nums.len() - 1)
    }

    fn best_memo(cache: &mut [Option<i32>], nums: &[i32], at: usize) -> i32 {
        if cache[at].is_some() {
            return cache[at].unwrap();
        }
        if at == 0 {
            return nums[0];
        }
        if at == 1 {
            return max(nums[0], nums[1]);
        }

        let m = max(
            Solution::best_memo(cache, nums, at - 2) + nums[at],
            Solution::best_memo(cache, nums, at - 1),
        );
        cache[at] = Some(m);
        m
    }
}
