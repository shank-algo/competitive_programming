#![allow(dead_code)]

impl Solution {
    pub fn find_unsorted_subarray(nums: Vec<i32>) -> i32 {
        let n = nums.len();
        let mut start = 0;
        while start + 1 < n && nums[start + 1] > nums[start] {
            start = start + 1;
        }

        let mut end = n - 1;
        while end >= 1 && nums[end - 1] <= nums[end] {
            end = end - 1;
        }

        dbg!(&start);
        dbg!(&end);

        if start >= end {
            0
        } else {
            (end - start + 1) as i32
        }
    }
}

struct Solution;

#[test]
fn test_find_unsorted_array() {
    assert_eq!(0, Solution::find_unsorted_subarray([1, 2, 3, 4].to_vec()));
    assert_eq!(0, Solution::find_unsorted_subarray([1, 2, 3, 3].to_vec()));
    assert_eq!(3, Solution::find_unsorted_subarray([1, 3, 2, 2].to_vec()));
    assert_eq!(4, Solution::find_unsorted_subarray([3, 3, 2, 2].to_vec()));
}
