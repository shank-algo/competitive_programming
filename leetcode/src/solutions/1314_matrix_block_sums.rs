impl Solution {
    pub fn matrix_block_sum(mat: Vec<Vec<i32>>, k: i32) -> Vec<Vec<i32>> {
        let ku = k as usize;
        let rows = mat.len();
        let cols = mat[0].len();

        let mut sum_till: Vec<Vec<i32>> = vec![vec![0; cols]; rows];
        // first row
        sum_till[0][0] = mat[0][0];
        for c in 1..cols {
            sum_till[0][c] = sum_till[0][c - 1] + mat[0][c];
        }
        // first col
        for r in 1..rows {
            sum_till[r][0] = sum_till[r - 1][0] + mat[r][0];
        }
        // balance
        for r in 1..rows {
            for c in 1..cols {
                sum_till[r][c] =
                    mat[r][c] + sum_till[r - 1][c] + sum_till[r][c - 1] - sum_till[r - 1][c - 1];
            }
        }

        let mut answer: Vec<Vec<i32>> = vec![vec![0; cols]; rows];
        for r in 0..rows {
            for c in 0..cols {
                let rstart = if r < ku { 0 } else { r - ku };
                let rend = if r + ku > rows - 1 { rows - 1 } else { r + ku };
                let cstart = if c < ku { 0 } else { c - ku };
                let cend = if c + ku > cols - 1 { cols - 1 } else { c + ku };

                let top = if rstart > 0 {
                    sum_till[rstart - 1][cend]
                } else {
                    0
                };
                let left = if cstart > 0 {
                    sum_till[rend][cstart - 1]
                } else {
                    0
                };
                let top_left = if rstart > 0 && cstart > 0 {
                    sum_till[rstart - 1][cstart - 1]
                } else {
                    0
                };
                answer[r][c] = sum_till[rend][cend] - top - left + top_left;
            }
        }

        answer
    }
}
