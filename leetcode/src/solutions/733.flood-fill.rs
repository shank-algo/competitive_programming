/*
 * @lc app=leetcode id=733 lang=rust
 *
 * [733] Flood Fill
 */

// @lc code=start
use std::collections::{HashSet, VecDeque};

impl Solution {
    pub fn flood_fill(image: Vec<Vec<i32>>, sr: i32, sc: i32, new_color: i32) -> Vec<Vec<i32>> {
        let row_cnt = image.len();
        let col_cnt = image[0].len();

        let sr = sr as usize;
        let sc = sc as usize;
        let start_color = image[sr][sc];

        let mut result = image;

        let mut q: VecDeque<(usize, usize)> = VecDeque::new();
        q.push_back((sr, sc));

        let mut visited: HashSet<(usize, usize)> = HashSet::new();

        while !q.is_empty() {
            let (cr, cc) = q.pop_front().unwrap();
            if visited.contains(&(cr, cc)) {
                continue;
            } else {
                visited.insert((cr, cc));
            }

            result[cr][cc] = new_color;
            for &(nr, nc) in &[(cr + 1, cc), (cr - 1, cc), (cr, cc + 1), (cr, cc - 1)] {
                if 0 <= nr
                    && nr < row_cnt
                    && 0 <= nc
                    && nc < col_cnt
                    && result[nr][nc] == start_color
                {
                    q.push_back((nr, nc));
                }
            }
        }

        result
    }
}
// @lc code=end
