// Definition for singly-linked list.
// #[derive(PartialEq, Eq, Clone, Debug)]
// pub struct ListNode {
//   pub val: i32,
//   pub next: Option<Box<ListNode>>
// }
//
// impl ListNode {
//   #[inline]
//   fn new(val: i32) -> Self {
//     ListNode {
//       next: None,
//       val
//     }
//   }
// }
impl Solution {
    pub fn middle_node(head: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
        let mut len = 0;
        let mut curr = &head;
        while let Some(ref node) = *curr {
            len += 1;
            curr = &node.next;
        }

        let mid = len / 2;
        let mut idx = 0;
        curr = &head;
        loop {
            if idx == mid {
                return curr.clone();
            }
            idx += 1;
            curr = &curr.as_ref().unwrap().next;
        }
    }
}
