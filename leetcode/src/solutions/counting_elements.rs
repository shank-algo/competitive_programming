use std::collections::HashMap;

impl Solution {
    pub fn count_elements(arr: Vec<i32>) -> i32 {
        let mut counts: HashMap<i32, i32> = HashMap::new();
        arr.for_each(|i| {
            let cnt = counts.entry(i).or_default();
            *cnt += 1;
        });

        let mut result = 0;
        for (&i, &cnt) in &counts {
            if counts.contains(i + 1) {
                result += cnt;
            }
        }
        result
    }
}
