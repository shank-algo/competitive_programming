/*
 * @lc app=leetcode id=101 lang=rust
 *
 * [101] Symmetric Tree
 */

// @lc code=start
// Definition for a binary tree node.
// #[derive(Debug, PartialEq, Eq)]
// pub struct TreeNode {
//   pub val: i32,
//   pub left: Option<Rc<RefCell<TreeNode>>>,
//   pub right: Option<Rc<RefCell<TreeNode>>>,
// }
//
// impl TreeNode {
//   #[inline]
//   pub fn new(val: i32) -> Self {
//     TreeNode {
//       val,
//       left: None,
//       right: None
//     }
//   }
// }
use std::cell::RefCell;
use std::rc::Rc;
impl Solution {
    pub fn is_symmetric(root: Option<Rc<RefCell<TreeNode>>>) -> bool {
        if let Some(root_node) = root {
            let root_node = root_node.borrow();
            Solution::eq(
                root_node.left.clone(),
                Solution::rotate(root_node.right.clone()),
            )
        } else {
            true
        }
    }

    fn rotate(root: Option<Rc<RefCell<TreeNode>>>) -> Option<Rc<RefCell<TreeNode>>> {
        if let Some(node) = root.clone() {
            let left = node.borrow().left.clone();
            let right = node.borrow().right.clone();
            let new_left = Solution::rotate(right);
            let new_right = Solution::rotate(left);
            node.borrow_mut().left = new_left;
            node.borrow_mut().right = new_right;
        }
        root
    }

    fn eq(root1: Option<Rc<RefCell<TreeNode>>>, root2: Option<Rc<RefCell<TreeNode>>>) -> bool {
        match (root1, root2) {
            (None, None) => true,
            (Some(node1), Some(node2)) => {
                let node1 = node1.borrow();
                let node2 = node2.borrow();
                node1.val == node2.val
                    && Solution::eq(node1.left.clone(), node2.left.clone())
                    && Solution::eq(node1.right.clone(), node2.right.clone())
            }
            _ => false,
        }
    }
}
// @lc code=end
