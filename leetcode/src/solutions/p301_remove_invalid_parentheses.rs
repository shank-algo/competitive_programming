use std::collections::HashSet;

impl Solution {
    #[allow(unused)]
    pub fn remove_invalid_parentheses(s: String) -> Vec<String> {
        let chars: Vec<char> = s.chars().collect();
        let (l, r) = Self::find_invalid_counts(&chars);
        let mut result: HashSet<String> = HashSet::new();
        let mut curr: Vec<char> = Vec::new();
        Self::dfs(&chars, chars.len(), 0, l, r, 0, &mut curr, &mut result);
        result.iter().cloned().collect()
    }

    fn dfs(
        chars: &Vec<char>,
        n: usize,
        depth: usize,
        l: usize,
        r: usize,
        mut balance: isize,
        curr: &mut Vec<char>,
        result: &mut HashSet<String>,
    ) {
        for i in depth..n {
            if balance < 0 {
                return;
            }

            // case when we remove `i`th char
            if chars[i] == ')' && r > 0 {
                let curr_len = curr.len();
                Self::dfs(chars, n, i + 1, l, r - 1, balance, curr, result);
                curr.truncate(curr_len);
            } else if chars[i] == '(' && l > 0 {
                let curr_len = curr.len();
                Self::dfs(chars, n, i + 1, l - 1, r, balance, curr, result);
                curr.truncate(curr_len);
            }

            // case when we consider `i`th char
            curr.push(chars[i]);
            if chars[i] == ')' {
                balance -= 1;
            }
            if chars[i] == '(' {
                balance += 1;
            }
        }

        if balance == 0 && l == 0 && r == 0 {
            result.insert(curr.iter().cloned().collect());
        }
    }

    /// returns [invalid `)` count, invalid `(` count]
    fn find_invalid_counts(chars: &Vec<char>) -> (usize, usize) {
        let invalid = Self::find_invalid(chars);
        let mut l = 0;
        let mut r = 0;
        for c in invalid {
            if c == ')' {
                r += 1;
            }
            if c == '(' {
                l += 1;
            }
        }
        return (l, r);
    }

    fn find_invalid(chars: &Vec<char>) -> Vec<char> {
        let mut st: Vec<char> = Vec::new();
        for &c in chars.iter().filter(|&&ch| ch == ')' || ch == '(') {
            if st.is_empty() {
                st.push(c);
                continue;
            }
            if c == ')' && *st.last().unwrap() == '(' {
                st.pop();
                continue;
            } else {
                st.push(c);
            }
        }
        st
    }
}

struct Solution;

#[test]
fn test_remove_invalid_parentheses() {
    use std::collections::HashSet;

    let arg_exp = [
        (")(", vec![""]),
        ("(()", vec!["()"]),
        ("()())()", vec!["()()()", "(())()"]),
        ("(a)())()", vec!["(a)()()", "(a())()"]),
    ];

    for (arg, exp) in &arg_exp {
        let expected: HashSet<String> = exp.iter().map(|&s| s.to_owned()).collect();
        assert_eq!(
            expected,
            Solution::remove_invalid_parentheses(arg.to_string())
                .iter()
                .cloned()
                .collect::<HashSet<String>>()
        );
    }
}
