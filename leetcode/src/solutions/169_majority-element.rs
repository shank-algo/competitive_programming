/*
 * @lc app=leetcode id=169 lang=rust
 *
 * [169] Majority Element
 */

use std::collections::HashMap;

impl Solution {
    pub fn majority_element(nums: Vec<i32>) -> i32 {
        let n_items = &nums.len() / 2;

        let mut count: HashMap<i32, usize> = HashMap::new();

        for num in nums {
            if let Some(c) = count.get_mut(&num) {
                *c += 1;
            } else {
                count.insert(num, 1);
            }
        }

        count
            .iter()
            .find(|&(_, &c)| c > n_items)
            .map(|(&n, _)| n)
            .unwrap()
    }
}
