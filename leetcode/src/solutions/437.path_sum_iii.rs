// Definition for a binary tree node.
// #[derive(Debug, PartialEq, Eq)]
// pub struct TreeNode {
//   pub val: i32,
//   pub left: Option<Rc<RefCell<TreeNode>>>,
//   pub right: Option<Rc<RefCell<TreeNode>>>,
// }
//
// impl TreeNode {
//   #[inline]
//   pub fn new(val: i32) -> Self {
//     TreeNode {
//       val,
//       left: None,
//       right: None
//     }
//   }
//}
use std::cell::RefCell;
use std::rc::Rc;
impl Solution {
    pub fn path_sum(root: Option<Rc<RefCell<TreeNode>>>, sum: i32) -> i32 {
        Solution::paths(&root, sum).0
    }

    /// returns:
    /// i32 -> number of paths having a total of sum
    /// Vec<i32> -> vec of sums of paths starting at this node
    fn paths(root: &Option<Rc<RefCell<TreeNode>>>, sum: i32) -> (i32, Vec<i32>) {
        if let Some(node) = root {
            // recursion
            let node = node.borrow();
            let (left_cnt, left_paths) = Solution::paths(&node.left, sum);
            let (right_cnt, right_paths) = Solution::paths(&node.right, sum);
            let curr_paths: Vec<i32> = left_paths
                .iter()
                .chain(right_paths.iter())
                .map(|&c| c + node.val)
                .chain(vec![node.val].iter().cloned())
                .collect();
            let curr_cnt: i32 = curr_paths.iter().filter(|&&c| c == sum).count() as i32;

            (left_cnt + right_cnt + curr_cnt, curr_paths)
        } else {
            // base cases
            return (0, vec![]);
        }
    }
}
