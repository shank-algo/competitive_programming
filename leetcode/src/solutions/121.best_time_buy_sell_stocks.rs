impl Solution {
    pub fn max_profit(prices: Vec<i32>) -> i32 {
        // [7,1,5,3,6,4]
        let n = prices.len();
        if n <= 1 {
            return 0;
        }

        let mut mp = 0;
        let mut min_so_far = prices[0];
        for i in 1..n {
            if prices[i] <= min_so_far {
                min_so_far = prices[i];
                continue;
            }
            mp = std::cmp::max(mp, prices[i] - min_so_far);
        }

        mp
    }
}
