impl Solution {
    pub fn move_zeroes(nums: &mut Vec<i32>) {
        let n = nums.len();
        for i in 0..n - 1 {
            if nums[i] == 0 {
                // switch with the first non-zero value
                for j in i + 1..n {
                    if nums[j] != 0 {
                        nums[i] = nums[j];
                        nums[j] = 0;
                        break;
                    }
                }
            }
        }
    }
}
