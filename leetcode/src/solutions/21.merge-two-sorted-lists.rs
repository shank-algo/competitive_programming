impl Solution {
    pub fn merge_two_lists(
        mut l1: Option<Box<ListNode>>,
        mut l2: Option<Box<ListNode>>,
    ) -> Option<Box<ListNode>> {
        // Move values of l1 and l2 to the match.
        // From this point you won't be able to access values of l1 and l2 using l1,l2 :).
        match (l1, l2) {
            (Some(v1), None) => Some(v1), // l2 is empty, just value of l1 option.
            (None, Some(v2)) => Some(v2), // l1 is empty, just value of l2 option.
            (Some(mut v1), Some(mut v2)) => {
                if v1.val < v2.val {
                    let n = v1.next.take(); // Get v1.next node and set the "v1.next = None".
                    v1.next = Solution::merge_two_lists(n, Some(v2));
                    Some(v1)
                } else {
                    let n = v2.next.take(); // Get v2.next node and set the "v2.next = None".
                    v2.next = Solution::merge_two_lists(Some(v1), n);
                    Some(v2)
                }
            }
            _ => None,
        }
    }
}
