impl Solution {
    pub fn is_valid(s: String) -> bool {
        let mut st: Vec<char> = Vec::new();
        for br in s.chars() {
            match br {
                '{' | '(' | '[' => st.push(br),
                _ => {
                    if let Some(last) = st.pop() {
                        let required = match br {
                            '}' => '{',
                            ')' => '(',
                            ']' => '[',
                            _ => char::default(),
                        };
                        if last != required {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
            }
        }

        if st.is_empty() {
            return true;
        } else {
            return false;
        }
    }
}
