// Definition for a binary tree node.
// #[derive(Debug, PartialEq, Eq)]
// pub struct TreeNode {
//   pub val: i32,
//   pub left: Option<Rc<RefCell<TreeNode>>>,
//   pub right: Option<Rc<RefCell<TreeNode>>>,
// }
//
// impl TreeNode {
//   #[inline]
//   pub fn new(val: i32) -> Self {
//     TreeNode {
//       val,
//       left: None,
//       right: None
//     }
//   }
// }
use std::cell::RefCell;
use std::cmp::max;
use std::rc::Rc;
impl Solution {
    pub fn diameter_of_binary_tree(root: Option<Rc<RefCell<TreeNode>>>) -> i32 {
        if root.is_some() {
            let (_max_depth, max_diameter) = Solution::max_depth_diameter(&root);
            max_diameter
        } else {
            0
        }
    }

    fn max_depth_diameter(root: &Option<Rc<RefCell<TreeNode>>>) -> (i32, i32) {
        if let Some(node) = root {
            let node = node.borrow();

            let (left_depth, left_diameter) = Solution::max_depth_diameter(&node.left);
            let (right_depth, right_diameter) = Solution::max_depth_diameter(&node.right);

            let max_depth = 1 + max(left_depth, right_depth);
            let max_diameter = [left_diameter, right_diameter, left_depth + right_depth]
                .iter()
                .max()
                .unwrap()
                .clone();

            (max_depth, max_diameter)
        } else {
            (0, 0)
        }
    }
}
