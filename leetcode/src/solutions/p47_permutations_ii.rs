use std::collections::HashMap;

impl Solution {
    #[allow(unused)]
    pub fn permute_unique(nums: Vec<i32>) -> Vec<Vec<i32>> {
        let mut num_counts: HashMap<i32, usize> = HashMap::new();
        for &num in &nums {
            let cnt = num_counts.entry(num).or_default();
            *cnt += 1;
        }

        let mut uniques: Vec<i32> = Vec::with_capacity(num_counts.len());
        let mut counts: Vec<usize> = Vec::with_capacity(num_counts.len());
        for (num, count) in num_counts {
            uniques.push(num);
            counts.push(count);
        }

        let mut result: Vec<Vec<i32>> = Vec::new();
        let mut curr: Vec<i32> = Vec::new();
        Solution::perm_helper(
            &uniques[..],
            &mut counts,
            nums.len(),
            0,
            &mut curr,
            &mut result,
        );
        result.iter().cloned().collect()
    }

    fn perm_helper(
        uniques: &[i32],
        counts: &mut Vec<usize>,
        n: usize,
        depth: usize,
        curr: &mut Vec<i32>,
        result: &mut Vec<Vec<i32>>,
    ) {
        if depth == n {
            result.push(curr.clone());
            return;
        }

        for i in 0..uniques.len() {
            if counts[i] > 0 {
                counts[i] -= 1;
                curr.push(uniques[i]);
                Solution::perm_helper(uniques, counts, n, depth + 1, curr, result);
                curr.pop();
                counts[i] += 1;
            }
        }
    }
}

struct Solution;

#[test]
fn test_permute_unique() {
    use std::collections::HashSet;

    let expected: HashSet<Vec<i32>> = [vec![1, 1, 2], vec![1, 2, 1], vec![2, 1, 1]]
        .iter()
        .cloned()
        .collect();

    assert_eq!(
        expected,
        Solution::permute_unique(vec![1, 1, 2])
            .iter()
            .cloned()
            .collect::<HashSet<Vec<i32>>>()
    );
}
