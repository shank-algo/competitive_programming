/*
 * @lc app=leetcode id=136 lang=rust
 *
 * [136] Single Number
 */
use std::collections::HashMap;

impl Solution {
    pub fn single_number(nums: Vec<i32>) -> i32 {
        let mut numbers = HashMap::<i32, i32>::new();

        for i in nums {
            *numbers.entry(i).or_default() += 1;
        }

        numbers
            .iter()
            .find(|&(_, &times)| times == 1)
            .map(|(&num, _)| num)
            .unwrap()
    }
}
