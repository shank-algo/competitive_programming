impl Solution {
    pub fn read_binary_watch(num: i32) -> Vec<String> {
        let mut result = Vec::new();
        let num = num as u32;

        for h in 0..12_i32 {
            for m in 0..60_i32 {
                if h.count_ones() + m.count_ones() == num {
                    result.push(format!("{}:{:02}", h, m));
                }
            }
        }

        result
    }
}
