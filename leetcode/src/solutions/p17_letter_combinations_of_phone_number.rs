#![allow(dead_code)]

use std::collections::{HashMap, HashSet};

impl Solution {
    pub fn letter_combinations(digits: String) -> Vec<String> {
        if digits.is_empty() {
            return vec![];
        }

        let mut digit_letters: HashMap<char, Vec<char>> = HashMap::new();
        digit_letters.insert('2', "abc".chars().collect());
        digit_letters.insert('3', "def".chars().collect());
        digit_letters.insert('4', "ghi".chars().collect());
        digit_letters.insert('5', "jkl".chars().collect());
        digit_letters.insert('6', "mno".chars().collect());
        digit_letters.insert('7', "pqrs".chars().collect());
        digit_letters.insert('8', "tuv".chars().collect());
        digit_letters.insert('9', "wxyz".chars().collect());

        let mut used: HashSet<char> = HashSet::new();
        let mut result: Vec<String> = Vec::new();
        let mut curr: String = String::new();
        Solution::permutations_dfs(
            &digit_letters,
            &digits,
            digits.len(),
            digits.len(),
            0,
            &mut curr,
            &mut used,
            &mut result,
        );

        result
    }

    fn permutations_dfs(
        digit_letters: &HashMap<char, Vec<char>>,
        digits: &str,
        n: usize,
        k: usize,
        depth: usize,
        curr: &mut String,
        used: &mut HashSet<char>,
        result: &mut Vec<String>,
    ) {
        if depth == k {
            result.push(curr.to_owned());
            return;
        }

        let curr_digit = digits.chars().nth(depth).unwrap();
        for &c in &digit_letters[&curr_digit] {
            curr.push(c);
            Solution::permutations_dfs(digit_letters, digits, n, k, depth + 1, curr, used, result);
            curr.pop();
        }
    }
}

struct Solution;

#[test]
fn test_letter_combinations() {
    let expected: HashSet<&str> = ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"]
        .iter()
        .map(|s| s.as_ref())
        .collect();

    assert_eq!(
        expected,
        Solution::letter_combinations("23".to_string())
            .iter()
            .map(|s| s.as_ref())
            .collect()
    );
}
