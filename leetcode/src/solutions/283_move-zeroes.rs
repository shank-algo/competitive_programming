/*
 * @lc app=leetcode id=283 lang=rust
 *
 * [283] Move Zeroes
 *
 * https://leetcode.com/problems/move-zeroes/description/
 *
 * algorithms
 * Easy (54.69%)
 * Likes:    2188
 * Dislikes: 79
 * Total Accepted:    496.9K
 * Total Submissions: 907.4K
 * Testcase Example:  '[0,1,0,3,12]'
 *
 * Given an array nums, write a function to move all 0's to the end of it while
 * maintaining the relative order of the non-zero elements.
 *
 * Example:
 *
 *
 * Input: [0,1,0,3,12]
 * Output: [1,3,12,0,0]
 *
 * Note:
 *
 *
 * You must do this in-place without making a copy of the array.
 * Minimize the total number of operations.
 *
 */
impl Solution {
    pub fn move_zeroes(nums: &mut Vec<i32>) {
        for pos in 0..nums.len() {
            // if nums[pos] == 0, switch it with the first non-zero number
            // that comes after `pos`
            if nums[pos] == 0 {
                Solution::switch_zero(nums, pos);
            }
        }
    }

    fn switch_zero(nums: &mut Vec<i32>, pos: usize) {
        for i in pos + 1..nums.len() {
            if nums[i] != 0 {
                nums.swap(pos, i);
                return;
            }
        }
    }
}
