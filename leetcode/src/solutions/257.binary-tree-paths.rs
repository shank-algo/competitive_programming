/*
 * @lc app=leetcode id=257 lang=rust
 *
 * [257] Binary Tree Paths
 */

// @lc code=start
// Definition for a binary tree node.
// #[derive(Debug, PartialEq, Eq)]
// pub struct TreeNode {
//   pub val: i32,
//   pub left: Option<Rc<RefCell<TreeNode>>>,
//   pub right: Option<Rc<RefCell<TreeNode>>>,
// }
//
// impl TreeNode {
//   #[inline]
//   pub fn new(val: i32) -> Self {
//     TreeNode {
//       val,
//       left: None,
//       right: None
//     }
//   }
// }
use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

impl Solution {
    pub fn binary_tree_paths(root: Option<Rc<RefCell<TreeNode>>>) -> Vec<String> {
        let mut paths: Vec<String> = Vec::new();
        Solution::dfs(root, String::new(), &mut paths);
        paths
    }

    fn dfs(root: Option<Rc<RefCell<TreeNode>>>, current_path: String, paths: &mut Vec<String>) {
        if let Some(node) = root {
            let mut current_path = current_path;
            if !current_path.is_empty() {
                current_path += "->";
            }
            current_path += &node.val.to_string();
            let node = node.borrow();
            if node.left.is_none() && node.right.is_none() {
                paths.push(current_path.to_string());
            } else {
                Solution::dfs(node.left.clone(), current_path.clone(), paths);
                Solution::dfs(node.right.clone(), current_path.clone(), paths);
            }
        }
    }
}
// @lc code=end
