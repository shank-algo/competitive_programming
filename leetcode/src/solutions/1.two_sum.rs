use std::collections::HashMap;

impl Solution {
    pub fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
        let mut num_idx: HashMap<i32, usize> =
            nums.iter().enumerate().map(|(i, &n)| (n, i)).collect();
        for i in 0..nums.len() {
            if let Some(&idx) = num_idx.get(&(target - nums[i])) {
                if idx != i {
                    return vec![i as i32, idx as i32];
                }
            }
        }
        vec![]
    }
}
