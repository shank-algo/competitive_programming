/*
 * @lc app=leetcode id=771 lang=rust
 *
 * [771] Jewels and Stones
 */
use std::collections::HashSet;

impl Solution {
    pub fn num_jewels_in_stones(j: String, s: String) -> i32 {
        let mut jewels: HashSet<char> = j.chars().collect();

        s.chars()
            .map(|c| jewels.contains(&c))
            .filter(|r| *r == true)
            .count() as i32
    }
}
