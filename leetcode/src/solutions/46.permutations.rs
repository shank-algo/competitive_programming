impl Solution {
    pub fn permute(nums: Vec<i32>) -> Vec<Vec<i32>> {
        if nums.len() == 1 {
            return vec![nums];
        }

        Solution::permutations(&nums[..])
    }

    fn permutations(nums: &[i32]) -> Vec<Vec<i32>> {
        if nums.len() == 1 {
            return vec![nums.to_vec()];
        }

        let mut result = Vec::new();
        for permutation in Solution::permutations(&nums[1..]) {
            for i in 0..permutation.len() + 1 {
                let mut perm = permutation.clone();
                perm.insert(i, nums[0]);
                result.push(perm);
            }
        }
        result
    }
}
