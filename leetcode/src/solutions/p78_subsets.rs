#[allow(unused_imports)]
use std::collections::HashSet;

impl Solution {
    #[allow(unused)]
    pub fn subsets(nums: Vec<i32>) -> Vec<Vec<i32>> {
        let mut result: Vec<Vec<i32>> = Vec::new();
        let mut curr: Vec<i32> = Vec::new();
        Solution::subsets_helper(&nums[..], 0, &mut curr, &mut result);
        result
    }

    fn subsets_helper(nums: &[i32], i: usize, curr: &mut Vec<i32>, result: &mut Vec<Vec<i32>>) {
        result.push(curr.clone());

        for idx in i..nums.len() {
            curr.push(nums[idx]);
            Solution::subsets_helper(nums, idx + 1, curr, result);
            curr.pop();
        }
    }
}

struct Solution;

#[test]
fn test_subsets() {
    let expected: HashSet<Vec<i32>> = [
        vec![3],
        vec![1],
        vec![2],
        vec![1, 2, 3],
        vec![1, 3],
        vec![2, 3],
        vec![1, 2],
        vec![],
    ]
    .iter()
    .cloned()
    .collect();
    assert_eq!(
        expected,
        Solution::subsets(vec![1, 2, 3]).iter().cloned().collect()
    );
}
