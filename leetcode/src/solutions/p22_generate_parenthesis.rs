#![allow(dead_code)]

use std::collections::HashSet;

impl Solution {
    pub fn generate_parenthesis(n: i32) -> Vec<String> {
        Solution::perm_gen(n).into_iter().collect()
    }

    fn perm_gen(n: i32) -> HashSet<String> {
        if n == 1 {
            return ["()".to_owned()].iter().cloned().collect();
        }

        let mut result: HashSet<String> = HashSet::new();
        for perm in Solution::perm_gen(n - 1) {
            // prepend
            let mut to_add = perm.clone();
            to_add = "()".to_owned() + &to_add;
            result.insert(to_add);

            // append
            to_add = perm.clone();
            to_add += "()";
            result.insert(to_add);

            // enclose
            to_add = perm.clone();
            to_add = "(".to_owned() + &to_add;
            to_add += ")";
            result.insert(to_add);

            // at each position
            for i in 1..perm.len() {
                to_add = perm.clone();
                to_add.insert_str(i, "()");
                result.insert(to_add);
            }
        }
        result
    }
}

struct Solution;

#[test]
fn test_generate_parentheses() {
    let expected: HashSet<String> = [
        "((()))".to_owned(),
        "(()())".to_owned(),
        "(())()".to_owned(),
        "()(())".to_owned(),
        "()()()".to_owned(),
    ]
    .iter()
    .cloned()
    .collect();

    assert_eq!(
        Solution::generate_parenthesis(3)
            .iter()
            .cloned()
            .collect::<HashSet<String>>(),
        expected
    );
}
