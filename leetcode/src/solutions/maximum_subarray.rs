impl Solution {
    pub fn max_sub_array(nums: Vec<i32>) -> i32 {
        let mut max_sum: Vec<i32> = nums.clone();
        for i in 1..nums.len() {
            let sum_including_prev = max_sum[i - 1] + nums[i];
            max_sum[i] = std::cmp::max(max_sum[i], sum_including_prev);
        }
        *max_sum.iter().max().unwrap()
    }
}
