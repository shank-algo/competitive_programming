/*
 * @lc app=leetcode id=872 lang=rust
 *
 * [872] Leaf-Similar Trees
 */

// @lc code=start
// Definition for a binary tree node.
// #[derive(Debug, PartialEq, Eq)]
// pub struct TreeNode {
//   pub val: i32,
//   pub left: Option<Rc<RefCell<TreeNode>>>,
//   pub right: Option<Rc<RefCell<TreeNode>>>,
// }
//
// impl TreeNode {
//   #[inline]
//   pub fn new(val: i32) -> Self {
//     TreeNode {
//       val,
//       left: None,
//       right: None
//     }
//   }
// }
use std::cell::RefCell;
use std::rc::Rc;
impl Solution {
    pub fn leaf_similar(
        root1: Option<Rc<RefCell<TreeNode>>>,
        root2: Option<Rc<RefCell<TreeNode>>>,
    ) -> bool {
        let t1_leafs = Solution::leafs(root1);
        let t2_leafs = Solution::leafs(root2);
        t1_leafs == t2_leafs
    }

    fn leafs(root: Option<Rc<RefCell<TreeNode>>>) -> Vec<i32> {
        let mut leafs = vec![];
        if let Some(node) = root.clone() {
            let node = node.borrow();
            if node.left.is_none() && node.right.is_none() {
                leafs.push(node.val);
            } else {
                leafs.extend(Solution::leafs(node.left.clone()));
                leafs.extend(Solution::leafs(node.right.clone()));
            }
        }
        leafs
    }
}
// @lc code=end
