impl Solution {
    pub fn max_profit(prices: Vec<i32>) -> i32 {
        let mut have = false;
        let mut bought_at = 0;
        let mut profit = 0;
        for i in 0..prices.len() {
            if have {
                // sell
                if i == prices.len() - 1 || prices[i + 1] < prices[i] {
                    profit += prices[i] - bought_at;
                    have = false;
                }
            } else {
                // buy
                if i < prices.len() - 1 && prices[i + 1] > prices[i] {
                    bought_at = prices[i];
                    have = true;
                }
            }
        }
        profit
    }
}
