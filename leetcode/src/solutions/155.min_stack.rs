struct MinStack {
    min: Option<i32>,
    stack: Vec<i32>
}


/** 
 * `&self` means the method takes an immutable reference.
 * If you need a mutable reference, change it to `&mut self` instead.
 */
impl MinStack {

    /** initialize your data structure here. */
    fn new() -> Self {
        Self{min: None, stack: vec![]}
    }
    
    fn push(&mut self, x: i32) {
        self.stack.push(x);
        if self.min.is_none(){
            self.min = Some(x);
        }else{
            let m = self.min.unwrap();
            if x < m{
                self.min = Some(x);
            }
        }
    }
    
    fn pop(&mut self) {
        if let Some(v) = self.stack.pop(){
            if let Some(m) = self.min {
                if v == m{
                    self.min = self.stack.iter().min().cloned();
                }
            }
        }
    }
    
    fn top(&self) -> i32 {
        self.stack[self.stack.len()-1]
    }
    
    fn get_min(&self) -> i32 {
        self.min.unwrap()
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * let obj = MinStack::new();
 * obj.push(x);
 * obj.pop();
 * let ret_3: i32 = obj.top();
 * let ret_4: i32 = obj.get_min();
 */