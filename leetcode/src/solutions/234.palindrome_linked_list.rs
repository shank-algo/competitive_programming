// Definition for singly-linked list.
// #[derive(PartialEq, Eq, Clone, Debug)]
// pub struct ListNode {
//   pub val: i32,
//   pub next: Option<Box<ListNode>>
// }
//
// impl ListNode {
//   #[inline]
//   fn new(val: i32) -> Self {
//     ListNode {
//       next: None,
//       val
//     }
//   }
// }
impl Solution {
    pub fn is_palindrome(head: Option<Box<ListNode>>) -> bool {
        let mut head = head;
        let mut items: Vec<i32> = Vec::new();
        while let Some(mut node) = head {
            items.push(node.val);
            head = node.next;
        }

        let n = items.len();
        for i in 0..n / 2 {
            if items[i] != items[n - i - 1] {
                return false;
            }
        }

        true
    }
}
