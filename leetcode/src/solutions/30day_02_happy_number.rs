use std::collections::HashSet;

impl Solution {
    pub fn is_happy(n: i32) -> bool {
        let mut states: HashSet<i32> = HashSet::new();

        let mut n = n;
        let mut digits: Vec<i32> = Solution::digits(n);
        loop {
            let sq_sum = digits.iter().map(|&i| i * i).sum();
            if states.contains(&sq_sum) {
                return false;
            } else {
                states.insert(sq_sum);
                if sq_sum == 1 {
                    return true;
                }
                digits = Solution::digits(sq_sum);
            }
        }
    }

    fn digits(n: i32) -> Vec<i32> {
        let mut n = n;
        let mut result: Vec<i32> = Vec::new();
        while n > 9 {
            result.push(n % 10);
            n = n / 10;
        }
        result.push(n);
        result.reverse();
        result
    }
}
