#
# @lc app=leetcode id=300 lang=python3
#
# [300] Longest Increasing Subsequence
#
# https://leetcode.com/problems/longest-increasing-subsequence/description/
#
# algorithms
# Medium (40.40%)
# Total Accepted:    210.7K
# Total Submissions: 519.9K
# Testcase Example:  '[10,9,2,5,3,7,101,18]'
#
# Given an unsorted array of integers, find the length of longest increasing
# subsequence.
#
# Example:
#
#
# Input: [10,9,2,5,3,7,101,18]
# Output: 4
# Explanation: The longest increasing subsequence is [2,3,7,101], therefore the
# length is 4.
#
# Note:
#
#
# There may be more than one LIS combination, it is only necessary for you to
# return the length.
# Your algorithm should run in O(n^2) complexity.
#
#
# Follow up: Could you improve it to O(n log n) time complexity?
#
#
class Solution:
    def lengthOfLIS(self, nums: List[int]) -> int:
        if not nums: return 0

        L = [None] * len(nums)
        for i, n in enumerate(nums):
            # find the lenght of LIS ending at each integer.
            res = 1
            for j, pre in enumerate(nums[:i]):
                if pre < n:
                    res = max(res, 1 + L[j])
            L[i] = res

        # return the largest length
        return max(L)
