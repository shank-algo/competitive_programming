#
# @lc app=leetcode id=53 lang=python3
#
# [53] Maximum Subarray
#
# https://leetcode.com/problems/maximum-subarray/description/
#
# algorithms
# Easy (43.09%)
# Total Accepted:    496.5K
# Total Submissions: 1.1M
# Testcase Example:  '[-2,1,-3,4,-1,2,1,-5,4]'
#
# Given an integer array nums, find the contiguous subarray (containing at
# least one number) which has the largest sum and return its sum.
#
# Example:
#
#
# Input: [-2,1,-3,4,-1,2,1,-5,4],
# Output: 6
# Explanation: [4,-1,2,1] has the largest sum = 6.
#
#
# Follow up:
#
# If you have figured out the O(n) solution, try coding another solution using
# the divide and conquer approach, which is more subtle.
#
#
class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        if len(nums) == 0: return 0
        if len(nums) == 1: return nums[0]

        mid = len(nums) // 2
        left, right = nums[:mid], nums[mid:]
        left_result = self.maxSubArray(left)
        right_result = self.maxSubArray(right)

        mid_left_result, left_sum = nums[mid - 1], nums[mid - 1]
        for i in range(mid - 2, -1, -1):
            left_sum += nums[i]
            mid_left_result = max(mid_left_result, left_sum)
        mid_right_result, right_sum = nums[mid], nums[mid]
        for i in range(mid + 1, len(nums)):
            right_sum += nums[i]
            mid_right_result = max(mid_right_result, right_sum)
        mid_result = mid_left_result + mid_right_result

        return max(left_result, right_result, mid_result)
