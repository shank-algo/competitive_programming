#
# @lc app=leetcode id=23 lang=python3
#
# [23] Merge k Sorted Lists
#
# https://leetcode.com/problems/merge-k-sorted-lists/description/
#
# algorithms
# Hard (33.53%)
# Total Accepted:    360.1K
# Total Submissions: 1.1M
# Testcase Example:  '[[1,4,5],[1,3,4],[2,6]]'
#
# Merge k sorted linked lists and return it as one sorted list. Analyze and
# describe its complexity.
#
# Example:
#
#
# Input:
# [
# 1->4->5,
# 1->3->4,
# 2->6
# ]
# Output: 1->1->2->3->4->4->5->6
#
#
#
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None


class Solution:
    def mergeKLists(self, lists: List[ListNode]) -> ListNode:
        if len(lists) == 0: return None
        if len(lists) == 1: return lists[0]
        mid = len(lists) // 2
        left, right = lists[:mid], lists[mid:]
        left = self.mergeKLists(left)
        right = self.mergeKLists(right)
        return self.merge(left, right)

    # [T], [T] -> [T]
    # where T: ListNode
    def merge(self, left, right):
        if left is None or right is None:
            return left or right

        root = ListNode(0)
        current = root
        while left and right:
            if left.val <= right.val:
                current.next = left
                left = left.next
                current = current.next
            else:
                current.next = right
                right = right.next
                current = current.next

        if left: current.next = left
        if right: current.next = right

        return root.next
