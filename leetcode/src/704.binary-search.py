import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


# ( [int], int ) -> int
def binary_search(nums, target):
    lo, hi = 0, len(nums)
    while lo < hi:
        mid = (lo + hi) // 2
        if nums[mid] == target: return mid

        if nums[mid] < target: lo = mid + 1
        else: hi = mid
    return -1


def main():
    import json
    arr = json.loads(input())
    target = int(input())
    result = binary_search(arr, target)
    print(result)


main()

# ==============================================================

if debug_mode:
    inf.close()
