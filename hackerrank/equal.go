package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// INPUT TEMPLATE

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res := <-mi.lineChan
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

// INPUT TEMPLATE END

func main() {
	// f, _ := os.Open("equal.in")
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	t := mi.readInt()
	for caseNo := 1; caseNo <= t; caseNo++ {
		n := mi.readInt()
		c := mi.readInts()
		fmt.Println(solve(n, c))
	}
}

func solve(n int, c []int) int {
	// increasing the count of every others' chocolate by 1,2 or 5 except one of them
	// is equivalent of saying, take away the chocolates of one coworker by 1, 2 or 5 while keeping others' chocolate untouched.

	m := min(c)
	// fmt.Println("m >>", m)

	a := [5]int{}
	for i := range a {
		f := m - i
		counts := 0
		for _, v := range c {
			counts += opsCount(v, f)
		}
		a[i] = counts
	}

	return min(a[:])
}

// number of operations required to reduce n to v
func opsCount(n, f int) int {
	counts := 0
	counts += (n - f) / 5
	tmp := (n - f) % 5
	counts += tmp / 2
	tmp = tmp % 2
	counts += tmp
	return counts
}

// MISC
func min(arr []int) int {
	res := arr[0]
	for _, v := range arr {
		res = minOf(res, v)
	}
	return res
}

func minOf(a, b int) int {
	if a < b {
		return a
	}
	return b
}
