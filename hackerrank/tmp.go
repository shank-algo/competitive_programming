package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"math/big"
	"strconv"
	"strings"
)

// INPUT TEMPLATE START ////////////////////////////////////////////////////////

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res, ok := <-mi.lineChan
	if !ok {
		panic("trying to read from a closed channel")
	}
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readUint() uint {
	ui, err := strconv.ParseUint(mi.readLine(), 10, 32)
	if err != nil {
		panic(err)
	}
	return uint(ui)
}

func (mi *MyInput) readFloat() float32 {
	line := mi.readLine()
	f, err := strconv.ParseFloat(line, 32)
	if err != nil {
		panic(err)
	}
	return float32(f)
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readFloat64() float64 {
	f, err := strconv.ParseFloat(mi.readLine(), 64)
	if err != nil {
		panic(err)
	}
	return f
}

func (mi *MyInput) readBigInt() *big.Int {
	return mi.readBigIntToBase(10)
}

func (mi *MyInput) readBigIntToBase(base int) *big.Int {
	line := mi.readLine()
	var bi big.Int
	bi.SetString(line, base)
	return &bi
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readFloats() []float32 {
	res := []float32{}
	for _, s := range mi.readWords() {
		f, err := strconv.ParseFloat(s, 32)
		if err != nil {
			panic(err)
		}
		res = append(res, float32(f))
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readFloat64s() []float64 {
	res := []float64{}
	for _, s := range mi.readWords() {
		f, err := strconv.ParseFloat(s, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, f)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END //////////////////////////////////////////////////////////

// CUSTOM TYPE DEFINITIONS START////////////////////////////////////////////////

type E interface{} // the empty interface

type pi [2]int     // pair of int
type pi64 [2]int64 // pair of int64

type vi []int                     // vector of int
func (vec vi) Less(i, j int) bool { return vec[i] < vec[j] }
func (vec vi) Swap(i, j int)      { vec[i], vec[j] = vec[j], vec[i] }
func (vec vi) Len() int           { return len(vec) }

type vi64 []int64                   // vector of int64
func (vec vi64) Less(i, j int) bool { return vec[i] < vec[j] }
func (vec vi64) Swap(i, j int)      { vec[i], vec[j] = vec[j], vec[i] }
func (vec vi64) Len() int           { return len(vec) }

type vpi []pi // vector of int pairs

// INT SET - START

type iset map[int]struct{}

func newIset() *iset {
	s := make(iset)
	return &s
}

func (set *iset) AddArr(arr []int) *iset {
	for _, i := range arr {
		(*set)[i] = struct{}{}
	}
	return set
}

func (set *iset) Add(i int) *iset {
	(*set)[i] = struct{}{}
	return set
}

func (set *iset) Contains(i int) bool {
	if _, exists := (*set)[i]; exists {
		return true
	} else {
		return false
	}
}

func (set *iset) Remove(i int) *iset {
	delete((*set), i)
	return set
}

func (set *iset) Pop(i int) int {
	set.Remove(i)
	return i
}

func (set *iset) Clear() *iset {
	for i, _ := range *set {
		delete((*set), i)
	}
	return set
}

func (set *iset) Len() int {
	count := 0
	for _, _ = range *set {
		count++
	}
	return count
}

func (set *iset) Items() []int {
	res := []int{}
	for i, _ := range *set {
		res = append(res, i)
	}
	return res
}

func (set *iset) Update(set2 *iset) *iset {
	for i, _ := range *set2 {
		set.Add(i)
	}
	return set
}

func (set *iset) Subtract(set2 *iset) *iset {
	for i, _ := range *set2 {
		set.Remove(i)
	}
	return set
}

func (set *iset) Union(set2 *iset) *iset {
	return newIset().Update(set).Update(set2)
}

func (set *iset) Diff(set2 *iset) *iset {
	return newIset().Update(set).Subtract(set2)
}

// INT SET - END

const (
	INF     = math.MaxInt32
	INF64   = math.MaxInt64
	INFFL   = math.MaxFloat32
	INFFL64 = math.MaxFloat64
)

// CUSTOM TYPE DEFINITIONS END /////////////////////////////////////////////////

// OTHER MISC UTILS START //////////////////////////////////////////////////////

// max value from array
func max(values ...int) int {
	res := values[0]
	for _, v := range values {
		if res < v {
			res = v
		}
	}
	return res
}

// min value from array
func min(values ...int) int {
	res := values[0]
	for _, v := range values {
		if res > v {
			res = v
		}
	}
	return res
}

// equivalent to if a ? b :c
func ifelse(condition bool, iftrue, iffalse E) E {
	if condition {
		return iftrue
	} else {
		return iffalse
	}
}

// OTHER MISC UTILS END ////////////////////////////////////////////////////////

func main() {
	// f, _ := os.Open("equal.in")
	// defer f.Close()
	// mi := MyInput{rdr: f}
	// mi := MyInput{rdr: os.Stdin}

	arr := []int{1, 4, 6}
	set := newIset().AddArr(arr)
	fmt.Println("set", set)
	fmt.Println("contains 4", set.Contains(4))
	fmt.Println("add 7", set.Add(7))
	fmt.Println("remove 6", set.Remove(6))
	fmt.Println("clear", set.Clear())

	set2 := newIset().AddArr([]int{1, 20, 30})
	fmt.Println("set2", set2)
	fmt.Println("add set2 to set", set.Update(set2))

	set3 := newIset().AddArr([]int{99})
	fmt.Println("set3", set3)
	fmt.Println("union of set2 and set3", set2.Union(set3))

	set.AddArr(arr)
	fmt.Println("subtract set2 from set", set.Subtract(set2))

	fmt.Println("set items", set.Items())
	fmt.Println("set size", set.Len())
}
