defmodule Solution do
  def solve do
    IO.read(:stdio, :all)
    |> String.split
    |> Enum.map(&String.to_integer(&1))
    |> filter_even
    |> print_list
  end

  def filter_even(lst) do
    lst
    |> Enum.with_index
    |> Enum.filter_map(
        fn({_, i}) -> rem(i, 2) != 0 end, 
        fn{v, _} -> v end)
  end

  ## HELPERS

  def input do
    IO.read(:stdio, :line)
  end

  def input_loop(func) do
    case input() do
      :eof -> :ok
      data -> data |> String.trim |> func.()
              input_loop(func)
    end
  end

  def print_list([hd]), do: IO.puts hd
  def print_list([hd | tl]) do
    IO.puts hd
    print_list(tl)
  end
  
end

Solution.solve