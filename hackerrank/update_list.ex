defmodule Solution do
  def solve do
    IO.read(:stdio, :all)
    |> String.split
    |> Enum.map(&String.to_integer(&1))
    |> update_list
    |> print_list
  end

  def update_list(lst) do
    Enum.map(lst, &Kernel.abs(&1))
  end 

  def print_list(lst) do
    for e <- lst, do: IO.puts e
  end
end

Solution.solve