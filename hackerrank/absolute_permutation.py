import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def main():
    t = int(input().strip())
    for _cn in range(1, t + 1):
        n, k = list(map(int, input().strip().split()))
        if k == 0:
            print(" ".join(str(x) for x in range(1, n + 1)))
        elif n % (2 * k) != 0:
            print(-1)
        else:
            perm = []
            for i in range(1, n + 1, 2 * k):
                for j in range(k, 2 * k):
                    perm.append(i + j)
                for j in range(k):
                    perm.append(i + j)
            print(" ".join(str(x) for x in perm))


main()

# ==============================================================

if debug_mode:
    inf.close()