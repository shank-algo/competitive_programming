package main

import (
	"bufio"
	"fmt"
	"io"
	"math/big"
	"os"
	"strconv"
	"strings"
)

// CUSTOM TYPE DEFINITIONS START////////////////////////////////////////////////

type pi [2]int     // pair of int
type pi64 [2]int64 // pair of int64

type vi []int                     //vector of int
func (vec vi) Less(i, j int) bool { return vec[i] < vec[j] }
func (vec vi) Swap(i, j int)      { vec[i], vec[j] = vec[j], vec[i] }
func (vec vi) Len() int           { return len(vec) }

type vi64 []int64                   // vector of int64
func (vec vi64) Less(i, j int) bool { return vec[i] < vec[j] }
func (vec vi64) Swap(i, j int)      { vec[i], vec[j] = vec[j], vec[i] }
func (vec vi64) Len() int           { return len(vec) }

// CUSTOM TYPE DEFINITIONS END /////////////////////////////////////////////////

// INPUT TEMPLATE START ////////////////////////////////////////////////////////

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res, ok := <-mi.lineChan
	if !ok {
		panic("trying to read from a closed channel")
	}
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readUint() uint {
	ui, err := strconv.ParseUint(mi.readLine(), 10, 32)
	if err != nil {
		panic(err)
	}
	return uint(ui)
}

func (mi *MyInput) readFloat() float32 {
	line := mi.readLine()
	f, err := strconv.ParseFloat(line, 32)
	if err != nil {
		panic(err)
	}
	return float32(f)
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readFloat64() float64 {
	f, err := strconv.ParseFloat(mi.readLine(), 64)
	if err != nil {
		panic(err)
	}
	return f
}

func (mi *MyInput) readBigInt() *big.Int {
	return mi.readBigIntToBase(10)
}

func (mi *MyInput) readBigIntToBase(base int) *big.Int {
	line := mi.readLine()
	var bi big.Int
	bi.SetString(line, base)
	return &bi
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readFloats() []float32 {
	res := []float32{}
	for _, s := range mi.readWords() {
		f, err := strconv.ParseFloat(s, 32)
		if err != nil {
			panic(err)
		}
		res = append(res, float32(f))
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readFloat64s() []float64 {
	res := []float64{}
	for _, s := range mi.readWords() {
		f, err := strconv.ParseFloat(s, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, f)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END //////////////////////////////////////////////////////////

// OTHER MISC UTILS START //////////////////////////////////////////////////////

// max value from array
func max(arr []int) int {
	res := arr[0]
	for _, v := range arr {
		res = maxOf(res, v)
	}
	return res
}

func maxOf(a, b int) int {
	if a < b {
		return b
	}
	return a
}

// min value from array
func min(arr []int) int {
	res := arr[0]
	for _, v := range arr {
		res = minOf(res, v)
	}
	return res
}

func minOf(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// OTHER MISC UTILS END ////////////////////////////////////////////////////////

func main() {
	// f, _ := os.Open("equal.in")
	// defer f.Close()
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	t := mi.readInt()
	for caseNo := 1; caseNo <= t; caseNo++ {
		nm := mi.readInts()
		n, m := nm[0], nm[1]
		g := make(Graph)
		for i := 1; i <= n; i++ {
			g.AddNode(i)
		}
		// m lines for edges
		for i := 0; i < m; i++ {
			xy := mi.readInts()
			x, y := xy[0], xy[1]
			g.AddEdge(x, y)
		}
		// 1 line for starting node
		st := mi.readInt()
		fmt.Println(solve(&g, st))
	}
}

func solve(g *Graph, st int) string {
	res := []int{}
	for i := 0; i <= len(*g); i++ {
		res = append(res, -1)
	}
	visited := make(map[int]struct{})
	toVisit := []int{st}
	res[st] = 0
	var ind int
	for len(toVisit) > 0 {
		// fmt.Println("toVisit", toVisit)
		ind, toVisit = toVisit[0], toVisit[1:]
		// fmt.Println("level", level, "ind", ind, "toVisit", toVisit, "res", res)
		// time.Sleep(1 * time.Second)

		visited[ind] = struct{}{}

		for i, _ := range (*g)[ind] {
			if _, seen := visited[i]; !seen {
				if !contains(toVisit, i) {
					// fmt.Println("appending", i, "to toVisit[]")
					toVisit = append(toVisit, i)
					res[i] = res[ind] + 6
					// fmt.Println("toVisit", toVisit)
				}
			}
		}
	}

	return strings.Join(toStrArr(append(res[1:st], res[st+1:]...)), " ")
}

type Graph map[int]map[int]struct{}

func (g *Graph) AddNode(key int) *Graph {
	gr := *g
	if _, exists := gr[key]; !exists {
		gr[key] = make(map[int]struct{})
	}
	return g
}

func (g *Graph) AddEdge(a, b int) *Graph {
	// fmt.Println("adding edge between", a, b)
	gr := *g
	if _, existsA := gr[a]; !existsA {
		panic("unknown node " + string(a))
	}
	if _, existsB := gr[b]; !existsB {
		panic("unknown node " + string(b))
	}
	gr[a][b] = struct{}{}
	gr[b][a] = struct{}{}
	return g
}

func toStrArr(arr []int) []string {
	res := []string{}
	for _, i := range arr {
		res = append(res, strconv.Itoa(i))
	}
	return res
}

func contains(arr []int, val int) bool {
	for _, v := range arr {
		if v == val {
			return true
		}
	}
	return false
}
