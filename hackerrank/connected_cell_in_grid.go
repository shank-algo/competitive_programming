package main

import (
	"bufio"
	"fmt"
	"io"
	"math/big"
	"os"
	"strconv"
	"strings"
)

// CUSTOM TYPE DEFINITIONS START////////////////////////////////////////////////

type pi [2]int     // pair of int
type pi64 [2]int64 // pair of int64

type vi []int                     //vector of int
func (vec vi) Less(i, j int) bool { return vec[i] < vec[j] }
func (vec vi) Swap(i, j int)      { vec[i], vec[j] = vec[j], vec[i] }
func (vec vi) Len() int           { return len(vec) }

type vi64 []int64                   // vector of int64
func (vec vi64) Less(i, j int) bool { return vec[i] < vec[j] }
func (vec vi64) Swap(i, j int)      { vec[i], vec[j] = vec[j], vec[i] }
func (vec vi64) Len() int           { return len(vec) }

// CUSTOM TYPE DEFINITIONS END /////////////////////////////////////////////////

// INPUT TEMPLATE START ////////////////////////////////////////////////////////

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res, ok := <-mi.lineChan
	if !ok {
		panic("trying to read from a closed channel")
	}
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readUint() uint {
	ui, err := strconv.ParseUint(mi.readLine(), 10, 32)
	if err != nil {
		panic(err)
	}
	return uint(ui)
}

func (mi *MyInput) readFloat() float32 {
	line := mi.readLine()
	f, err := strconv.ParseFloat(line, 32)
	if err != nil {
		panic(err)
	}
	return float32(f)
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readFloat64() float64 {
	f, err := strconv.ParseFloat(mi.readLine(), 64)
	if err != nil {
		panic(err)
	}
	return f
}

func (mi *MyInput) readBigInt() *big.Int {
	return mi.readBigIntToBase(10)
}

func (mi *MyInput) readBigIntToBase(base int) *big.Int {
	line := mi.readLine()
	var bi big.Int
	bi.SetString(line, base)
	return &bi
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readFloats() []float32 {
	res := []float32{}
	for _, s := range mi.readWords() {
		f, err := strconv.ParseFloat(s, 32)
		if err != nil {
			panic(err)
		}
		res = append(res, float32(f))
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readFloat64s() []float64 {
	res := []float64{}
	for _, s := range mi.readWords() {
		f, err := strconv.ParseFloat(s, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, f)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END //////////////////////////////////////////////////////////

// OTHER MISC UTILS START //////////////////////////////////////////////////////

// max value from array
func max(arr []int) int {
	res := arr[0]
	for _, v := range arr {
		res = maxOf(res, v)
	}
	return res
}

func maxOf(a, b int) int {
	if a < b {
		return b
	}
	return a
}

// min value from array
func min(arr []int) int {
	res := arr[0]
	for _, v := range arr {
		res = minOf(res, v)
	}
	return res
}

func minOf(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// OTHER MISC UTILS END ////////////////////////////////////////////////////////

func main() {
	// f, _ := os.Open("equal.in")
	// defer f.Close()
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	m := mi.readInt()
	n := mi.readInt()
	mat := [][]int{}
	for i := 0; i < m; i++ {
		mat = append(mat, mi.readInts())
	}
	fmt.Println(solve(m, n, mat))
}

func solve(m, n int, mat [][]int) int {
	// maxN := 0

	mat2 := [][]int{}
	var row []int
	for i := 0; i <= n; i++ {
		row = append(row, 0)
	}
	mat2 = append(mat2, row) // first row of zeroes
	for r := 1; r <= m; r++ {
		mat2 = append(mat2, append([]int{0}, mat[r-1]...))
	}
	// pp2DIntArr(mat2)

	// create the graph from the matrix
	// number the cells as a single list, instead of the 2D representation
	g := make(Graph)

	// Add Nodes
	// m x n matrix
	// row 1 -> 1...n
	// row 2 -> n+1...2n
	// row m -> (m-1)n+1...mn
	for r := 1; r <= m; r++ {
		for c := 1; c <= n; c++ {
			if mat2[r][c] == 1 {
				i := getIndex(r, c, m, n)
				g.AddNode(i)
			}
		}
	}

	// Add Edges
	for r := 1; r <= m; r++ {
		for c := 1; c <= n; c++ {
			if mat2[r][c] == 1 {
				i := getIndex(r, c, m, n)
				// fmt.Println("adding edges for", i)

				// check adjacent nodes and add apropriate edges
				// adjacent cells are
				// [r-1][c-1] 	[r-1][c] 	[r-1][c+1]
				// [r][c-1]					[r][c+1]
				// [r+1][c-1]	[r+1][c]	[r+1][c+1]
				if mat2[r-1][c-1] == 1 {
					g.AddEdge(i, getIndex(r-1, c-1, m, n))
				}
				if mat2[r-1][c] == 1 {
					g.AddEdge(i, getIndex(r-1, c, m, n))
				}
				if c < n && mat2[r-1][c+1] == 1 {
					g.AddEdge(i, getIndex(r-1, c+1, m, n))
				}
				if mat2[r][c-1] == 1 {
					g.AddEdge(i, getIndex(r, c-1, m, n))
				}
				if c < n && mat2[r][c+1] == 1 {
					g.AddEdge(i, getIndex(r, c+1, m, n))
				}
				if r < m && mat2[r+1][c-1] == 1 {
					g.AddEdge(i, getIndex(r+1, c-1, m, n))
				}
				if r < m && mat2[r+1][c] == 1 {
					g.AddEdge(i, getIndex(r+1, c, m, n))
				}
				if r < m && c < n && mat2[r+1][c+1] == 1 {
					g.AddEdge(i, getIndex(r+1, c+1, m, n))
				}
			}
		}
	}
	// fmt.Println(g)
	allNodes := IntSetFromArr(g.Nodes())
	// fmt.Println(allNodes)

	// now that we have constructed the graph, complete with its edges, we need
	// to find the largest connected components
	visited := make(IntSet) // visited nodes
	balance := make(IntSet)
	for i, _ := range allNodes {
		balance[i] = struct{}{}
	}

	sizes := []int{}
	for balance.Len() > 0 {
		stack := []int{allNodes.Any()}
		size := 0
		for len(stack) > 0 {
			var curr int
			curr, stack = stack[len(stack)-1], stack[:len(stack)-1]
			if visited.Contains(curr) {
				continue
			}
			size += 1
			visited.Add(curr)
			balance.Remove(curr)
			neighbours := g[curr]
			// fmt.Println(neighbours)
			stack = append(stack, neighbours.Keys()...)
			// fmt.Println("stack", stack)
			// time.Sleep(1 * time.Second)
		}
		sizes = append(sizes, size)
	}
	return max(sizes)
}

func getIndex(r, c, m, n int) int {
	return (r-1)*n + c
}

// INTSET START ////////////////////////////////////////////////////////////////

type IntSet map[int]struct{}

func IntSetFromArr(arr []int) IntSet {
	res := make(IntSet)
	for _, i := range arr {
		res.Add(i)
	}
	return res
}

func (s *IntSet) Any() int {
	for i, _ := range *s {
		return i
	}
	panic("calling Any() on empty set")
}

func (s *IntSet) Keys() []int {
	res := []int{}
	for i, _ := range *s {
		res = append(res, i)
	}
	return res
}

func (s *IntSet) Len() int {
	return len(s.Keys())
}

func (s *IntSet) Add(i int) {
	(*s)[i] = struct{}{}
}

func (s *IntSet) Contains(i int) bool {
	if _, found := (*s)[i]; found {
		return true
	}
	return false
}

func (s *IntSet) Remove(i int) {
	if _, found := (*s)[i]; found {
		delete(*s, i)
	}
}

func (s *IntSet) Diff(s2 *IntSet) IntSet {
	res := make(IntSet)
	for i, _ := range *s {
		if !s2.Contains(i) {
			res[i] = struct{}{}
		}
	}
	return res
}

// INTSET END //////////////////////////////////////////////////////////////////

type Graph map[int]IntSet

func (g *Graph) AddNode(i int) {
	if _, found := (*g)[i]; !found {
		(*g)[i] = make(IntSet)
	}
}

func (g *Graph) AddEdge(i, j int) {
	// fmt.Println("adding edge between", i, j)
	if i == j {
		return
	}
	iNode, foundI := (*g)[i]
	if !foundI {
		panic("node " + string(i) + " not found")
	}
	jNode, foundJ := (*g)[j]
	if !foundJ {
		panic("node " + string(j) + " not found")
	}
	iNode.Add(j)
	jNode.Add(i)
	// fmt.Println(g)
}

func (g *Graph) Nodes() []int {
	res := []int{}
	for i, _ := range *g {
		res = append(res, i)
	}
	return res
}

// `pp2DIntArr` pretty prints a 2D int array
func pp2DIntArr(data [][]int) {
	for i := 0; i < len(data); i++ {
		fmt.Println(data[i])
	}
}

func keys(m map[int]interface{}) []int {
	res := []int{}
	for i, _ := range m {
		res = append(res, i)
	}
	return res
}
