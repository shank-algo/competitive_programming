def solve(n, matrix):
    count = 0
    for i in range(n):
        for j in range(n):
            mij = matrix[i][j]
            for p in range(n):
                for q in range(n):
                    mpq = matrix[p][q]
                    if mij > mpq and i <= p and j <= q:
                        count += 1
    return count


if __name__ == "__main__":
    import sys
    if len(sys.argv) > 1 and sys.argv[1] == "-d":
        import os
        fn = os.path.abspath(sys.argv[0])
        infile = fn.replace(".py", ".in")
        print("infile: {}".format(infile))
        f = open(infile)

        def input():
            return f.readline()

    t = int(input().strip())
    for case in range(1, t + 1):
        n = int(input().strip())
        matrix = []
        for _ in range(n):
            matrix.append([int(i) for i in input().strip().split(" ")])
        print(solve(n, matrix))
