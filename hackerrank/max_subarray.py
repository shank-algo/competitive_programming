def main():
    t = int(input().strip())
    for cn in range(t):
        c = int(input().strip())
        numbers = [int(v) for v in input().strip().split()]
        max_sum = []
        max_sum_non_contiguous = 0
        contains_positive_int = False

        for i, n in enumerate(numbers):
            if n > 0:
                contains_positive_int = True
            if i == 0:
                max_sum.append(n)
                continue
            max_sum.append(max(max_sum[i - 1] + n, n))

        if contains_positive_int:
            max_sum_non_contiguous = sum(n for n in numbers if n > 0)
        else:
            max_sum_non_contiguous = max(numbers)

        print(max(max_sum), max_sum_non_contiguous)


if __name__ == '__main__':
    main()
