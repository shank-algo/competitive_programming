def ways(n, c):
    if n < c[0]:
        return 0
    if len(c) == 1:
        if n % c[0] != 0:
            return 0
        else:
            return 1

    res = 1 if n % c[0] == 0 else 0
    c0 = c[0]
    cn = 0
    while cn <= n:
        res += ways(n - cn, c[1:])
        cn += c0
    return res


if __name__ == '__main__':

    n, m = [int(v) for v in input().strip().split()]
    c = [int(v) for v in input().strip().split()]
    c.sort()
    print(ways(n, c))
