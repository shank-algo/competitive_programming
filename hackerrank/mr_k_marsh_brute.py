import sys


class MyInput(object):

    def __init__(self, inp):
        self.inp = inp
        self.itr = None

    def yield_input(self):
        while True:
            yield input().strip()

    def yield_from_file(self):
        for line in self.inp:
            yield line.strip()
        self.inp.close()

    def setup_itr(self):
        if self.inp == sys.stdin:
            self.itr = self.yield_input()
        else:
            self.itr = self.yield_from_file()

    def readline(self):
        if not self.itr:
            self.setup_itr()
        try:
            return next(self.itr)
        except StopIteration:
            print("stream ended")


def pp2DArr(data, align=False, min_tab=None):
    if align:
        ml = 0
        for row in data:
            for v in row:
                ml = max(ml, len(str(v)))
        if min_tab:
            ml = max(ml, min_tab)
        for l in range(len(data) - 1, -1, -1):
            line = ", ".join("{:>{}s}".format(str(v), ml) for v in data[l])
            print(line)
    else:
        for l in range(len(data) - 1, -1, -1):
            print("[{}]".format(", ".join(map(str, data[l]))))


def init_counts(m, n, data):
    right = [[-1] * (n + 1) for l in data]
    up = [[-1] * (n + 1) for l in data]

    # find the right and up counts at each position in the matrix
    for i in range(1, m + 1):
        for j in range(1, n + 1):
            if data[i][j] != "x":
                # right
                if right[i][j] < 0:
                    # calculate the no. of positions open to the right
                    rigthCount = 0
                    for l in range(j + 1, n + 1):
                        if data[i][l] == ".":
                            rigthCount += 1
                        else:
                            break
                    # setup right values as per count
                    for k in range(rigthCount + 1):
                        right[i][j + k] = rigthCount - k
                # up
                if up[i][j] < 0:
                    # calculate the no. of positions open to the up
                    upCount = 0
                    for l in range(i + 1, m + 1):
                        if data[l][j] == ".":
                            upCount += 1
                        else:
                            break
                    # setup up values as per count
                    for k in range(upCount + 1):
                        up[i + k][j] = upCount - k

    return right, up


def max_hp(i, j, m, n, data, right, up):
    r = right[i][j]
    u = up[i][j]
    res = 0

    for c in range(j+r, j+1, -1):
        h = min(u, up[i][c])
        for row in range(i+h, i+1, -1):
            if right[row][j] == c-j:
                # rectangle is possible
                lb = (c-j) + (row-i)
                res = max(res, lb)

    return res


def solve(m, n, data):

    right, up = init_counts(m, n, data)
    # print("right")
    # pp2DArr(right, align=True)
    # print("up")
    # pp2DArr(up, align=True)

    # for every position in the array, find the largest rectangle perimeter
    # with that position as the lower left corner fo the square
    lb = 0  # half of max perimeter (l+b)
    for i in range(1, m + 1):
        for j in range(1, n + 1):
            if data[i][j] != "x" and right[i][j] > 0 and up[i][j] > 0:
                r = right[i][j]
                u = up[i][j]
                if lb > r + u:
                    # since we already have a permiter that is bigger than
                    # the max possible perimeter with this point as the lower
                    # left corner, skip it
                    continue
                lb = max(lb, max_hp(i, j, m, n, data, right, up))
    return 2 * lb


if __name__ == '__main__':
    # f = open("mr_k_marsh.in.1")
    # mi = MyInput(f)
    mi = MyInput(sys.stdin)

    m, n = [int(v) for v in mi.readline().split()]
    data = [["x"] * (n + 1)]
    for i in range(m):
        data.append(["x"] + list(mi.readline()))

    # print("data")
    # pp2DArr(data, align=True, min_tab=2)

    res = solve(m, n, data)
    if res == 0:
        print("impossible")
    else:
        print(res)
