package main

import (
	"bufio"
	"fmt"
	"io"
	"math/big"
	"os"
	"strconv"
	"strings"
	"time"
)

////////////////////////////////////////////////////////////////////////////////

// INPUT TEMPLATE START

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res, ok := <-mi.lineChan
	if !ok {
		panic("trying to read from a closed channel")
	}
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END

////////////////////////////////////////////////////////////////////////////////

func main() {
	f, _ := os.Open("acm_icpc_team.in")
	mi := MyInput{rdr: f}
	// mi := MyInput{rdr: os.Stdin}

	nm := mi.readInts()
	n := nm[0]
	// m := nm[1]
	personTopics := []*big.Int{}
	for i := 1; i <= n; i++ {
		var t big.Int
		t.SetString(mi.readLine(), 2)
		personTopics = append(personTopics, &t)
	}
	personTopics = append([]*big.Int{big.NewInt(0)}, personTopics...)
	solve(n, personTopics)
}

func solve(n int, personTopics []*big.Int) {
	// tT := teamTopics{}
	topicCounts := []int{}
	var topics big.Int
	for p1 := 1; p1 < n; p1++ {
		for p2 := p1 + 1; p2 <= n; p2++ {
			topics.Or(personTopics[p1], personTopics[p2])
			// nTopics := strings.Count(biToText(&topics, 2), "1")
			nTopics := strings.Count(topics.Text(2), "1")
			topicCounts = append(topicCounts, nTopics)
		}
	}

	m := max(topicCounts)
	fmt.Println(m)

	teamCountWithMaxTopics := 0
	for _, count := range topicCounts {
		if count == m {
			teamCountWithMaxTopics += 1
		}
	}
	fmt.Println(teamCountWithMaxTopics)
	// fmt.Println(durationInBiToText.Seconds())
	// fmt.Println(durationInStringConcatenation.Seconds())
}

var durationInBiToText time.Duration
var durationInStringConcatenation time.Duration

func biToText(n *big.Int, base int) string {
	start := time.Now()
	zero := big.NewInt(0)
	b := big.NewInt(int64(base))
	var mod big.Int
	res := ""
	for n.Cmp(zero) != 0 {
		n.DivMod(n, b, &mod)
		startConcat := time.Now()
		res = mod.String() + res
		durationInStringConcatenation += time.Since(startConcat)
	}
	durationInBiToText += time.Since(start)
	return res
}

func maxOf(a, b int) int {
	if a < b {
		return b
	}
	return a
}

// max value from array
func max(arr []int) int {
	res := arr[0]
	for _, v := range arr {
		res = maxOf(res, v)
	}
	return res
}
