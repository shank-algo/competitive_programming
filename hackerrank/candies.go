package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func readLines(rdr io.Reader) []string {
	r := bufio.NewReader(rdr)
	lines := []string{}
	for {
		line, err := r.ReadString('\n')
		lines = append(lines, strings.TrimSpace(line))
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
	return lines
}

func solve(n int, rating []int) int {
	prev := 0
	candies := []int{0}
	for i := 1; i < len(rating); i++ {
		if rating[i] > rating[i-1] {
			candies = append(candies, prev+1)
			prev += 1
		} else {
			candies = append(candies, 1)
			prev = 1
		}
	}

	for i := len(rating) - 2; i >= 1; i-- {
		if rating[i] > rating[i+1] && !(candies[i] > candies[i+1]) {
			candies[i] = candies[i+1] + 1
		}
	}
	// fmt.Println(candies)

	total := 0
	for _, c := range candies {
		total += c
	}
	return total
}

func main() {
	// f, _ := os.Open("candies.in")
	// lines := readLines(f)
	lines := readLines(os.Stdin)

	n, _ := strconv.Atoi(lines[0])

	ratings := []int{0}
	for i := 1; i < len(lines); i++ {
		r, _ := strconv.Atoi(lines[i])
		ratings = append(ratings, r)
	}

	fmt.Println(solve(n, ratings))
}
