defmodule Solution do
  def solve do
    IO.read(:stdio, :all)
    |> String.split
    |> reverse
    |> print_list
  end

  def reverse(lst), do: do_reverse(lst, [])
  defp do_reverse([], reversed), do: reversed
  defp do_reverse([h|t], reversed), do: do_reverse(t, [h|reversed])

  ## HELPERS

  def print_list([hd]), do: IO.puts hd
  def print_list([hd | tl]) do
    IO.puts hd
    print_list(tl)
  end
  
end

Solution.solve