defmodule Solution do
  def solve do
    a = IO.read(:stdio, :line) 
        |> String.trim 
        |> String.split
        |> Enum.map(&String.to_integer(&1))
    b = IO.read(:stdio, :line)
        |> String.trim
        |> String.split
        |> Enum.map(&String.to_integer(&1))
    [l | r] = IO.read(:stdio, :line)
              |> String.trim
              |> String.split
              |> Enum.map(&String.to_integer(&1))
  end
end

Solution.solve