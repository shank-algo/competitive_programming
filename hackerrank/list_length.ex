defmodule Solution do
  def solve do
    IO.read(:stdio, :all)
    |> String.split
    |> list_length
    |> IO.puts
  end

  def list_length([]), do: 0
  def list_length([_h|t]), do: 1 + list_length(t)
end

Solution.solve