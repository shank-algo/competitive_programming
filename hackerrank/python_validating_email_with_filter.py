import re

# Username can only contain letters, digits, dash and underscore
username_pattern = re.compile(r'^[\w-]+$')

# Website name can have letters and digits
website_pattern = re.compile(r'^[a-zA-Z0-9]+$')


def extn(e):
    return e.split('.')[1]


def username(e):
    return e.split('@')[0]


def website(e):
    return e.split('@')[1].split('.')[0]


def contains_dot(e):
    return '.' in e and e.count('.') == 1


def correct_order_of_at_and_dot(e):
    return e.index('@') < e.index('.')


def contains_at(e):
    return '@' in e and e.count('@') == 1


def valid_extension(e):
    # Maximum length of the extension is 3
    return 1 <= len(extn(e)) <= 3


def valid_website(e):
    return website_pattern.search(website(e)) is not None


def valid_username(e):
    return username_pattern.search(username(e)) is not None


def iter_input():
    try:
        while True:
            yield input()
    except:
        raise StopIteration()


validator_funcs = [
    contains_dot,
    contains_at,
    correct_order_of_at_and_dot,
    valid_extension,
    valid_username,
    valid_website
]


def solve(N):
    result = filter(validator_funcs.pop(0), iter_input())
    for func in validator_funcs:
        result = filter(func, result)

    print(list(sorted(result)))


if __name__ == '__main__':
    N = int(input())

    solve(N)
