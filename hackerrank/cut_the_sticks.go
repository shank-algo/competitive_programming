package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// INPUT TEMPLATE

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res := <-mi.lineChan
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END

func main() {
	// f, _ := os.Open("cut_the_sticks.in")
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	n := mi.readInt()
	arr := mi.readInts()

	solve(n, arr)
}

func solve(n int, arr []int) {
	for countNonZero(arr) > 0 {
		fmt.Println(countNonZero(arr))

		// fmt.Println("arr", arr)

		minLen := 0
		for _, l := range arr {
			if minLen == 0 && l > 0 {
				minLen = l
			} else if l > 0 {
				minLen = minOf(minLen, l)
			}
		}
		// fmt.Println("minLen", minLen)

		for i := 0; i < n; i++ {
			if arr[i] > 0 {
				arr[i] = maxOf(arr[i]-minLen, 0)
			}
		}

	}
}

func countNonZero(arr []int) int {
	res := 0
	for _, i := range arr {
		if i > 0 {
			res += 1
		}
	}
	return res
}

func minOf(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func maxOf(a, b int) int {
	if a < b {
		return b
	}
	return a
}
