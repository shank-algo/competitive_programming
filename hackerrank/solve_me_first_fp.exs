defmodule Solution do
    def solve do
      {v1, _} = IO.read(:stdio, :line) |> Integer.parse
      {v2, _} = IO.read(:stdio, :line) |> Integer.parse
      IO.puts v1+v2
    end
end

Solution.solve