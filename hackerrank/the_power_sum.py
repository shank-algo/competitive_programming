import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def main():
    x = int(input())
    n = int(input())

    def ways(v, p, start):
        if v < start**p:
            return 0
        elif v == start**p:
            return 1
        else:
            return ways(v - start**p, p, start + 1) + ways(v, p, start + 1)

    print(ways(x, n, 1))


main()

# ==============================================================

if debug_mode:
    inf.close()
