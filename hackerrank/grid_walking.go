package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type MyInput struct {
	rdr   io.Reader
	lines []string
	index int
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if mi.lines == nil {
		r := bufio.NewReader(mi.rdr)
		for {
			line, err := r.ReadString('\n')
			mi.lines = append(mi.lines, strings.TrimSpace(line))
			if err == io.EOF {
				break
			}
			if err != nil {
				panic(err)
			}
		}
	}

	res := mi.lines[mi.index]
	mi.index += 1
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func main() {
	f, _ := os.Open("grid_walking.in")
	in := MyInput{rdr: f}
	// in := MyInput{rdr: os.Stdin}

	t := in.readInt()

	for caseNo := 1; caseNo <= t; caseNo++ {
		nm := in.readInts()
		n := nm[0]
		m := nm[1]
		x := in.readInts()
		D := in.readInts()
		fmt.Println(n, m, x, D)
	}
}
