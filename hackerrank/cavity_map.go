package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

////////////////////////////////////////////////////////////////////////////////

// INPUT TEMPLATE START

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res := <-mi.lineChan
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END

////////////////////////////////////////////////////////////////////////////////

func main() {
	// f, _ := os.Open("cavity_map.in")
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	n := mi.readInt()
	m := [][]int{}
	for i := 0; i < n; i++ {
		r := strings.Split(mi.readLine(), "")
		row := []int{}
		for _, v := range r {
			intVal, _ := strconv.Atoi(v)
			row = append(row, intVal)
		}
		m = append(m, row)
	}
	res := solve(n, m)
	for _, r := range res {
		fmt.Println(r)
	}
}

func solve(n int, m [][]int) []string {
	xPos := [][]int{}

	for rNo := 1; rNo < len(m)-1; rNo++ {
		for cNo := 1; cNo < len(m[0])-1; cNo++ {
			d := m[rNo][cNo]
			allAdjSmallerDepths := true
			for _, adjD := range adjacentDepths(m, rNo, cNo) {
				if adjD >= d {
					allAdjSmallerDepths = false
				}
			}
			if allAdjSmallerDepths {
				xPos = append(xPos, []int{rNo, cNo})
			}
		}
	}

	resTmp := [][]string{}
	for _, r := range m {
		row := []string{}
		for _, v := range r {
			row = append(row, strconv.Itoa(v))
		}
		resTmp = append(resTmp, row)
	}
	for _, pos := range xPos {
		resTmp[pos[0]][pos[1]] = "X"
	}

	res := []string{}
	for _, row := range resTmp {
		res = append(res, strings.Join(row, ""))
	}

	return res
}

func adjacentDepths(m [][]int, r, c int) []int {
	res := []int{}
	res = append(res, m[r-1][c])
	res = append(res, m[r+1][c])
	res = append(res, m[r][c-1])
	res = append(res, m[r][c+1])
	return res
}
