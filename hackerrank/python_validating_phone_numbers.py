import re


# starting with 7, 8 or 9
# 10 digit number
pattern = re.compile(r'^[789][\d]{9}$')


def validate_phone_number(s):
    if pattern.search(s):
        return 'YES'
    else:
        return 'NO'


def iter_input():
    try:
        while True:
            yield input()
    except:
        raise StopIteration()


def solve(N):
    iter_results = map(validate_phone_number, iter_input())
    print('\n'.join(iter_results))


if __name__ == '__main__':
    N = int(input())

    solve(N)
