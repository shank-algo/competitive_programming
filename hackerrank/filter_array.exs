defmodule Solution do
  def solve do
    x = input() |> String.trim |> String.to_integer
    
    numbers = 
      IO.read(:stdio, :all)
      |> String.split
      |> Enum.map(&String.to_integer(&1))
      |> filter(fn(v) -> v < x end)
      
    print_list(numbers)
  end

  def print_list([hd]), do: IO.puts hd
  def print_list([hd | tl]) do
    IO.puts hd
    print_list(tl)
  end

  def filter([], _), do: []
  def filter([elem | tail], predicate) do
    if predicate.(elem) do
      [elem | filter(tail, predicate)]
    else
      filter(tail, predicate)
    end
  end

  def input do
    IO.read(:stdio, :line)
  end

end

Solution.solve