import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def main():
    n, k = list(map(int, input().strip().split()))
    a = list(map(int, input().strip().split()))

    index = [-1] * (n + 1)
    for i, v in enumerate(a):
        index[v] = i

    swaps = 0
    i = 0
    while swaps < k and i < n:
        if a[i] == n - i:
            i += 1
            continue
        from_pos = index[n - i]
        to_pos = i
        index[a[from_pos]] = to_pos
        index[a[to_pos]] = from_pos
        a[from_pos], a[to_pos] = a[to_pos], a[from_pos]
        swaps += 1
        i += 1

    print(" ".join(str(x) for x in a))


main()

# ==============================================================

if debug_mode:
    inf.close()
