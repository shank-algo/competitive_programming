package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// INPUT TEMPLATE

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res := <-mi.lineChan
	return res
}

func (msc *MyInput) readInt() int {
	line := msc.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (msc *MyInput) readInts() []int {
	line := msc.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func main() {
	f, _ := os.Open("mr_k_marsh.in.1")
	mi := MyInput{rdr: f}
	// mi := MyInput{rdr: os.Stdin}

	mnStr := mi.readInts()
	m := mnStr[0]
	n := mnStr[1]

	rows := [][]string{}
	r0 := []string{}
	for i := 0; i < n+1; i++ {
		r0 = append(r0, "x")
	}
	rows = append(rows, r0)
	for rowNo := 1; rowNo < m+1; rowNo++ {
		line := mi.readLine()
		parts := strings.Split(line, "")
		parts = append(parts, ".")
		copy(parts[1:], parts[:n])
		parts[0] = "x"
		rows = append(rows, parts)
	}

	res := solve(m, n, rows)
	if res == 0 {
		fmt.Println("impossible")
	} else {
		fmt.Println(res)
	}
}

func pp2DStrArr(data [][]string) {
	for i := len(data) - 1; i >= 0; i-- {
		fmt.Println(data[i])
	}
}

func pp2DIntArr(data [][]int) {
	for i := len(data) - 1; i >= 0; i-- {
		fmt.Println(data[i])
	}
}

func solve(m, n int, data [][]string) int {
	// TODO

	// print the 2D array
	pp2DStrArr(data)

	// intitalize the right and up counts array
	right := [][]int{}
	up := [][]int{}
	for i := 0; i < m+1; i++ {
		tmp_right := []int{}
		tmp_up := []int{}
		for j := 0; j < n+1; j++ {
			tmp_right = append(tmp_right, -1)
			tmp_up = append(tmp_up, -1)
		}
		right = append(right, tmp_right)
		up = append(up, tmp_up)
	}

	// for each position, calculate the empty positions to right and up
	for i := 1; i < m+1; i++ {
		for j := 1; j < n+1; j++ {
			if data[i][j] != "x" {
				// right
				if i != n {
					// find the max right
					rightPosDist := 0
					for l := j + 1; l < n+1; l++ {
						if data[i][l] == "." {
							rightPosDist += 1
						} else {
							break
						}
					}
					for r := 0; r <= rightPosDist; r++ {
						// fmt.Println("i", i, "j", j, "rightPosDist", rightPosDist, "r", r)
						right[i][j+r] = rightPosDist - r
					}
				}
				// up
				if j != m {
					// find the max right
					if i == 3 && j == 3 {
						pp2DStrArr(data)
						pp2DIntArr(up)
						_ = "breakpoint"
					}
					upPosDist := 0
					for l := i + 1; l < m+1; l++ {
						if data[l][j] == "." {
							upPosDist += 1
						} else {
							break
						}
					}
					for r := 0; r <= upPosDist; r++ {
						// fmt.Println("i", i, "j", j, "upPosDist", upPosDist, "r", r)
						up[i+r][j] = upPosDist - r
					}
				}
			}
		}
	}
	fmt.Println("right")
	pp2DIntArr(right)
	fmt.Println("up")
	pp2DIntArr(up)

	// for each position, find the rectangle of the largest perimeter
	// with that position as the lower left corner of the rectangle

	// return the result
	return 0
}
