import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def main():
    n = int(input().strip())
    k = int(input().strip())
    arr = []
    for _ in range(n):
        arr.append(int(input().strip()))

    arr.sort()

    min_unfairness = 10**9
    for i in range(n - k + 1):
        unfairness = arr[i + k - 1] - arr[i]
        min_unfairness = min(min_unfairness, unfairness)

    print(min_unfairness)


main()

# ==============================================================

if debug_mode:
    inf.close()
