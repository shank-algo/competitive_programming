defmodule Solution do
  def solve do
    n = IO.read(:stdio, :line) |> String.to_integer
    IO.inspect(Enum.to_list(1..n))
  end

  ## HELPERS

  def input do
    IO.read(:stdio, :line)
  end

  def input_loop(func) do
    case input() do
      :eof -> :ok
      data -> data |> String.trim |> func.()
              input_loop(func)
    end
  end

  def print_list([hd]), do: IO.puts hd
  def print_list([hd | tl]) do
    IO.puts hd
    print_list(tl)
  end
  
end

Solution.solve