# MISC utils and algo

import sys


class MyInput(object):
    '''Custom input class to handle input from both stdin and file'''

    def __init__(self, inp):
        self.inp = inp
        self.itr = None

    def yield_input(self):
        while True:
            yield input().strip()

    def yield_from_file(self):
        for line in self.inp:
            yield line.strip()
        self.inp.close()

    def setup_itr(self):
        if self.inp == sys.stdin:
            self.itr = self.yield_input()
        else:
            self.itr = self.yield_from_file()

    def readLine(self):
        if not self.itr:
            self.setup_itr()
        try:
            return next(self.itr)
        except StopIteration:
            print("stream ended")


def pp2DArr(data, align=False, min_tab=None):
    if align:
        ml = 0
        for row in data:
            for v in row:
                ml = max(ml, len(str(v)))
        if min_tab:
            ml = max(ml, min_tab)
        for l in range(len(data) - 1, -1, -1):
            line = ", ".join("{:>{}s}".format(str(v), ml) for v in data[l])
            print(line)
    else:
        for l in range(len(data) - 1, -1, -1):
            print("[{}]".format(", ".join(map(str, data[l]))))
