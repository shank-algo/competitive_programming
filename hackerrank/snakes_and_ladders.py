import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def mouth_of_snake(num, snakes):
    for start, _end in snakes:
        if start == num:
            return True
    return False


def start_of_ladder(num, ladders):
    for start, _end in ladders:
        if start == num:
            return True
    return False


def start_of_ladder_or_snake(num, ladders, snakes):
    return start_of_ladder(num, ladders) or mouth_of_snake(num, snakes)


def end(num, ladders, snakes):
    for start, end in ladders:
        if start == num:
            return end
    for start, end in snakes:
        if start == num:
            return end
    return num


from collections import defaultdict, deque


def bfs(start, end, graph):
    queue = deque()
    queue.append((start, 0))  # (node, num_steps)

    visited = set()
    visited.add(start)

    while queue:
        curr, steps = queue.popleft()
        if curr == end:
            return steps
        neighbours = graph[curr]
        for neighbour in neighbours:
            if neighbour not in visited:
                queue.append((neighbour, steps + 1))
                visited.add(neighbour)

    return -1


def solve(n, ladders, m, snakes):
    # construct the graph in the form of adjacency list
    graph = defaultdict(list)
    for i in range(1, 101):
        if start_of_ladder_or_snake(i, ladders, snakes):
            continue
        else:
            # dice 1-6
            for dice in range(1, 7):
                graph[i].append(end(i + dice, ladders, snakes))

    # bfs
    print(bfs(1, 100, graph))


def main():
    n_cases = int(input().strip())
    for _case_no in range(1, n_cases + 1):
        n_ladders = int(input().strip())
        ladders = []
        for _ in range(n_ladders):
            ladders.append(list(map(int, input().strip().split())))
        m_snakes = int(input().strip())
        snakes = []
        for _ in range(m_snakes):
            snakes.append(list(map(int, input().strip().split())))

        solve(n_ladders, ladders, m_snakes, snakes)


main()

# ==============================================================

if debug_mode:
    inf.close()