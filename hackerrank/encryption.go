package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
	"strings"
)

// INPUT TEMPLATE START ////////////////////////////////////////////////////////

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res, ok := <-mi.lineChan
	if !ok {
		panic("trying to read from a closed channel")
	}
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END //////////////////////////////////////////////////////////

func main() {
	// f, _ := os.Open("encryption.in")
	// defer f.Close()
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	msg := mi.readLine()
	fmt.Println(solve(msg))

}

func solve(msg string) string {
	l := len(msg)
	sqrtL := math.Sqrt(float64(l))
	minRows := int(math.Floor(sqrtL))
	maxCols := int(math.Ceil(sqrtL))

	var rSel, cSel int
	rcSel := math.MaxInt32
	for r := minRows; r <= maxCols; r++ {
		for c := r; c <= maxCols; c++ {
			if r*c >= l {
				if r*c < rcSel {
					rcSel = r * c
					rSel = r
					cSel = c
				}
			}
		}
	}

	rows := []string{}
	for rNo := 1; rNo <= rSel; rNo++ {
		if rNo == rSel {
			rows = append(rows, msg[(rSel-1)*cSel:])
			break
		}
		rows = append(rows, msg[(rNo-1)*cSel:rNo*cSel])
	}

	var buf bytes.Buffer
	for c := 0; c < cSel; c++ {
		for r := 0; r < rSel; r++ {
			if len(rows[r]) > c {
				buf.WriteByte(rows[r][c])
			}
		}
		buf.WriteString(" ")
	}

	return buf.String()
}
