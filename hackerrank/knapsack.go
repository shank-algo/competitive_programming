package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
)

// INPUT TEMPLATE

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res := <-mi.lineChan
	return res
}

func (msc *MyInput) readInt() int {
	line := msc.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (msc *MyInput) readInts() []int {
	line := msc.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func main() {
	f, _ := os.Open("knapsack.in")
	mi := MyInput{rdr: f}
	// mi := MyInput{rdr: os.Stdin}

	t := mi.readInt()
	for caseNo := 1; caseNo <= t; caseNo++ {
		nk := mi.readInts()
		n := nk[0]
		k := nk[1]
		a := mi.readInts()

		fmt.Println(solve(n, k, a))
	}
}

func solve(n, k int, a []int) int {
	// in case we dont have a value less than k
	m := min(a)
	if m > k {
		return 0
	}

	a = removeDuplicates(a)

	// in case there is a value which is a direct divisor of k
	for _, v := range a {
		if k%v == 0 {
			return k
		}
	}

	sort.Ints(a)
	l := len(a)
	for v := k; v > 0; v-- {
		if cc := coin_change(v, l, a); cc > int64(0) {
			return v
		}
	}
	return 0
}

// min value from array
func min(arr []int) int {
	res := arr[0]
	for _, v := range arr {
		res = minOf(res, v)
	}
	return res
}

func set(arr interface{}) interface{} {
	switch arr.(type) {
	case []int:
		res := make(map[int]bool)
		arr := arr.([]int)
		for _, v := range arr {
			res[v] = true
		}
		return res
	default:
		panic("type not supported for set")
	}
}

func removeDuplicates(arr []int) []int {
	res := []int{}
	s := set(arr).(map[int]bool)
	for k, _ := range s {
		res = append(res, k)
	}
	return res
}

func minOf(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func coin_change(n, m int, coins []int) int64 {
	// make and initialize an array for storing results
	res := twoDimArr(m, n+1, int64(0)).([][]int64)
	lCoins := m

	// initilize the first row of the res so as to build the rest
	// upon it
	biggestCoin := coins[lCoins-1]
	for i := 0; i < n+1; i++ {
		if i%biggestCoin == 0 {
			res[0][i] = 1
		} else {
			res[0][i] = 0
		}
	}

	for cindex := 1; cindex < lCoins; cindex++ {
		c := coins[lCoins-cindex-1]
		for v := 1; v <= n; v++ {
			if v < c {
				res[cindex][v] = 0
			} else {
				// ways(v, [5,6]) = ways(v, [6]) + ways(v-5, [6]) + ways(v-10, [6])...
				var r int64
				// initialize r to 1 in case v is a multiple of c
				if v%c == 0 {
					r = 1
				} else {
					r = 0
				}
				for mul := 0; v > mul*c; mul++ {
					r += res[cindex-1][v-mul*c]
				}
				res[cindex][v] = r
			}
		}
	}

	return res[m-1][n]
}

func twoDimArr(n, m int, def interface{}) interface{} {
	switch def.(type) {
	case int:
		res := [][]int{}
		v := def.(int)
		for i := 0; i < n; i++ {
			row := []int{}
			for j := 0; j < m; j++ {
				row = append(row, v)
			}
			res = append(res, row)
		}
		return res
	case int64:
		res := [][]int64{}
		v := def.(int64)
		for i := 0; i < n; i++ {
			row := []int64{}
			for j := 0; j < m; j++ {
				row = append(row, v)
			}
			res = append(res, row)
		}
		return res

	case string:
		res := [][]string{}
		v := def.(string)
		for i := 0; i < n; i++ {
			row := []string{}
			for j := 0; j < m; j++ {
				row = append(row, v)
			}
			res = append(res, row)
		}
		return res

	default:
		panic("type not suppported for twoDimArr")
	}
}
