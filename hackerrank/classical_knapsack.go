package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// INPUT TEMPLATE

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res := <-mi.lineChan
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

// INPUT TEMPLATE END

func main() {
	f, _ := os.Open("classical_knapsack.in")
	mi := MyInput{rdr: f}
	// mi := MyInput{rdr: os.Stdin}

	nM := mi.readInts()
	n := nM[0]
	M := nM[1]
	v := mi.readInts()
	w := mi.readInts()
	fmt.Println(solve(n, M, v, w))
}

func solve(n, M int, v, w []int) int {
	v = append([]int{0}, v...)
	w = append([]int{0}, w...)

	knapsack := twoDimArr(n+1, M+1, 0).([][]int)

	for i := 1; i <= n; i++ {
		for mw := 1; mw <= M; mw++ {
			if w[i] > mw {
				continue
			}
			knapsack[i][mw] = maxOf(knapsack[i-1][mw], v[i]+knapsack[i-1][mw-w[i]])
		}
	}

	// pp2DIntArr(knapsack)
	return knapsack[n][M]
}
func twoDimArr(n, m int, def interface{}) interface{} {
	switch def.(type) {
	case int:
		res := [][]int{}
		v := def.(int)
		for i := 0; i < n; i++ {
			row := []int{}
			for j := 0; j < m; j++ {
				row = append(row, v)
			}
			res = append(res, row)
		}
		return res
	case int64:
		res := [][]int64{}
		v := def.(int64)
		for i := 0; i < n; i++ {
			row := []int64{}
			for j := 0; j < m; j++ {
				row = append(row, v)
			}
			res = append(res, row)
		}
		return res

	case string:
		res := [][]string{}
		v := def.(string)
		for i := 0; i < n; i++ {
			row := []string{}
			for j := 0; j < m; j++ {
				row = append(row, v)
			}
			res = append(res, row)
		}
		return res

	default:
		panic("type not suppported for twoDimArr")
	}
}

// `pp2DIntArr` pretty prints a 2D int array
func pp2DIntArr(data [][]int) {
	for i := 0; i < len(data); i++ {
		fmt.Println(data[i])
	}
}

func maxOf(a, b int) int {
	if a < b {
		return b
	}
	return a
}
