package main

import (
	"bufio"
	"fmt"
	"io"
	"math/big"
	"os"
	"strconv"
	"strings"
)

// CUSTOM TYPE DEFINITIONS START////////////////////////////////////////////////

type pi [2]int     // pair of int
type pi64 [2]int64 // pair of int64

type vi []int                     //vector of int
func (vec vi) Less(i, j int) bool { return vec[i] < vec[j] }
func (vec vi) Swap(i, j int)      { vec[i], vec[j] = vec[j], vec[i] }
func (vec vi) Len() int           { return len(vec) }

type vi64 []int64                   // vector of int64
func (vec vi64) Less(i, j int) bool { return vec[i] < vec[j] }
func (vec vi64) Swap(i, j int)      { vec[i], vec[j] = vec[j], vec[i] }
func (vec vi64) Len() int           { return len(vec) }

// CUSTOM TYPE DEFINITIONS END /////////////////////////////////////////////////

// INPUT TEMPLATE START ////////////////////////////////////////////////////////

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res, ok := <-mi.lineChan
	if !ok {
		panic("trying to read from a closed channel")
	}
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readUint() uint {
	ui, err := strconv.ParseUint(mi.readLine(), 10, 32)
	if err != nil {
		panic(err)
	}
	return uint(ui)
}

func (mi *MyInput) readFloat() float32 {
	line := mi.readLine()
	f, err := strconv.ParseFloat(line, 32)
	if err != nil {
		panic(err)
	}
	return float32(f)
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readFloat64() float64 {
	f, err := strconv.ParseFloat(mi.readLine(), 64)
	if err != nil {
		panic(err)
	}
	return f
}

func (mi *MyInput) readBigInt() *big.Int {
	return mi.readBigIntToBase(10)
}

func (mi *MyInput) readBigIntToBase(base int) *big.Int {
	line := mi.readLine()
	var bi big.Int
	bi.SetString(line, base)
	return &bi
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readFloats() []float32 {
	res := []float32{}
	for _, s := range mi.readWords() {
		f, err := strconv.ParseFloat(s, 32)
		if err != nil {
			panic(err)
		}
		res = append(res, float32(f))
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readFloat64s() []float64 {
	res := []float64{}
	for _, s := range mi.readWords() {
		f, err := strconv.ParseFloat(s, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, f)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END //////////////////////////////////////////////////////////

// OTHER MISC UTILS START //////////////////////////////////////////////////////

// max value from array
func max(arr []int) int {
	res := arr[0]
	for _, v := range arr {
		res = maxOf(res, v)
	}
	return res
}

func maxOf(a, b int) int {
	if a < b {
		return b
	}
	return a
}

func maxOf64(a, b int64) int64 {
	if a < b {
		return b
	}
	return a
}

// min value from array
func min(arr []int) int {
	res := arr[0]
	for _, v := range arr {
		res = minOf(res, v)
	}
	return res
}

func minOf(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// OTHER MISC UTILS END ////////////////////////////////////////////////////////

var (
	div  = big.NewInt(int64(1000000007))
	zero = big.NewInt(int64(0))
	one  = big.NewInt(int64(2))
)

var T = [][]int{[]int{}}       // tree with first node at index 1
var G = [][]int{[]int{}}       // the undirected Graph for the tree T
var NodeVal = []*big.Int{zero} // node values (1 indexed)
var Depths = []int{-1}         // depths for trees at each node
var fib = map[int64]*big.Int{
	0: zero,
	1: one,
	2: one,
} // array to hold the fibonacci values
var queries = []query{}

type query struct {
	typ   string
	x, ky int64
}

func main() {
	// f, _ := os.Open("equal.in")
	// defer f.Close()
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	nm := mi.readInts()
	n, m := nm[0], nm[1]

	// construct the tree and graph and depths holders
	for i := 1; i <= n; i++ {
		T = append(T, []int{})
		G = append(G, []int{})
		Depths = append(Depths, -1)
	}
	for i := 1; i < n; i++ {
		p := mi.readInt()
		T[p] = append(T[p], i+1)
		G[i+1] = append(G[i+1], p)
		G[p] = append(G[p], i+1)
	}
	fmt.Println("T", T)
	fmt.Println("G", G)

	// cosntruct the NodeVal array
	for i := 1; i <= n; i++ {
		NodeVal = append(NodeVal, big.NewInt(int64(0)))
	}
	fmt.Println("NodeVal", NodeVal)

	// read the queries
	for i := 0; i < m; i++ {
		parts := mi.readWords()
		typ := parts[0]
		x, _ := strconv.ParseInt(parts[1], 10, 64)
		y, _ := strconv.ParseInt(parts[2], 10, 64)
		queries = append(queries, query{typ, x, y})
	}
	// fmt.Println("queries", queries)

	solve(n, m)
}

func initDepths(node int, level int) int {
	deepest := 0
	for _, c := range T[node] {
		deepest = maxOf(deepest, initDepths(c, level+1))
	}
	depth := deepest + 1
	Depths[node] = depth
	return depth
}

func solve(n, m int) {
	// find the depth of the tree
	initDepths(1, 1)
	fmt.Println("Depths", Depths)

	maxD := max(Depths)

	// find the max 'k'
	maxK := int64(2)
	var kArr []int64
	for _, query := range queries {
		if query.typ == "U" {
			x := query.x
			k := query.ky
			maxK = maxOf64(maxK, k)
			for i := 0; i < Depths[x]; i++ {
				kArr = append(kArr, k+x+int64(i))
			}
		}
	}
	fmt.Println("maxK", maxK)

	// initialize the fibonacci numbers
	initFib(maxK+int64(maxD), kArr)
	fmt.Println("fib", fib)

	option1()
}

func option1() {
	for _, query := range queries {
		fmt.Println("query", query)
		switch query.typ {

		case "U":
			// do the tree traversal downwards and update NodeVal appropriately
			x := int(query.x)
			k := query.ky
			toVisit := []int{int(x)}
			levels := []int{0}
			for len(toVisit) > 0 {
				// fmt.Println("toVisit", toVisit)
				// fmt.Println("levels", levels)
				var node, level int
				node, toVisit = toVisit[0], toVisit[1:]
				level, levels = levels[0], levels[1:]
				// fmt.Println("node", node, "level", level)
				// fmt.Println("incrementing NodeVal", node, "by", fib[k+int64(level)])
				nv := NodeVal[node]
				fmt.Println("k", k, "level", level)
				nv.Add(nv, fib[k+int64(level)])

				toVisit = append(toVisit, T[node]...)
				levels = append(levels, repeat(level+1, len(T[node]))...)
			}
			fmt.Println("NodeVal", NodeVal)

		case "Q":
			// run bfs and find the path between x and y nodes
			// calculate the sum of all the nodes on the path and also x and y nodes
			x := int(query.x)
			y := int(query.ky)
			path := bfs(x, y)
			fmt.Println("path", x, y, path)
			res := big.NewInt(int64(0))
			for _, node := range path {
				res.Add(res, NodeVal[node])
			}
			fmt.Println(res.Mod(res, div))
		}
	}
}

func bfs(x, y int) (path []int) {
	// fmt.Println("running bfs for", x, y)
	prev := make([]int, len(G))
	toVisit := []int{x}
	seen := make(IntSet)
	seen.Add(x)
	found := false
	for len(toVisit) > 0 && !found {
		var node int
		node, toVisit = toVisit[0], toVisit[1:]
		for _, c := range G[node] {
			if !seen.Contains(c) {
				prev[c] = node
				if c == y {
					found = true
				}
				toVisit = append(toVisit, c)
				seen.Add(c)
			}
		}
	}

	// start from y and track back
	node := y
	path = append(path, y)
	for node != x {
		node = prev[node]
		path = append(path, node)
	}
	return
}

func contains(arr []int, val int) bool {
	for _, v := range arr {
		if v == val {
			return true
		}
	}
	return false
}

func contains64(arr []int64, val int64) bool {
	for _, v := range arr {
		if v == val {
			return true
		}
	}
	return false
}

func initFib(n int64, arr []int64) {
	// calculate fibonacci numbers upto n
	fmt.Println("initFib for", arr)
	a := big.NewInt(int64(0)).Set(one)
	b := big.NewInt(int64(0)).Set(one)
	for i := int64(3); i <= n; i++ {
		tmp := big.NewInt(int64(0)).Set(b)
		b.Add(b, a)
		a = tmp
		if contains64(arr, i) {
			fib[i] = big.NewInt(int64(0)).Set(b)
		}
	}
}

func repeat(n, times int) []int {
	res := make([]int, times)
	for i := 0; i < times; i++ {
		res[i] = n
	}
	return res
}

type Op func(a, b int) int

// INTSET START ////////////////////////////////////////////////////////////

type IntSet map[int]struct{}

func IntSetFromArr(arr []int) IntSet {
	res := make(IntSet)
	for _, i := range arr {
		res.Add(i)
	}
	return res
}

func (s *IntSet) Any() int {
	for i, _ := range *s {
		return i
	}
	panic("calling Any() on empty set")
}

func (s *IntSet) Keys() []int {
	res := []int{}
	for i, _ := range *s {
		res = append(res, i)
	}
	return res
}

func (s *IntSet) Len() int {
	return len(s.Keys())
}

func (s *IntSet) Add(i int) {
	(*s)[i] = struct{}{}
}

func (s *IntSet) Contains(i int) bool {
	if _, found := (*s)[i]; found {
		return true
	}
	return false
}

func (s *IntSet) Remove(i int) {
	if _, found := (*s)[i]; found {
		delete(*s, i)
	}
}

func (s *IntSet) Diff(s2 *IntSet) IntSet {
	res := make(IntSet)
	for i, _ := range *s {
		if !s2.Contains(i) {
			res[i] = struct{}{}
		}
	}
	return res
}

// INTSET END //////////////////////////////////////////////////////////////
