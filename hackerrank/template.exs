defmodule Solution do
  def solve do
    # TODO
  end

  ## HELPERS

  def input do
    IO.read(:stdio, :line)
  end

  def input_loop(func) do
    case input() do
      :eof -> :ok
      data -> data |> String.trim |> func.()
              input_loop(func)
    end
  end

  def print_list([hd]), do: IO.puts hd
  def print_list([hd | tl]) do
    IO.puts hd
    print_list(tl)
  end
  
end

Solution.solve