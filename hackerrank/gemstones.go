package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// INPUT TEMPLATE START ////////////////////////////////////////////////////////

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res, ok := <-mi.lineChan
	if !ok {
		panic("trying to read from a closed channel")
	}
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END //////////////////////////////////////////////////////////

func main() {
	// f, _ := os.Open("gemstones.in")
	// defer f.Close()
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	n := mi.readInt()
	var rocks []string
	for rockNo := 1; rockNo <= n; rockNo++ {
		rocks = append(rocks, mi.readLine())
	}
	fmt.Println(solve(rocks))
}

type set map[string]struct{}

func solve(rocks []string) int {
	gemElems := make(set)

	var rockElems []set
	for _, rock := range rocks {
		elems := make(set)
		for _, c := range rock {
			elems[string(c)] = struct{}{}
		}
		rockElems = append(rockElems, elems)
	}

	var rock1Elems set
	var rock2Elems set
	for r1 := 0; r1 < len(rocks); r1++ {
		rock1Elems = rockElems[r1]
		for c := range rock1Elems {
			existsInAll := true
			for r2 := 0; r2 < len(rocks); r2++ {
				if r1 == r2 {
					continue
				}
				rock2Elems = rockElems[r2]
				if _, exists := rock2Elems[c]; !exists {
					existsInAll = false
					break
				}
			}
			if existsInAll {
				gemElems[c] = struct{}{}
			}
		}
	}

	return len(gemElems)
}
