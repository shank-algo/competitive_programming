import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def main():
    from math import factorial
    from collections import defaultdict

    MOD = 10**9 + 7

    q = int(input().strip())
    for _qn in range(1, q + 1):
        n = int(input().strip())
        a = list(map(int, input().strip().split()))

        d = defaultdict(int)
        twos = 0
        for v in a:
            d[v] += 1
            if d[v] == 2:
                twos += 1

        all_perms = (factorial(n) // (2**twos)) % MOD
        ugly_perms = 0
        for i in range(1, twos + 1):
            ugly_perms += factorial(n - i) // (2**(twos - i))
            ugly_perms %= MOD

        print((all_perms - ugly_perms) % MOD)


main()

# ==============================================================

if debug_mode:
    inf.close()