defmodule Solution do
  def solve do
    IO.read(:stdio, :all)
    |> String.split
    |> Enum.map(&String.to_integer(&1))
    |> sum_odd
    |> IO.puts
  end

  def sum_odd([]), do: 0
  def sum_odd([h|t]) when rem(h, 2) != 0, do: h + sum_odd(t)
  def sum_odd([_|t]), do: sum_odd(t)
end

Solution.solve