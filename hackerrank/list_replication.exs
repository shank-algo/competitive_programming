defmodule Solution do
  def solve do
    {times, _} = Integer.parse(input())
    input_loop(fn v -> 
      for _ <- 1..times, do: IO.puts v end)
  end

  def input do
    IO.read(:stdio, :line)
  end

  def input_loop(func) do
    case input() do
      :eof -> :ok
      data -> data |> String.trim |> func.()
              input_loop(func)
    end
  end
end

Solution.solve