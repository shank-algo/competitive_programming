import timeit
from math import sqrt

def findAllDivisors(n):
	"""Finds all the divisors of a number n"""
	divisors = []
	x = 1
	sqrt_n = sqrt(n)
	# print sqrt_n
	while x <= sqrt_n:
		if n % x == 0:
			divisors.append(x)
			divisors.append(n/x)
		x += 1
	
	return list(set(divisors))


def isAmicable(n):
	if n == 1: return False
	# print n
	divisors = findAllDivisors(n)
	if n in divisors: divisors.remove(n)
	# print('divisors > '+str(divisors))
	sum_divisors = reduce(lambda x,y: x+y, divisors)
	# print('sum_divisors > '+str(sum_divisors))

	if sum_divisors == 1: return False

	if n == sum_divisors: return False
	
	divisors_sum_divisors = findAllDivisors(sum_divisors)
	if sum_divisors in divisors_sum_divisors: divisors_sum_divisors.remove(sum_divisors)
	sum_divisors_sum_divisors = reduce(lambda x,y: x+y, divisors_sum_divisors)

	if sum_divisors_sum_divisors == n:
		print n
		print divisors
		return True
	else:
		return False

def main():
	max_n = 10000
	result = 0
	for x in xrange(3,max_n):
		if isAmicable(x):
			# print x
			result += x

	print('result > '+str(result));


if __name__ == '__main__':
	print(timeit.timeit(main, number=1))
	# print isAmicable(220)