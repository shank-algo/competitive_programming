from math import sqrt

#-----------------------------------------------------------------------------
def findAllDivisors(n):
	"""Finds all the divisors of a number n"""
	divisors = []
	x = 1
	while x <= sqrt(n):
		if n % x == 0:
			divisors.append(x)
			divisors.append(n/x)
		x += 1
	return list(set(divisors))


#-----------------------------------------------------------------------------
def gcd(a,b):
	"""returns the gcd of two numbers a and b where a > b"""
	remainder = -1 	# dummy initialization
	while remainder != 0:
		# print a," ",b
		remainder = a%b
		a,b = b,remainder
	return a


#-----------------------------------------------------------------------------
def lcm(a,b):
	"""returns the lcm of two numbers"""
	return (a*b)/gcd(a,b)


#-----------------------------------------------------------------------------
def isPalindrome(n):
	"""checks if a number is a palindrome number"""
	str_n = str(n)
	numLen = len(str_n)
	result = True
	for i in range(numLen/2):
		if str_n[i] != str_n[numLen-1-i]:
			result = False
			return result
	return result


#-----------------------------------------------------------------------------
def isPrime(n):
	"""checks if a number is prime"""
	
	if n <= 1:
		return False

	if n == 2:
		return True

	# rule out the evens
	if n%2 == 0:
		return False

	x = 3
	sqrt_n = n ** 0.5
	while x <= sqrt_n+1:
		if n%x == 0:
			return False
		x += 2 	# since we have eliminated the evens

	return True


#-----------------------------------------------------------------------------
def sum_n(n):
	"""calculates the sum of first n numbers"""
	return n*(n+1)/2


#-----------------------------------------------------------------------------
