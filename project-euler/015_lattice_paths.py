import timeit

def main():

	v = [[0 for j in range(21)] for i in range(21)]

	for x in xrange(21):
		v[x][0] = 1
		v[0][x] = 1

	for r in xrange(1,21):
		for c in xrange(1,21):
			v[r][c] = v[r-1][c] + v[r][c-1]

	print(v[20][20])

if __name__ == '__main__':
	print(timeit.timeit(main, number=10))