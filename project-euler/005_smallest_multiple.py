import math

def gcd(a,b):
	"""returns the gcd of two numbers a and b where a > b"""
	remainder = -1 	# dummy initialization
	while remainder != 0:
		# print a," ",b
		remainder = a%b
		a,b = b,remainder
	return a

def lcm(a,b):
	"""returns the lcm of two numbers"""
	return (a*b)/gcd(a,b)

def main():
	mList = range(20,0,-1)
	result = reduce(lcm,mList)
	print result

if __name__ == '__main__':
	main()