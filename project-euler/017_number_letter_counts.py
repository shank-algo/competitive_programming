import timeit

def main():
	word_dict = {
		1:'one',
		2:'two',
		3:'three',
		4:'four',
		5:'five',
		6:'six',
		7:'seven',
		8:'eight',
		9:'nine',
		10:'ten',
		11:'eleven',
		12:'twelve',
		13:'thirteen',
		14:'fourteen',
		15:'fifteen',
		16:'sixteen',
		17:'seventeen',
		18:'eighteen',
		19:'nineteen',
		20:'twenty',
		30:'thirty',
		40:'forty',
		50:'fifty',
		60:'sixty',
		70:'seventy',
		80:'eighty',
		90:'ninety'
	}

	word_len_dict = {k:len(v) for k,v in word_dict.items()}

	len_hundred = len('hundred')
	len_thousand = len('onethousand')
	len_and = len('and')

	letters_used = 0
	letters_used += len_thousand
	for x in xrange(1,1000):
		digits_x = [int(d) for d in str(x)]

		if len(digits_x) == 1:
			letters_used += word_len_dict[digits_x[0]]

		if len(digits_x) == 2 and digits_x[0] == 1:
			letters_used += word_len_dict[digits_x[0]*10 + digits_x[1]]

		if len(digits_x) == 2 and digits_x[0] != 1:
			letters_used += word_len_dict[digits_x[0]*10]
			if digits_x[1] != 0:
				letters_used += word_len_dict[digits_x[1]]

		if len(digits_x) == 3:
			letters_used += word_len_dict[digits_x[0]] + len_hundred
			
			if digits_x[1] == 0:
				if digits_x[2] == 0:
					# 100	
					continue
				else:
					# 101
					letters_used += len_and
					letters_used += word_len_dict[digits_x[2]]
			elif digits_x[1] == 1:
				# 110
				letters_used += len_and
				letters_used += word_len_dict[digits_x[1]*10 + digits_x[2]]
			else:
				# 111
				letters_used += len_and
				letters_used += word_len_dict[digits_x[1]*10]
				if digits_x[2] != 0:
					letters_used += word_len_dict[digits_x[2]]

	print('letters_used > '+str(letters_used))

if __name__ == '__main__':
	print(timeit.timeit(main, number=1))