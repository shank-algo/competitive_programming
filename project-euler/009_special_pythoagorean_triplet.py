from math import sqrt, pow

"""
This can be solved using Euclid's formula for pythagorean triplets
a = 2mn
b = m^2 - n^2
c = m^2 + n^2

considering a+b+c = 1000,
we get
m = (-n + sqrt(n^2 + 2000)) / 2
and
n^2 + 2000 = k^2
"""

min_k = int(sqrt(2000))+1

def get_m(n):
	"""returns the value of m on the basis of n"""
	return (-n + sqrt(n**2+2000))/2

def get_n(k):
	return sqrt(k**2 - 2000)

def get_abc(m,n):
	"""
	Given m, n calculates the value of a,b and c and returns a tuple 
	of these values
	"""
	a = 2*m*n
	b = m**2 - n**2
	c = m**2 + n**2
	return (a,b,c)

def checkCase(n):
	"""
	Given a value of n, calculate the value of m
	Then calculate the value of a, b and c
	In case a+b+c = 1000 return (true,(a,b,c)), else return (false,())
	"""
	m = get_m(n)
	if int(m) == m:
		a,b,c = get_abc(m,n)
		if a+b+c == 1000:
			return (True, (a,b,c))
	return (False, ())

def main():
	solved = False
	k = min_k
	while not solved:
		n = get_n(k)
		if int(n) == n:
			solved, abc = checkCase(n)
			if solved:
				print long(abc[0]*abc[1]*abc[2])
		k += 1

if __name__ == '__main__':
	main()