import timeit
import re

def main():
	with open('022_input','r') as f:
		names = f.read().replace('"','').split(',')

	print len(names)

	names.sort()

	result = 0
	for i,name in enumerate(names):
		num_sum = 0
		print(str(i)+'>'+name)
		for ch in name:
			num_sum += ord(ch)-64
		result += (i+1)*num_sum

	print result

if __name__ == '__main__':
	print(timeit.timeit(main, number=1))