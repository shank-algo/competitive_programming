import timeit

def main():
	p = 1000
	value = 2**1000
	digits_sum = reduce(lambda x,y: int(x)+int(y), str(value))
	print(digits_sum)

if __name__ == '__main__':
	print(timeit.timeit(main, number=1))