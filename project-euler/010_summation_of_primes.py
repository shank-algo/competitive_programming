import timeit

def isPrime(n):
	"""checks if a number is prime"""
	
	if n <= 1:
		return False

	if n == 2:
		return True

	# rule out the evens
	if n%2 == 0:
		return False

	x = 3
	sqrt_n = n ** 0.5
	while x <= sqrt_n+1:
		if n%x == 0:
			return False
		x += 2 	# since we have eliminated the evens

	return True


def main():
	primes = [2]
	n = 3
	while n < 2000000:
		if isPrime(n):
			primes.append(n)
		n += 2
	
	print sum(primes)

if __name__ == '__main__':
	main()