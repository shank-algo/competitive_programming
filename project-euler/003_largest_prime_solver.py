def largest_prime_factor(n):
	"""Returns all the largest prime factor for a number"""
	lpf = 2
	while n>lpf:
		while n%lpf == 0:
			n = n/lpf
			lpf = 2
		else:
			lpf += 1
	return lpf


def main():
	x = long(raw_input("Input long int:"))
	print largest_prime_factor(x)


if __name__ == '__main__':
	main()