from math import sqrt
import timeit

def findAllDivisors(n):
	"""Finds all the divisors of a number n"""
	divisors = []
	x = 1
	sqrt_n = sqrt(n)
	# print sqrt_n
	while x <= sqrt_n:
		if n % x == 0:
			divisors.append(x)
			divisors.append(n/x)
		x += 1
	
	return list(set(divisors))

def findDivisorsCount(n):
	"""Calculates the number of divisors of a number"""
	nDivisors = 0;
	x = 1
	sqrt_n = sqrt(n)
	# print sqrt_n
	while x <= sqrt_n:
		if n % x == 0:
			nDivisors += 2
		x += 1

	if int(sqrt_n) * int(sqrt_n) == n:
		nDivisors -= 1
	
	return nDivisors

def sum_n(n):
	"""calculates the sum of first n numbers"""
	return n*(n+1)/2

def main1():
	solved = False
	n = 0
	sum_of_n_nums = 0
	while not solved:
		n += 1
		sum_of_n_nums += n
		divisors = findAllDivisors(sum_of_n_nums)
		if len(divisors) > 500:
			solved = True
			print 'result >',sum_of_n_nums

def main2():
	solved = False
	n = 0
	sum_of_n_nums = 0
	while not solved:
		n += 1
		sum_of_n_nums += n
		nDivisors = findDivisorsCount(sum_of_n_nums)
		if nDivisors > 500:
			solved = True
			print 'result >',sum_of_n_nums
		

if __name__ == '__main__':
	print timeit.timeit(main1, number=10)
	print timeit.timeit(main2, number=10)