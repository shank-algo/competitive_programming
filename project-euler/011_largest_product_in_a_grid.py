filename = '011_input_20x20_grid'

def read_num_grid(filename):
	with open(filename,'r') as file:
		masterNumList = []
		for line in file:
			numList = [int(n) for n in line.split()]
			masterNumList.append(numList)
		return masterNumList


def get_right(i,j):
	nums_right = num_grid[i][j:j+4]
	if len(nums_right) < 4:
		return None
	
	if 0 in nums_right:
		return None

	return nums_right


def get_down(i,j):
	if i > len(num_grid)-4:
		return None

	nums_down = []
	for i in range(i,i+4):
		nums_down.append(num_grid[i][j])
	
	if 0 in nums_down:
		return None
	else:
		return nums_down


def get_up_right_diag(i,j):
	if i < 3:
		return None

	if j > len(num_grid[0])-4:
		return None

	nums_diag = [num_grid[i][j]]
	while len(nums_diag) != 4:
		i -= 1
		j += 1
		nums_diag.append(num_grid[i][j])

	if 0 in nums_diag:
		return None
	else:
		return nums_diag


def get_down_right_diag(i,j):
	if i > len(num_grid)-4:
		return None

	if j > len(num_grid[0])-4:
		return None

	nums_diag = [num_grid[i][j]]
	while len(nums_diag) != 4:
		i += 1
		j += 1
		nums_diag.append(num_grid[i][j])

	if 0 in nums_diag:
		return None
	else:
		return nums_diag


def main():
	global num_grid
	num_grid = read_num_grid(filename)

	greatest_product = 0

	functions = [get_right, get_down, get_up_right_diag, get_down_right_diag]

	for i,nList in enumerate(num_grid):
		for j,n in enumerate(nList):

			if n==0:
				continue

			for function in functions:
				nums = function(i,j)
				if nums:
					product = reduce(lambda x,y: x*y, nums)

				if product > greatest_product:
					greatest_product = product

	print greatest_product


if __name__ == '__main__':
	main()