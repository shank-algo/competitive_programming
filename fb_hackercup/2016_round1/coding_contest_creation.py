def printquad(quad):
    print("appending to quad")
    print("quad is {}".format(quad))


def completequad(quad_count):
    return 4 - quad_count, 0


def solve(ds, N):
    # import ipdb
    # ipdb.set_trace()
    # if N == 11:
    #     import ipdb
    #     ipdb.set_trace()
    count = 0
    quad_count = 0
    quad_max = 0
    for i, d in enumerate(ds):

        # print("d: {}".format(d))

        if i == 0:
            quad_count = 1
            quad_max = d
            if i == N - 1 or d >= ds[i + 1]:
                # end quad here itself
                count = 3
                quad_count = 0
                quad_max = 0

        elif i == N - 1 or d >= ds[i + 1]:
            # end the current quad
            if quad_count is 0:
                count += 3
                quad_max = 0
            else:
                if d - quad_max <= 10:
                    quad_count += 1
                    count += 4 - quad_count
                    quad_count = 0
                    quad_max = 0
                elif 11 <= d - quad_max <= (4 - quad_count) * 10:
                    min_nums_to_b_inserted = 0
                    if (d - quad_max) % 10 == 0:
                        min_nums_to_b_inserted = (d - quad_max) // 10 - 1
                    else:
                        min_nums_to_b_inserted = (d - quad_max) // 10
                    count += min_nums_to_b_inserted   # min numbers to be inserted
                    quad_count += min_nums_to_b_inserted
                    quad_count += 1     # insert the current number
                    count += 4 - quad_count   # complete the quad
                    quad_count = 0
                elif d - quad_max > (4 - quad_count) * 10:
                    count += 4 - quad_count
                    count += 3  # new quad with the current element
                    quad_count = 0

        elif d < ds[i + 1]:
            if quad_count is 0:
                quad_count = 1
                quad_max = d
            elif d - quad_max <= 10:
                quad_count += 1
                quad_max = d
            elif 11 <= d - quad_max <= (4 - quad_count) * 10:
                min_nums_to_b_inserted = 0
                if (d - quad_max) % 10 == 0:
                    min_nums_to_b_inserted = (d - quad_max) // 10 - 1
                else:
                    min_nums_to_b_inserted = (d - quad_max) // 10
                count += min_nums_to_b_inserted   # min numbers to be inserted
                quad_count += min_nums_to_b_inserted
                quad_count += 1     # insert the current number
                quad_max = d
            elif d - quad_max > (4 - quad_count) * 10:
                count += 4 - quad_count
                quad_count = 1  # the current element
                quad_max = d

            if quad_count == 4:
                quad_count = 0
                quad_max = 0

    return count


if __name__ == '__main__':

    with open('coding_contest_creation.txt') as f:
        # with open('coding_contest_creation_example_input.txt') as f:
        # with open('FB1.in') as f:

        # n_cases = int(input().strip())
        n_cases = int(f.readline().strip())
        # print(n_cases)

        for case_no in range(1, n_cases + 1):
            # print("Case No.:", case_no)

            # N = int(input().strip())
            N = int(f.readline().strip())
            # print(N)

            # ds_strings = input().strip().split()
            ds_strings = f.readline().strip().split()
            ds = [int(v) for v in ds_strings]

            # print(ds)

            count = solve(ds, N)

            print("Case #{}: {}".format(case_no, count))
