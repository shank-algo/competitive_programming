from itertools import cycle


def calculate_expected_yatch_left(N, C, all_range):
    res = 0.0
    for i in range(N):
        res += (C[i] * C[i]) / (2 * all_range)
    return res


def solve(N, A, B, C):
    # if N == 5:
    #     import ipdb
    #     ipdb.set_trace()

    expected_left = 0.0

    sumC = sum(C)

    all_range = B - A

    if N == 1 and B < C[0]:
        return (B + A) / 2

    fist_range_start = A
    first_range_end = 0
    first_range_end_index = -1
    sum_till_step_before_start = 0

    # find the first breaking point
    while first_range_end < A:
        first_range_end_index = (first_range_end_index + 1) % N
        first_range_end += C[first_range_end_index]
        if first_range_end < A:
            sum_till_step_before_start += C[first_range_end_index]

    first_range_left_start = A - sum_till_step_before_start
    first_range_left_end = C[first_range_end_index]

    avg_first_range = (first_range_left_start + first_range_left_end) / 2

    expected_left += (avg_first_range) * \
        (first_range_left_end - first_range_left_start) / all_range

    curr_range_start = first_range_end
    curr_range_end = first_range_end
    if B - first_range_end >= sumC:
        # in case sumC >= B - first_range_end, calculate the cost of the whole cycle
        # with the cycle starting from the next cost
        expected_yatch_left = calculate_expected_yatch_left(N, C, all_range)

        # calculate the no. of yatches that can be made whole in this range
        whole_yatch_count = (B - first_range_end) // sumC
        curr_range_end = first_range_end + whole_yatch_count * sumC
        expected_left += whole_yatch_count * expected_yatch_left

    # calculate the rest of the break points and their respective weights
    # we need to take care of values from curr_range_end to B
    curr_range_start = curr_range_end
    end_idx = first_range_end_index
    while curr_range_end <= B:
        end_idx = (end_idx + 1) % N
        curr_range_end = min(B, curr_range_end + C[end_idx])
        diff = curr_range_end - curr_range_start
        expected_left += (diff * diff) / (2 * all_range)
        curr_range_start = curr_range_end
        if curr_range_end == B:
            break

    return expected_left


if __name__ == '__main__':

    with open('yachtzee.txt') as f:
        # n_cases = int(input().strip())
        n_cases = int(f.readline().strip())
        # print(n_cases)

        for case_no in range(1, n_cases + 1):
            # print("Case No.:", case_no)

            N, A, B = [int(v) for v in f.readline().strip().split()]
            # print(N, A, B)

            C = [int(v) for v in f.readline().strip().split()]
            # print(C)

            count = solve(N, A, B, C)

            print("Case #{}: {}".format(case_no, count))
