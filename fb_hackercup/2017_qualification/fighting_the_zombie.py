import sys
import re
from collections import namedtuple

SPELL_SPLITTERS = re.compile(r"[d+-]")

Spell = namedtuple("Spell",
                   ["rolls", "sides", "c", "min_damage", "max_damage"])


def spell_breakup(s):
    parts = [int(i) for i in SPELL_SPLITTERS.split(s)]
    rolls = parts[0]
    sides = parts[1]
    min_damage = rolls * 1
    max_damage = rolls * sides
    c = None
    if len(parts) == 3:
        c = parts[2]
        if "-" in s:
            min_damage -= c
            max_damage -= c
        else:
            min_damage += c
            max_damage += c
    return Spell(rolls, sides, c, min_damage, max_damage)


def max_damage_spell(spells):
    result = None
    for spell in spells:
        if result is None:
            result = spell
            continue
        result = spell if spell.max_damage > result.max_damage else result
    return result


# REFERENCE
PARTITIONS = {}


# REFERENCE
def partitions_util(n, k, min_part, max_part):
    # Base cases
    if min_part > n:
        return 0
    if min_part == n and k != 1:
        return 0
    if min_part == n and k == 1:
        return 1

    # is this sub-problem already solved
    if (n, k, min_part, max_part) in PARTITIONS:
        return PARTITIONS[(n, k, min_part, max_part)]

    res = 0
    for i in range(min_part, max_part + 1):
        res += partitions_util(n - i, k - 1, min_part + 1, max_part)

    PARTITIONS[(n, k, min_part, max_part)] = res
    return res


# REFERENCE
def partitions(n, k, max_part):
    """number of k-element partitions of n"""
    return partitions_util(n, k, 1, max_part)


def solve(h, s, spell_strings):
    spells = []
    for spell_str in spell_strings:
        spells.append(spell_breakup(spell_str))

    # in case we dont have any spell which can kill the zombie,
    # return 0
    max_spell = max_damage_spell(spells)
    if max_spell.max_damage < h:
        return 0

    # if we reach here, there is a non-zero probability of killing the zombie
    partitions(max_spell.max_damage, max_spell.rolls, max_spell.sides)
    best_probability = 0
    for spell in spells:
        partitions(spell.max_damage, spell.rolls, spell.sides)

    return 0


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "-d":
        import os
        f = open(os.path.abspath(__file__).replace(".py", ".txt"))

        def input():
            return f.readline()

    t = int(input())
    for caseno in range(1, t + 1):
        h, s = [int(i) for i in input().strip().split()]
        spells = input().strip().split()
        result = solve(h, s, spells)
        print("Case #{}: {}".format(caseno, result))

    if len(sys.argv) > 1 and sys.argv[1] == "-d":
        f.close()
