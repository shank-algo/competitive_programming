import sys
import math

BLACK = "black"
WHITE = "white"

CENTER = (50, 50)
RADIUS = 50

RADIANS_90_DEG = math.pi / 2


def dist_from_center(x, y):
    return math.hypot(math.fabs(x - CENTER[0]), math.fabs(y - CENTER[1]))


def solve(p, x, y):
    if dist_from_center(x, y) > RADIUS:
        return WHITE
    ny, nx = y - 50, x - 50
    ang_pt = math.degrees(math.atan2(ny, nx))
    ang_pt_new_axis = 90 - ang_pt
    if ang_pt_new_axis < 0:
        ang_pt_new_axis += 360
    ang_progress = p / 100 * 360
    if ang_pt_new_axis <= ang_progress:
        return BLACK
    else:
        return WHITE


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "-d":
        import os
        f = open(os.path.abspath(__file__).replace(".py", ".in"))

        def input():
            return f.readline()

    t = int(input())
    for caseno in range(1, t + 1):
        p, x, y = [int(i) for i in input().strip().split()]
        result = solve(p, x, y)
        print("Case #{}: {}".format(caseno, result))

    if len(sys.argv) > 1 and sys.argv[1] == "-d":
        f.close()
