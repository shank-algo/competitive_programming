import sys
import math
from collections import deque


def solve(n, weight):
    items = deque(sorted(weight))
    n_trips = 0
    while len(items):
        top = items.pop()
        n_items_req = math.ceil(50 / top)
        if len(items) < n_items_req - 1:
            break
        for i in range(n_items_req - 1):
            items.popleft()
        n_trips += 1
    return n_trips


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "-d":
        import os
        f = open(os.path.abspath(__file__).replace(".py", ".txt"))

        def input():
            return f.readline()

    t = int(input())
    for caseno in range(1, t + 1):
        if caseno == 412:
            print("case 412")
        n = int(input())
        weight = []
        for i in range(n):
            weight.append(int(input()))
        result = solve(n, weight)
        print("Case #{}: {}".format(caseno, result))

    if len(sys.argv) > 1 and sys.argv[1] == "-d":
        f.close()
