def find_end_pos(S, P, bs, curr_pos, end_pos, N):
    '''Returns either the end_pos'''
    # in case we are already at the end, return it
    if end_pos == N - 1:
        return (S, N - 1)

    # in case the curr_pos number is greater than P, return None so that we
    # can skip it
    if bs[curr_pos] > P:
        return None

    if bs[curr_pos] == P:
        return (P, curr_pos)

    # remove the prev number from S
    if curr_pos >= 1 and S != 0:
        S -= bs[curr_pos - 1]

    # increase the end pos by 1
    end_pos += 1

    while end_pos <= N - 1 and S < P:
        S += bs[end_pos]
        # print("end_pos, S >>", end_pos, S)

        if S == P:
            # we had reached a sum of P in the last iteration itself
            # print("returing due to S = P :", S)
            return (S, end_pos)

        if S > P:
            # print("returing due to S > P :", S, P)
            return (S - bs[end_pos], end_pos - 1)

        end_pos += 1

    # if control reaches here, we have gone till the end of the list and still
    # havent got the sum of P
    # print("returing due to reaching the end of the boxes:")
    return (S, N - 1)

if __name__ == '__main__':
    n_cases = int(input().strip())
    # print(n_cases)

    for case_no in range(1, n_cases + 1):
        # print(case_no)

        N, P = input().strip().split(' ')
        N = int(N)
        P = int(P)
        # print(N, P)

        bs = input().strip().split()
        for i in range(N):
            bs[i] = int(bs[i])
        # print(bs)

        # case when we have only one number and it is greater than P
        if N == 1:
            print("Case #{}: {}".format(case_no, 0 if bs[0] > P else 1))
            continue

        end_pos = -1
        count = 0
        S = 0
        for curr_pos in range(N):
            res = find_end_pos(S, P, bs, curr_pos, end_pos, N)
            # print("curr_pos >>", curr_pos)

            if res is None:
                # skip the current number and set the end_pos to be the next pos
                # end_pos = curr_pos + 1
                end_pos = curr_pos
                S = 0
                # print("end_pos >>", end_pos)
                continue
            else:
                # increase the count
                S, end_pos = res
                # print("end_pos >>", end_pos)
                # print("S :", S)
                count += end_pos - curr_pos + 1
                # print("count >>", count)

        print("Case #{}: {}".format(case_no, count))
