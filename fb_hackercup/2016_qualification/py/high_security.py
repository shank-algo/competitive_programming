def is_empty(c):
    return c == '.'


def is_bdg(c):
    return c == 'X'


def place_bdg(r, i):
    r[i] = 'X'


def place_guard(ra, rb, i, N):
    # update rb[i]
    place_bdg(rb, i)

    # go in either direction of i position till we get a X
    for pos in range(i, N+1):
        if is_bdg(ra[pos]):
            break
        place_bdg(ra, pos)

    for pos in range(i-1, 0, -1):
        if is_bdg(ra[pos]):
            break
        place_bdg(ra, pos)


def solve(ra, rb, N):
    count = 0

    # FIRST PASS
    # scan rows
    # print("FIRST PASS")
    for i in range(1, N+1):
        # a
        if is_empty(ra[i]) and is_bdg(ra[i-1]) and is_bdg(ra[i+1]):
            # in case there is a building in row b, place guard here
            if is_bdg(rb[i]):
                place_guard(ra, rb, i, N)
                # print('placing guard in row A at', i)
                # print(ra)
                # print(rb)
                count += 1
            else:
                # else place guard in row b
                place_guard(rb, ra, i, N)
                # print('placing guard in row B at', i)
                # print(ra)
                # print(rb)
                count += 1
        # b
        if is_empty(rb[i]) and is_bdg(rb[i-1]) and is_bdg(rb[i+1]):
            # in case there is a building in row a, place guard here
            if is_bdg(ra[i]):
                place_guard(rb, ra, i, N)
                # print('placing guard in row B at', i)
                # print(ra)
                # print(rb)
                count += 1
            else:
                # else place guard in row a
                place_guard(ra, rb, i, N)
                # print('placing guard in row A at', i)
                # print(ra)
                # print(rb)
                count += 1

    # SECOND PASS
    # print("SECOND PASS")
    for i in range(1, N+1):
        if is_empty(ra[i]):
            place_guard(ra, rb, i, N)
            # print('placing guard in row A at', i)
            # print(ra)
            # print(rb)
            count += 1

        if is_empty(rb[i]):
            place_guard(rb, ra, i, N)
            # print('placing guard in row B at', i)
            # print(ra)
            # print(rb)
            count += 1

    return count


if __name__ == '__main__':
    n_cases = int(input().strip())
    # print(n_cases)

    for case_no in range(1, n_cases + 1):
        # print("Case No.:", case_no)

        N = int(input().strip())
        # print(N)

        ra = list('X'+input().strip()+'X')
        # print(ra)
        rb = list('X'+input().strip()+'X')
        # print(rb)

        count = solve(ra, rb, N)

        print("Case #{}: {}".format(case_no, count))
