use std::io::stdin;

// INPUT / OUTPUT

pub fn read_int() -> i32 {
    let mut s = String::new();
    stdin().read_line(&mut s);
    s.to_string().trim().parse::<i32>().unwrap()
}

pub fn read_line() -> String {
    let mut s = String::new();
    stdin().read_line(&mut s);
    s.to_string().trim().to_string()
}

// MATH

pub fn factorial(n: u64) -> u64 {
    let mut res: u64 = 1;
    for i in 2..n + 1 {
        res *= i;
    }
    res
}

#[test]
fn factorial_test() {
    assert_eq!(factorial(1), 1);
    assert_eq!(factorial(3), 6);
    assert_eq!(factorial(5), 120);
}

fn nCr(n: u64, r: u64) -> u64 {
    if n < r {
        panic!("n:{} is greater than r:{}", n, r);
    }
    factorial(n) / (factorial(r) * factorial(n - r))
}

#[test]
#[should_panic]
fn nCr_failed_test() {
    assert_eq!(nCr(2, 3), 1);
}

#[test]
fn nCr_test() {
    assert_eq!(nCr(2, 1), 2);
    assert_eq!(nCr(3, 2), 3);
    assert_eq!(nCr(4, 2), 6);
}

pub fn nC2(n: u64) -> u64 {
    n * (n - 1) / 2
}
