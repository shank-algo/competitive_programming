import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================

import itertools
from operator import itemgetter


def solve(N, K, V, A):
    # visited V-1 times already
    iac = [list(t) for t in zip(N, A, itertools.repeat(0))]
    for visit in range(V - 1):
        iac.sort(key=itemgetter(2, 0))


def main():
    T = int(input())
    for caseno in range(1, T + 1):
        N, K, V = list(map(int, input().strip().split()))
        A = list(input() for _ in range(N))
        result = solve(N, K, V, A)
        print("Case #{}: {}".format(caseno, result))


main()

# ==============================================================

if debug_mode:
    infile.close()
