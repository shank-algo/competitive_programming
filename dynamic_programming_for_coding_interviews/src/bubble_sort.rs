#[allow(dead_code)]
fn bubble_sort_rec(arr: &mut [i32], n: usize) {
    if n == 0 {
        return;
    }

    for i in 0..n - 1 {
        if arr[i] > arr[i + 1] {
            arr.swap(i, i + 1);
        }
    }

    bubble_sort_rec(arr, n - 1);
}

#[test]
fn test_rec() {
    let mut arr = [4, 3, 1, 2];
    let n = arr.len();
    println!("before sorting: {:?}", &arr);
    bubble_sort_rec(&mut arr, n);
    println!("after sorting: {:?}", &arr);
}
