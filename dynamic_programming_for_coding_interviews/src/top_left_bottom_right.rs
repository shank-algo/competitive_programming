#![allow(dead_code)]

fn top_left_bottom_right_rec(r: usize, c: usize) -> u64 {
    if r == 0 && c == 0 {
        return 1;
    }
    // ways(r, c) = ways(r-1, c) + ways(r, c-1)
    let mut ways = 0;
    if r > 0 {
        ways += top_left_bottom_right_rec(r - 1, c);
    }
    if c > 0 {
        ways += top_left_bottom_right_rec(r, c - 1);
    }
    ways
}

#[test]
fn test_rec() {
    assert_eq!(top_left_bottom_right_rec(1, 1), 2);
    assert_eq!(top_left_bottom_right_rec(2, 2), 6);
}

fn top_left_bottom_right_memo(cache: &mut Vec<Vec<u64>>, r: usize, c: usize) -> u64 {
    if cache[r][c] != 0 {
        return cache[r][c];
    }
    // ways(r, c) = ways(r-1, c) + ways(r, c-1)
    let mut ways = 0;
    if r > 0 {
        ways += top_left_bottom_right_rec(r - 1, c);
    }
    if c > 0 {
        ways += top_left_bottom_right_rec(r, c - 1);
    }
    cache[r][c] = ways;
    cache[r][c]
}

#[test]
fn test_memo() {
    let mut cache = vec![vec![0; 3]; 3];
    cache[0][0] = 1;
    assert_eq!(top_left_bottom_right_memo(&mut cache, 2, 2), 6);
}

struct Solution {
    cache: Vec<Vec<u64>>,
}

impl Solution {
    fn new(n: usize) -> Self {
        let mut cache = vec![vec![0; n]; n];
        cache[0][0] = 1;
        Self { cache }
    }
    fn top_left_bottom_right(&mut self, r: usize, c: usize) -> u64 {
        if self.cache[r][c] != 0 {
            return self.cache[r][c];
        }

        let mut ways = 0;
        if r > 0 {
            ways += self.top_left_bottom_right(r - 1, c);
        }
        if c > 0 {
            ways += self.top_left_bottom_right(r, c - 1);
        }
        self.cache[r][c] = ways;
        self.cache[r][c]
    }
}

#[test]
fn test_solution_struct() {
    let mut sol = Solution::new(3);
    assert_eq!(sol.top_left_bottom_right(2, 2), 6);
}
