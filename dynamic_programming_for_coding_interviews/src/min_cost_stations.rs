#![allow(dead_code)]

use std::cmp::min;

// RECURSIVE

/// `cost[i][j]` is the cost of ticket from station `i` to `j`
fn min_cost_stations_rec(cost: &Vec<Vec<i64>>, source: usize, dest: usize) -> i64 {
    if source == dest || source == dest - 1 {
        return cost[source][dest];
    }

    let mut min_cost = cost[source][dest];

    for i in source + 1..dest {
        min_cost = min(
            min_cost,
            min_cost_stations_rec(cost, source, i) + min_cost_stations_rec(cost, i, dest),
        );
    }

    min_cost
}

// MEMOIZED

const INF: i64 = i64::max_value();

/// `cost[i][j]` is the cost of ticket from station `i` to `j`
fn min_cost_stations_memo(
    cache: &mut Vec<Vec<i64>>,
    cost: &Vec<Vec<i64>>,
    source: usize,
    dest: usize,
) -> i64 {
    if source == dest || source == dest - 1 {
        return cost[source][dest];
    }

    let mut min_cost = cost[source][dest];
    for i in source + 1..dest {
        min_cost = min(
            min_cost,
            min_cost_stations_memo(cache, cost, source, i)
                + min_cost_stations_memo(cache, cost, i, dest),
        )
    }
    cache[source][dest] = min_cost;

    cache[source][dest]
}

// SOLUTION STRUCT

struct Solution {
    cache: Vec<Vec<i64>>,
    cost: Vec<Vec<i64>>,
}

impl Solution {
    fn new(cost: Vec<Vec<i64>>) -> Self {
        let n = cost.len();
        let mut cache = vec![vec![INF; n]; n];
        for i in 0..n {
            cache[i][i] = 0;
        }

        Self { cache, cost }
    }

    fn min_cost_stations(&mut self, source: usize, dest: usize) -> i64 {
        if source == dest || source == dest - 1 {
            return self.cost[source][dest];
        }

        let mut min_cost = self.cost[source][dest];
        for i in source + 1..dest {
            min_cost = min(
                min_cost,
                self.min_cost_stations(source, i) + self.min_cost_stations(i, dest),
            );
        }
        self.cache[source][dest] = min_cost;

        self.cache[source][dest]
    }
}

#[test]
fn test_rec() {
    let c0 = vec![0, 1, 3];
    let c1 = vec![INF, 0, 1];
    let c2 = vec![INF, INF, 0];
    let cost = vec![c0, c1, c2];
    let result = min_cost_stations_rec(&cost, 0, 2);
    assert_eq!(result, 2)
}

#[test]
fn test_memo() {
    let c0 = vec![0, 1, 3];
    let c1 = vec![INF, 0, 1];
    let c2 = vec![INF, INF, 0];
    let cost = vec![c0, c1, c2];
    let mut cache = vec![vec![INF; 3]; 3];
    for i in 0..3 {
        cache[i][i] = 0;
    }
    let result = min_cost_stations_memo(&mut cache, &cost, 0, 2);
    assert_eq!(result, 2)
}

#[test]
fn test_solution_struct() {
    let c0 = vec![0, 1, 3];
    let c1 = vec![INF, 0, 1];
    let c2 = vec![INF, INF, 0];
    let cost = vec![c0, c1, c2];
    let mut sol = Solution::new(cost);
    let result = sol.min_cost_stations(0, 2);
    assert_eq!(result, 2)
}
