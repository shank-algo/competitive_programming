#[allow(dead_code)]
fn tower_of_hanoi_rec(source: char, dest: char, extra: char, n: usize) {
    if n == 0 {
        return;
    }

    tower_of_hanoi_rec(source, extra, dest, n - 1);
    println!("Move disk-{} from {} to {}", n, source, dest);
    tower_of_hanoi_rec(extra, dest, source, n - 1);
}

#[test]
fn test_rec() {
    tower_of_hanoi_rec('s', 'd', 'e', 3);
}
