#[allow(dead_code)]
fn max_substring_digit_sum_brute(arr: &[usize]) -> usize {
    let n = arr.len();
    println!("{:?}", &arr);

    let mut max_len = 0;
    for i in 1..n {
        for len in (2..=i + 1).step_by(2) {
            let start = (i + 1) - len;
            let m = start + len / 2;
            let left = &arr[start..m].iter().cloned().sum::<usize>();
            let right = &arr[m..i + 1].iter().cloned().sum::<usize>();

            // println!(
            //     "i:{}, len:{}, start:{}, m:{}, left:{}, right:{}, max_len:{}",
            //     i, len, start, m, left, right, max_len
            // );
            if left == right && len > max_len {
                max_len = len;
            }
        }
    }

    max_len
}

#[test]
fn test_brute() {
    let arr = [0, 1, 3, 4, 0, 3];
    assert_eq!(max_substring_digit_sum_brute(&arr), 4);
    let arr = [0, 1, 3, 4, 6, 3];
    assert_eq!(max_substring_digit_sum_brute(&arr), 0);
}
