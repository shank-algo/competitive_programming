#include <bits/stdc++.h>
using namespace std;

void print(vector<pair<int, int>> points)
{
    for (int i = 0; i < points.size(); i++)
    {
        cout << "(" << points[i].first << "," << points[i].second << ")";
    }
    cout << "\n";
}

int main()
{

    vector<pair<int, int>> a;
    a.push_back({1, 1});
    print(a);

    cout << "hello world\n";
    return 0;
}