#![allow(non_snake_case)]
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufWriter, Write};

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    // CODE HERE
    let T: usize = scan.next();
    for case in 1..T + 1 {
        let N: usize = scan.next();
        let mut tasks: Vec<((usize, usize), usize)> =
            (0..N).map(|i| ((scan.next(), scan.next()), i)).collect();
        tasks.sort();

        let mut assignments: Vec<String> = vec![String::default(); N];
        let mut possible: bool = true;
        let mut c_end: usize = 0;
        let mut j_end: usize = 0;
        for (i, &((start, end), idx)) in tasks.iter().enumerate() {
            if i == 0 {
                assignments[idx] = "C".to_string();
                c_end = end;
                continue;
            }
            if c_end <= start {
                assignments[idx] = "C".to_string();
                c_end = end;
            } else if j_end <= start {
                assignments[idx] = "J".to_string();
                j_end = end;
            } else {
                possible = false;
                break;
            }
        }
        if possible {
            writeln!(out, "Case #{}: {}", case, assignments.join("")).ok();
        } else {
            writeln!(out, "Case #{}: IMPOSSIBLE", case).ok();
        }
    }
}

// Generic utility for reading data from standard input, based on [voxl's
// stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}
