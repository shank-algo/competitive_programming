#![allow(non_snake_case)]
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufWriter, Write};

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    // CODE HERE
    let T: usize = scan.next();
    for case in 1..T + 1 {
        let N: usize = scan.next();
        let mut mat: Vec<Vec<usize>> = Vec::new();
        for _ in 0..N {
            mat.push((0..N).map(|_| scan.next()).collect());
        }

        // main diagonal
        let k: usize = (0..N).map(|i| mat[i][i]).sum();
        // rows
        let mut row_cnt = 0;
        for r in 0..N {
            let mut count: HashMap<usize, usize> = HashMap::new();
            for c in 0..N {
                let cnt = count.entry(mat[r][c]).or_insert(0);
                *cnt += 1;
            }
            if count.iter().any(|(_, &cnt)| cnt > 1) {
                row_cnt += 1;
            }
        }
        // cols
        let mut col_cnt = 0;
        for c in 0..N {
            let mut count: HashMap<usize, usize> = HashMap::new();
            for r in 0..N {
                let cnt = count.entry(mat[r][c]).or_insert(0);
                *cnt += 1;
            }
            if count.iter().any(|(_, &cnt)| cnt > 1) {
                col_cnt += 1;
            }
        }
        writeln!(out, "Case #{}: {} {} {}", case, k, row_cnt, col_cnt).ok();
    }
}

// Generic utility for reading data from standard input, based on [voxl's
// stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}
