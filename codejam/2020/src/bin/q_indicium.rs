#![allow(non_snake_case)]
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufWriter, Write};

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    // CODE HERE
    let T: usize = scan.next();
    for case in 1..T + 1 {
        let N: usize = scan.next();
        let K: usize = scan.next();
        let mut result: Option<Vec<Vec<u8>>> = None;
        for sq in latin_squares(N) {
            if diagonal_sum(&sq) == K {
                result = Some(sq);
                break;
            }
        }

        if result.is_none() {
            writeln!(out, "Case #{}: IMPOSSIBLE", case).ok();
        } else {
            writeln!(
                out,
                "Case #{}: POSSIBLE\n{}",
                case,
                printable(&result.unwrap())
            )
            .ok();
        }
    }
}

fn diagonal_sum(mat: &Vec<Vec<u8>>) -> usize {
    (0..mat.len()).map(|i| mat[i][i] as usize).sum()
}

fn printable(mat: &Vec<Vec<u8>>) -> String {
    let mut result: Vec<String> = Vec::new();
    for i in 0..mat.len() {
        result.push(
            mat[i]
                .iter()
                .map(|&v| v.to_string())
                .collect::<Vec<String>>()
                .join(" "),
        )
    }
    result.iter().cloned().collect::<Vec<String>>().join("\n")
}

// Generic utility for reading data from standard input, based on [voxl's
// stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

fn latin_squares(n: usize) -> Vec<Vec<Vec<u8>>> {
    let base: Vec<Vec<u8>> = base_square(n);
    let mut result: Vec<Vec<Vec<u8>>> = Vec::new();
    for mut row_idx in permutations(n) {
        row_idx = decrease_by_1(row_idx);
        let start = rearrange_matrix_rows(base.clone(), row_idx);
        for mut col_idx in permutations(n) {
            col_idx = decrease_by_1(col_idx);
            result.push(rearrange_matrix_cols(start.clone(), col_idx));
        }
    }
    result
}

fn decrease_by_1(arr: Vec<usize>) -> Vec<usize> {
    arr.iter().map(|&v| v - 1).collect()
}

fn rearrange_matrix_cols(mat: Vec<Vec<u8>>, cols: Vec<usize>) -> Vec<Vec<u8>> {
    let n = mat.len();
    let mut result = Vec::new();
    for r in 0..n {
        let mut row = vec![std::u8::MAX; n];
        for (i, &c) in cols.iter().enumerate() {
            row[i] = mat[r][c];
        }
        result.push(row);
    }
    result
}

fn rearrange_matrix_rows(mat: Vec<Vec<u8>>, rows: Vec<usize>) -> Vec<Vec<u8>> {
    let n = mat.len();
    let mut result = vec![vec![]; n];
    for r in 0..n {
        result[r] = mat[rows[r]].clone();
    }
    result
}

fn permutations(n: usize) -> Vec<Vec<usize>> {
    let nums: Vec<usize> = (1..n + 1).collect();
    perms(&nums[..])
}

fn perms(nums: &[usize]) -> Vec<Vec<usize>> {
    if nums.len() == 1 {
        return vec![nums.to_vec()];
    }

    let mut result = Vec::new();
    for perm in perms(&nums[1..]) {
        for i in 0..perm.len() + 1 {
            let mut perm = perm.clone();
            perm.insert(i, nums[0]);
            result.push(perm);
        }
    }
    result
}

#[test]
fn test_permutations() {
    use std::collections::HashSet;
    let expected: Vec<Vec<usize>> = vec![
        vec![1, 2, 3],
        vec![1, 3, 2],
        vec![2, 1, 3],
        vec![2, 3, 1],
        vec![3, 1, 2],
        vec![3, 2, 1],
    ];
    let expected_set: HashSet<Vec<usize>> = expected.iter().cloned().collect();
    assert_eq!(expected_set, permutations(3).iter().cloned().collect());
}

/// the basic latin square for size n x n
fn base_square(n: usize) -> Vec<Vec<u8>> {
    let mut sq: Vec<Vec<u8>> = Vec::new();
    let mut row: Vec<u8> = (1..n as u8 + 1).collect();
    sq.push(row.clone());
    for _ in 0..n - 1 {
        rotate_left(&mut row);
        sq.push(row.clone());
    }
    sq
}

fn rotate_left(row: &mut Vec<u8>) {
    if row.len() > 0 {
        let l = row.remove(0);
        row.push(l);
    }
}

#[test]
fn test_base_square() {
    let base4 = base_square(4);
    let expected = vec![
        vec![1, 2, 3, 4],
        vec![2, 3, 4, 1],
        vec![3, 4, 1, 2],
        vec![4, 1, 2, 3],
    ];

    assert_eq!(base4, expected);
}
