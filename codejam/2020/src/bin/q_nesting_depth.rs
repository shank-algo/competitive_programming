#![allow(non_snake_case)]
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufWriter, Write};

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    // CODE HERE
    let T: usize = scan.next();
    for case in 1..T + 1 {
        let digits: Vec<u32> = scan
            .next::<String>()
            .chars()
            .map(|c| c.to_digit(10).unwrap())
            .collect();

        let mut st: Vec<String> = Vec::new();
        for d in digits {
            if st.is_empty() {
                for _ in 0..d {
                    st.push("(".to_string());
                }
                st.push(d.to_string());
                for _ in 0..d {
                    st.push(")".to_string());
                }
                continue;
            }
            for _ in 0..d {
                if st.last().unwrap() == &")" {
                    st.pop();
                } else {
                    st.push("(".to_string());
                }
            }
            st.push(d.to_string());
            for _ in 0..d {
                st.push(")".to_string());
            }
        }

        writeln!(out, "Case #{}: {}", case, st.join("")).ok();
    }
}

// Generic utility for reading data from standard input, based on [voxl's
// stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}
