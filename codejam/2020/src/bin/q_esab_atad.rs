#![allow(non_snake_case)]
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufWriter, Write};

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    // CODE HERE
    let T: usize = scan.next();
    let B: usize = scan.next();

    for _case in 1..T + 1 {
        let f = match B {
            10 => solve10,
            20 => solve20,
            100 => solve100,
            _ => unreachable!(),
        };
        let bits = f(scan, out);
        writeln!(out, "{}", bits).ok();
        out.flush().ok();
        let _res: char = scan.next();
    }
}

fn solve10<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) -> String {
    let mut bits: Vec<String> = Vec::new();
    for i in 1..11 {
        writeln!(out, "{}", i).ok();
        out.flush().ok();
        bits.push(scan.next());
    }
    bits.join("")
}

fn read<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>, idx: usize) -> u8 {
    writeln!(out, "{}", idx).ok();
    out.flush().ok();
    scan.next()
}

fn solve20<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) -> String {
    let mut bits: Vec<u8> = vec![0; 22];
    // read [6, 15]
    for i in 6..16_usize {
        bits[i] = read(scan, out, i);
    }

    let mut temp: Vec<u8> = vec![0; 22];
    // 5 steps to right
    for step in 1..6_usize {
        // read 9 elements starting at `6 + step + i`
        let start = 6 + step;
        for i in 0..9 {
            let idx = start + i;
            temp[idx] = read(scan, out, idx);
        }
        // detect operation
        let op = detect_op(&bits, &temp, start);
        // modify the bits accordingly
        modify(&mut bits, op);
        // set the last bit
        let new_idx = 15 + step;
        bits[new_idx] = read(scan, out, new_idx);
    }

    // 5 steps to left
    for step in 1..6_usize {
        // read 9 elements starting at `6 + step + i`
        let start = 6 - step;
        for i in 0..9 {
            let idx = start + i;
            temp[idx] = read(scan, out, idx);
        }
        // detect operation
        let op = detect_op(&bits, &temp, start + 1);
        // modify the bits accordingly
        modify(&mut bits, op);
        // set the last bit
        let new_idx = 6 - step;
        bits[new_idx] = read(scan, out, new_idx);
    }

    bits.iter()
        .skip(1)
        .take(20)
        .map(|&b| b.to_string())
        .collect::<Vec<String>>()
        .join("")
}

fn modify(bits: &mut Vec<u8>, op: Op) {
    match op {
        Op::Nothing => {
            // no change
        }
        Op::Complement => {
            complement(bits);
        }
        Op::Reverse => {
            reverse(bits);
        }
        Op::ComplementAndReverse => {
            complement(bits);
            reverse(bits);
        }
    }
}

/// detect operation using 8 values starting at `from`
fn detect_op(bits: &Vec<u8>, temp: &Vec<u8>, from: usize) -> Op {
    // using values with positions in range [from..(from+7)]
    let to = from + 7;
    if &bits[from..to] == &temp[from..to] {
        return Op::Nothing;
    }

    if is_complement(&bits[from..to], &temp[from..to]) {
        return Op::Complement;
    }

    if is_reversed(&bits[from..to], &temp[from..to]) {
        return Op::Reverse;
    }

    return Op::ComplementAndReverse;
}

fn is_complement(a: &[u8], b: &[u8]) -> bool {
    let mut complement = true;
    let n = a.len();
    for i in 0..n {
        if a[i] == b[i] {
            complement = false;
            break;
        }
    }
    complement
}

fn is_reversed(a: &[u8], b: &[u8]) -> bool {
    let mut b = b.to_vec();
    b.reverse();
    &a[..] == &b[..]
}

fn complement(bits: &mut Vec<u8>) {
    for i in 1..21 {
        bits[i] = if bits[i] == 0 { 1 } else { 0 };
    }
}

fn reverse(bits: &mut Vec<u8>) {
    bits[1..21].reverse();
}

enum Op {
    Complement,
    Reverse,
    ComplementAndReverse,
    Nothing,
}

fn solve100<T: Write>(_scan: &mut Scanner, _out: &mut BufWriter<T>) -> String {
    "".to_string()
}

// Generic utility for reading data from standard input, based on [voxl's
// stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}
