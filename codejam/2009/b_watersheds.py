from collections import defaultdict
from Queue import PriorityQueue

def least_neighbour(row, col, data):
	# NEWS

	ln_r = None
	ln_c = None
	ln_val = data[row][col]

	# N
	if row != 0:
		n_val = data[row-1][col]
		if n_val < ln_val:
			ln_val = n_val
			ln_r = row -1
			ln_c = col

	# W
	if col != 0:
		n_val = data[row][col-1]
		if n_val < ln_val:
			ln_val = n_val
			ln_r = row
			ln_c = col -1

	# E
	if col != len(data[0]) - 1:
		n_val = data[row][col+1]
		if n_val < ln_val:
			ln_val = n_val
			ln_r = row
			ln_c = col + 1

	# S
	if row != len(data)-1:
		n_val = data[row+1][col]
		if n_val < ln_val:
			ln_val = n_val
			ln_r = row + 1
			ln_c = col

	return (ln_r, ln_c)


def print_list(l):
	if l:
		for i in l:
			print(str(i))


def solve(data):
	n_rows = len(data)
	n_cols = len(data[0])

	label_positions = defaultdict(list)

	available_labels = PriorityQueue()
	for i in range(1,500):
		available_labels.put(i, str(i))

	labels = []
	for rowNr in xrange(n_rows):
		labels.append([])
		for colNr in xrange(n_cols):
			labels[rowNr].append(None)

	for rowNr in xrange(n_rows):
		# print 'processing row {}'.format(str(rowNr))
		for colNr in xrange(n_cols):
			# print 'col {}'.format(str(colNr))

			if rowNr == 0 and colNr == 0:
				labels[0][0] = available_labels.get()
				label_positions[labels[0][0]].append((0,0))

			if labels[rowNr][colNr] == None:
				labels[rowNr][colNr] = available_labels.get()
				label_positions[labels[rowNr][colNr]].append((rowNr,colNr))

			# find the least neighbour
			ln_r, ln_c = least_neighbour(rowNr, colNr, data)

			if ln_r is None and ln_c is None:
				# if there is no least neighbour
				continue
			else:
				# if we reach here than we have found a lower neighbour

				# possible conditions are
				# 1. the lower neighbour has None label
				# 2. the lower neighbour has the same label
				# 3. the lower neighbour has a different label

				if labels[ln_r][ln_c] == None:
					# 1. the lower neighbour has None label
					# assign label to the least neighbour
					labels[ln_r][ln_c] = labels[rowNr][colNr]
					label_positions[labels[ln_r][ln_c]].append((ln_r, ln_c))
				else:
					if labels[ln_r][ln_c] == labels[rowNr][colNr]:
						# 2. the lower neighbour has the same label
						# do nothing
						pass
					else:
						# 3. the lower neighbour has a different label
						if int(labels[rowNr][colNr]) < int(labels[ln_r][ln_c]):
							higher_label = labels[ln_r][ln_c]
							# convert the label of (ln_r, ln_c)
							# move all positions for label at (ln_r, ln_c) to label at (rowNr, colNr)
							label_positions[labels[rowNr][colNr]].extend(label_positions[labels[ln_r][ln_c]])
							available_labels.put(labels[ln_r][ln_c], int(labels[ln_r][ln_c]))
							for r,c in label_positions[labels[ln_r][ln_c]]:
								labels[r][c] = labels[rowNr][colNr]
							label_positions[higher_label] = []
						else:
							higher_label = labels[rowNr][colNr]
							label_positions[labels[ln_r][ln_c]].extend(label_positions[labels[rowNr][colNr]])
							available_labels.put(labels[rowNr][colNr], int(labels[rowNr][colNr]))
							for r,c in label_positions[labels[rowNr][colNr]]:
								labels[r][c] = labels[ln_r][ln_c]
							label_positions[higher_label] = []

	# now convert int labels to char labels
	unique_labels = list(reduce(lambda x,y: set(x).union(set(y)), labels))
	unique_labels = sorted(unique_labels, key=int)
	lab_to_ch = {}
	print len(unique_labels)
	for i, label in enumerate(unique_labels):
		lab_to_ch[label] = chr(ord('a')+i)
	print lab_to_ch
	for r in range(len(labels)):
		for c in range(len(labels[0])):
			labels[r][c] = lab_to_ch[int(labels[r][c])]

	res_labels = '\n'.join(' '.join(str(row_labels)) for row_labels in labels)
	return res_labels


if __name__ == '__main__':

	infile_name = 'B-large-practice.in'
	infile = open(infile_name, 'rb')

	n_testcases = int(infile.readline().strip())

	# n_testcases = input()

	for caseNr in xrange(1, n_testcases+1):
		in_str = infile.readline().strip()
		# in_str = raw_input()

		n_row, n_col = in_str.split(' ')

		data = []
		for rowNr in xrange(int(n_row)):
			# row_data = raw_input()
			row_data = infile.readline().strip()
			data.append(row_data.strip().split(' '))

		print("Case #{}:\n{}".format(str(caseNr), str(solve(data))))