from Queue import PriorityQueue


def least_neighbour(row, col, data):
    # NEWS

    ln_r = None
    ln_c = None
    ln_val = data[row][col]

    # N
    if row != 0:
        n_val = data[row-1][col]
        if n_val < ln_val:
            ln_val = n_val
            ln_r = row -1
            ln_c = col

    # W
    if col != 0:
        n_val = data[row][col-1]
        if n_val < ln_val:
            ln_val = n_val
            ln_r = row
            ln_c = col -1

    # E
    if col != len(data[0]) - 1:
        n_val = data[row][col+1]
        if n_val < ln_val:
            ln_val = n_val
            ln_r = row
            ln_c = col + 1

    # S
    if row != len(data)-1:
        n_val = data[row+1][col]
        if n_val < ln_val:
            ln_val = n_val
            ln_r = row + 1
            ln_c = col

    return (ln_r, ln_c)



def solve(data):

    # available labels
    av_ch = PriorityQueue()
    for i in range(1000):
        av_ch.put(i)

    # labels
    label = []
    for r in range(len(data)):
        label.append([])
        for c in range(len(data[0])):
            label[r].append(None)

    # for each position go through the flow
    for r in range(len(data)):
        for c in range(len(data[0])):

            # if position is already labeled, skip it
            if label[r][c] is not None:
                continue

            # flow
            lab = av_ch.get()
            reached_sink = False
            cur_path = [(r,c)]   # list of tuples of coordinates for this flow
            x, y = r, c
            while not reached_sink:
                # find the least neighbor of (x,y)
                ln_r, ln_c = least_neighbour(x, y, data)

                # if there is not least neighbor, we have reached the sink, exit the while loop
                if ln_r is None and ln_c is None:
                    reached_sink = True
                    continue

                # if there is a lower neighbor
                if label[ln_r][ln_c] is not None:
                    #    if the lower neighbor has a label,
                    #    we do not need to go any further
                    #    push 'lab' back into av_ch
                    av_ch.put(lab)
                    #    set lab to be the label found at the lower neighbor
                    lab = label[ln_r][ln_c]
                    #    exit the while
                    reached_sink = True
                else:
                    #    if the lower neighbor does not have a label
                    #    add its coord to the cur_path
                    cur_path.append((ln_r, ln_c))
                    #    change x, y to be ln_r, ln_c
                    x,y = ln_r, ln_c

            # now that we have traced the complete flow for this (r,c)
            for x,y in cur_path:
                label[x][y] = lab

    pass

    for r in range(len(label)):
        for c in range(len(label[0])):
            label[r][c] = chr(label[r][c] + ord('a'))

    result = '\n'.join(' '.join(lab_row) for lab_row in label)
    return result



if __name__ == '__main__':

    infile_name = 'B-large-practice.in'
    infile = open(infile_name, 'rb')

    n_testcases = int(infile.readline().strip())

    # n_testcases = input()

    for caseNr in xrange(1, n_testcases+1):
        in_str = infile.readline().strip()
        # in_str = raw_input()

        n_row, n_col = in_str.split(' ')

        data = []
        for rowNr in xrange(int(n_row)):
            # row_data = raw_input()
            row_data = infile.readline().strip()
            data.append(row_data.strip().split(' '))

        print("Case #{}:\n{}".format(str(caseNr), str(solve(data))))