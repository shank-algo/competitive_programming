package main

import (
	"bufio"
	"fmt"
	"io"
	"math/big"
	"os"
	"strconv"
	"strings"
)

// INPUT TEMPLATE START ////////////////////////////////////////////////////////

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res, ok := <-mi.lineChan
	if !ok {
		panic("trying to read from a closed channel")
	}
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readBigInt() *big.Int {
	return mi.readBigIntToBase(10)
}

func (mi *MyInput) readBigIntToBase(base int) *big.Int {
	line := mi.readLine()
	var bi big.Int
	bi.SetString(line, base)
	return &bi
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END //////////////////////////////////////////////////////////

func main() {
	// f, _ := os.Open("manasa_and_stones.in")
	// defer f.Close()
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	t := mi.readInt()
	for caseNo := 1; caseNo <= t; caseNo++ {
		n := mi.readInt()
		m := mi.readInts()
		fmt.Printf("Case #%d: %s\n", caseNo, strings.Join(toStrArr(solve(n, m)), " "))
	}
}

func solve(n int, m []int) []int {
	var c1, c2, rate int
	for i := 1; i < n; i++ {
		rate = maxOf(rate, m[i-1]-m[i])
	}
	for i := 1; i < n; i++ {
		if m[i-1] > m[i] {
			c1 += m[i-1] - m[i]
		}
		c2 += minOf(rate, m[i-1])
	}
	return []int{c1, c2}
}

func toStrArr(arr []int) []string {
	res := []string{}
	for _, i := range arr {
		res = append(res, strconv.Itoa(i))
	}
	return res
}

func minOf(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func maxOf(a, b int) int {
	if a < b {
		return b
	}
	return a
}
