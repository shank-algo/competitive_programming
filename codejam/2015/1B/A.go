package main

import (
	"fmt"
	"log"
	"math"
	"os"

	"gopkg.in/karalabe/cookiejar.v2/exts/fmtext"
)

const (
	dataset = "A-small-attempt3"
)

// Main in the contest solution entry point to access the data streams.
func main() {
	// Open the input and output streams
	in, err := os.Open(dataset + ".in")
	if err != nil {
		log.Fatalf("Failed to open input file: %v.", err)
	}
	defer in.Close()

	out, err := os.OpenFile(dataset+".out", os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0600)
	if err != nil {
		log.Fatalf("Failed to open output file: %v.", err)
	}
	defer out.Close()

	// Read the number of tests in the input and solve each
	tests := fmtext.FscanInt(in)
	for i := 1; i <= tests; i++ {
		result, err := solve(in)
		if err != nil {
			log.Fatalf("Failed to solve test case %d: %v.", i, err)
		}
		fmt.Fprintf(out, "Case #%d: %s\n", i, result)
		fmt.Printf("Case #%d: %s\n", i, result)
	}
}

// Solve is invoked for each test case of the input dataset.
func solve(in *os.File) (string, error) {
	n := fmtext.FscanInt(in)

	moves := 0
	for n > 19 {
		if n%10 == 0 {
			moves++
			n--
		}
		seq := digits(n)

		cutter := int(math.Pow10((len(seq) + 1) / 2))
		fmt.Println(n, cutter, moves)

		diff := n%cutter - 1
		n, moves = n-diff, moves+diff
		if i := invert(n, 0); i != n {
			n, moves = i, moves+1
		}
		moves += n%cutter - 1
		n = n/cutter*cutter + 1

		n -= 2
		moves += 2
	}
	return fmt.Sprintf("%d", moves+n), nil
}

func digits(n int) []int {
	d := []int{}
	for n > 0 {
		d = append([]int{n % 10}, d...)
		n /= 10
	}
	return d
}

func invert(n int, acc int) int {
	if n == 0 {
		return acc
	}
	return invert(n/10, 10*acc+n%10)
}
