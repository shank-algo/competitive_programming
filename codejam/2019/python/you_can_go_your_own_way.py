import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def solve(N, P):
    result = ""
    for step in P:
        if step == "E": result += "S"
        elif step == "S": result += "E"
    return result


def main():
    T = int(input().strip())
    for i in range(1, T + 1):
        N = int(input().strip())
        P = input().strip()
        result = solve(N, P)
        print("Case #{}: {}".format(i, result))


main()

# ==============================================================

if debug_mode:
    inf.close()
