import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================

import string, math


# int -> [int]
def prime_factors(n):
    res = set()
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            res.add(i)
            res.add(n // i)
    res = sorted(res)
    return res


# int, int, [int] -> str
def solve(N, L, ciphertext):
    factors = prime_factors(min(ciphertext))
    processed, to_process = set(), set(factors)
    while to_process:
        curr = to_process.pop()
        for n in ciphertext:
            if n % curr == 0 and (n // curr not in processed
                                  and n // curr not in to_process):
                to_process.add(n // curr)
        processed.add(curr)
    selected = sorted(processed)
    prime_letter = {
        prime: letter
        for (prime, letter) in zip(selected, string.ascii_uppercase)
    }

    # find the first and second letters
    c0, c1 = ciphertext[0], ciphertext[1]
    c0_f, c1_f = prime_factors(c0), prime_factors(c1)
    l1 = set(c0_f).intersection(set(c1_f)).pop()
    l0 = c0 // l1

    result = "" + str(prime_letter[l0]) + str(prime_letter[l1])
    curr_l = l1
    for i, c in enumerate(ciphertext):
        if i == 0: continue
        l = c // curr_l
        result += str(prime_letter[l])
        curr_l = l
    return result


def main():
    T = int(input().strip())
    for i in range(1, T + 1):
        N, L = list(map(int, input().strip().split()))
        ciphertext = list(map(int, input().strip().split()))
        result = solve(N, L, ciphertext)
        print("Case #{}: {}".format(i, result))


main()

# ==============================================================

if debug_mode:
    inf.close()
