# --------------------------------------------------------------
# Default construct to add the grandparent folder to python path
# --------------------------------------------------------------
if __name__ == '__main__' and __package__ is None:
	try:
		sys
	except:
		import sys

	try:
		path
	except:
		from os import path

	parentDir = path.dirname(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
	if parentDir not in sys.path:
		sys.path.append(parentDir)	
# --------------------------------------------------------------
# Default construct end
# --------------------------------------------------------------

from collections import namedtuple

MAX_INT = 10000

def getSearchEngineIndexes(searchEngines, searchStrings):
	searchEngineIndexes = {}
	for engine in searchEngines:
		searchEngineIndexes[engine] = []

	for x in xrange(len(searchStrings)):
		text = searchStrings[x]
		for engine in searchEngines:
			if engine == text:
				searchEngineIndexes[engine].append(x)

	for engine in searchEngineIndexes:
		searchEngineIndexes[engine].append(MAX_INT)

	return searchEngineIndexes

def updateIndexes(searchEngineIndexes, maxFirstPos):
	for engine in searchEngineIndexes:
		newIndexes = []
		for index in searchEngineIndexes[engine]:
			if index >= maxFirstPos:
				newIndexes.append(index)
		searchEngineIndexes[engine] = newIndexes


def getNewEngAndChangePos(searchEngineIndexes, currEngine):
	maxFirstPos = -1
	newEngine = None
	for engine in searchEngineIndexes:
		if currEngine == engine:
			continue
		if searchEngineIndexes[engine][0] > maxFirstPos:
			maxFirstPos = searchEngineIndexes[engine][0]
			newEngine = engine
	updateIndexes(searchEngineIndexes, maxFirstPos)
	return newEngine, maxFirstPos

def main():
	filename = 'saving_universe_A-large-practice.in'

	with open(filename, 'r') as inFile:
		lines = inFile.readlines()
		cleanedLines = []
		for line in lines:
			if '\n' in line:
				cleanedLines.append(line[:-1])

		nCases = int(cleanedLines[0])
		print 'number of cases > {}'.format(nCases)

		Case = namedtuple('case', ['searchEngines', 'searchStrings'])

		listOfCases = []
		rowIndex = 1	# tracks the row till which we have extracted the data from
		for x in xrange(nCases):
			# create the case and add it to the list of cases
			# the nSearchEngines is at rowIndex
			nSearchEngines = int(cleanedLines[rowIndex])
			print 'number of search engines for case {} > {}'.format(x, nSearchEngines)
			searchEngines = []
			for i in xrange(rowIndex+1, rowIndex+1+nSearchEngines):
				searchEngines.append(cleanedLines[i])

			# the search strings count is at rowIndex + nSearchEninges row
			nSearchStrings = int(cleanedLines[rowIndex+nSearchEngines+1])
			print 'number of search strings for case {} > {}'.format(x, nSearchStrings)
			searchStrings = []
			for j in xrange(rowIndex+1+nSearchEngines+1, rowIndex+1+nSearchEngines+1+nSearchStrings):
				try:
					searchStrings.append(cleanedLines[j])
				except Exception, e:
					print 'index out of range is {}'.format(j)

			currCase = Case(searchEngines, searchStrings)
			listOfCases.append(currCase)
			
			rowIndex = rowIndex + nSearchEngines + 1 + nSearchStrings + 1

		resultList = []
		caseNo = 1
		for currCase in listOfCases:
			searchEngines = currCase.searchEngines
			strings = currCase.searchStrings

			searchEngineIndexes = getSearchEngineIndexes(searchEngines, strings)
			
			for engine in searchEngineIndexes:
				print engine,':',searchEngineIndexes[engine]

			nextChangePos = -1
			newEngine = None
			changes = -1
			while nextChangePos < len(strings):
				newEngine, nextChangePos = getNewEngAndChangePos(searchEngineIndexes, newEngine)
				print newEngine
				changes += 1

			print 'changes required > {}'.format(changes)
			resultList.append('Case #{}: {}'.format(caseNo, changes))

			caseNo += 1


		with open(filename.replace('.in','.out'),'w') as outFile:
			outFile.write('\n'.join(resultList))
				
		
if __name__ == '__main__':
	main()