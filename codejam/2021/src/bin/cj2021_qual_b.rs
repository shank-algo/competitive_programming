#![allow(non_snake_case)]
#![allow(unused_imports)]

use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufWriter, Write};

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    let n_cases: usize = scan.next();
    for case in 1..=n_cases {
        let x = scan.next();
        let y = scan.next();
        let s = scan.next::<String>();
        write!(out, "Case #{}: {}\n", case, solve_case(x, y, s)).ok();
    }
}

fn reduce_input(s: &[char]) -> Vec<char> {
    // reduce the input string
    let (_, reduced) = s.iter().fold((None, vec![]), |(prev, mut acc), ch| {
        if let Some(prev) = prev {
            if prev == ch {
                (Some(ch), acc)
            } else {
                acc.push(*ch);
                (Some(ch), acc)
            }
        } else {
            acc.push(*ch);
            (Some(ch), acc)
        }
    });

    reduced
}

fn optimise(reduced: &mut Vec<char>) {
    // optimise the string
    reduced.insert(0, 'X');
    reduced.push('X');
    for i in 0..reduced.len() {
        let curr = reduced[i];

        if curr != '?' {
            continue;
        }

        let prev = reduced[i - 1];
        let next = reduced[i + 1];

        // possible cases
        // X?C or X?J (at either edge)
        // C?X or J?X (at either edge)
        // C?C or J?J (having same char on both sides)
        // C?J or J?C (having diff char on both sides)

        if prev == 'X' {
            reduced[i] = next;
        } else if next == 'X' {
            reduced[i] = prev;
        } else if prev == next {
            reduced[i] = prev;
        } else {
            if (prev, next) == ('C', 'J') {
                reduced[i] = 'C';
            } else {
                reduced[i] = 'J';
            }
        }
    }

    reduced.pop().unwrap();
    reduced.remove(0);
}

fn solve_case(x: usize, y: usize, s: String) -> usize {
    let s: Vec<char> = s.chars().collect();

    let mut reduced = reduce_input(&s);

    optimise(&mut reduced);

    // calculate the cost
    cost(x, y, &reduced)
}

fn cost(x: usize, y: usize, s: &[char]) -> usize {
    s.iter()
        .enumerate()
        .skip(1)
        .map(|(idx, ch)| {
            let prev = s[idx - 1];
            match (prev, ch) {
                ('C', 'J') => x,
                ('J', 'C') => y,
                _ => 0,
            }
        })
        .sum()
}

// Generic utility for reading data from standard input, based on [voxl's
// stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}
