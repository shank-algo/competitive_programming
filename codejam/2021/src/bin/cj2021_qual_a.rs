#![allow(non_snake_case)]
#![allow(unused_imports)]

use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufWriter, Write};

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    let n_cases: usize = scan.next();
    for case in 1..=n_cases {
        let n = scan.next();
        let nums: Vec<usize> = (0..n).map(|_| scan.next::<usize>()).collect();
        // write!(out, "{:?}\n", nums).ok();
        let result = solve_case(n, nums);
        write!(out, "Case #{}: {}\n", case, result).ok();
    }
}

fn solve_case(n: usize, mut nums: Vec<usize>) -> usize {
    let mut cost = 0;

    for i in 0..n - 1 {
        let idx_min = nums[i..n]
            .iter()
            .enumerate()
            .min_by(|(_, a), (_, b)| a.cmp(b))
            .map(|(idx, _)| idx)
            .unwrap();
        let j = i + idx_min;
        cost += j - i + 1;
        // write!(out, "cost:{}, nums:{:?} i:{} j:{}\n", cost, nums, i, j).ok();
        nums[i..j + 1].reverse();
    }

    cost as usize
}

// Generic utility for reading data from standard input, based on [voxl's
// stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}
