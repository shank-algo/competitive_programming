//! Generic utility for reading data from standard input, based on [voxl's
//! stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufWriter, Write};

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    // CODE HERE
    let T: usize = scan.next();
    for case in 1..T + 1 {
        let mut A: u64 = scan.next();
        let mut B: u64 = scan.next();
        let N: u64 = scan.next();

        for i in 0..N {
            let guess = (A + B) / 2;
            writeln!(out, "{}", guess);
            out.flush();
            let result: String = scan.next();
            match result.as_ref() {
                "CORRECT" => {
                    break;
                }
                "TOO_SMALL" => {
                    A = guess + 1;
                }
                "TOO_BIG" => {
                    B = guess - 1;
                }
                _ => unreachable!(),
            }
        }
    }
}

#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}
