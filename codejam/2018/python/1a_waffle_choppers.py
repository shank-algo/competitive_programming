import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def solve(R, C, H, V, waffle):
    cookies = sum(sum(row) for row in waffle)
    if cookies == 0:
        return "POSSIBLE"

    if cookies % (H + 1) != 0 or cookies % (V + 1) != 0:
        return "IMPOSSIBLE"

    hs_cookies = cookies // (H + 1)
    vs_cookies = cookies // (V + 1)
    hs, vs = [-1], [-1]
    cookies_till_now = 0
    for r, row in enumerate(waffle):
        cookies_till_now += sum(row)
        if cookies_till_now == hs_cookies:
            hs.append(r)
            cookies_till_now = 0
        elif cookies_till_now > hs_cookies:
            return "IMPOSSIBLE"

    cookies_till_now = 0
    for c in range(C):
        cookies_till_now += sum(waffle[i][c] for i in range(R))
        if cookies_till_now == vs_cookies:
            vs.append(c)
            cookies_till_now = 0
        elif cookies_till_now > vs_cookies:
            return "IMPOSSIBLE"

    cookies_per_slice = cookies // ((H + 1) * (V + 1))
    for hsn, hsr in enumerate(hs):
        for vsn, vsc in enumerate(vs):
            if hsn == 0 or vsn == 0:
                continue
            n_cookies = 0
            slice_row_start = hs[hsn-1]+1
            slice_row_end = hs[hsn]
            slice_col_start = vs[vsn-1]+1
            slice_col_end = vs[vsn]
            for r in range(slice_row_start, slice_row_end+1):
                for c in range(slice_col_start, slice_col_end+1):
                    n_cookies += waffle[r][c]
            if n_cookies != cookies_per_slice:
                return "IMPOSSIBLE"

    return "POSSIBLE"


def main():
    cases = int(input().strip())
    for case_no in range(1, cases + 1):
        R, C, H, V = list(map(int, input().strip().split()))
        waffle_str = []
        for _ in range(R):
            waffle_str.append(list(input().strip()))

        waffle = [[-1 for _ in range(C)] for _ in range(R)]
        for r in range(R):
            for c in range(C):
                if waffle_str[r][c] == "@":
                    waffle[r][c] = 1
                else:
                    waffle[r][c] = 0

        result = solve(R, C, H, V, waffle)
        print("Case #{}: {}".format(case_no, result))


main()

# ==============================================================

if debug_mode:
    inf.close()