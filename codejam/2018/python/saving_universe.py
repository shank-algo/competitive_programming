import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================

from collections import defaultdict


def calc_damage(P):
    strength = 1
    damage = 0
    for oper in P:
        if oper == "C":
            strength *= 2
        else:
            damage += strength
    return damage


def solve(D, P):
    while P and P[-1] == "C":
        P.pop()

    damage = calc_damage(P)
    if damage <= D:
        return 0

    swaps = 0
    while P and P.count("C") > 0:
        while P and P[-1] == "C":
            P.pop()
        for idx in range(len(P) - 1, -1, -1):
            if P[idx] == "C":
                P[idx], P[idx + 1] = P[idx + 1], P[idx]
                swaps += 1
                damage = calc_damage(P)
                if damage <= D:
                    return swaps

    return -1


def main():
    t = int(input().strip())
    for case_no in range(1, t + 1):
        d_str, p = input().strip().split()
        d, p = int(d_str), list(p)
        result = solve(d, p)
        if result == -1:
            print("Case #{}: {}".format(case_no, "IMPOSSIBLE"))
        else:
            print("Case #{}: {}".format(case_no, result))


main()

# ==============================================================

if debug_mode:
    infile.close()
