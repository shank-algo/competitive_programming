import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================


def solve(S, mn):
    longest = 0
    lengths = [0] * S

    for start in range(0, S):

        # m of first
        m, n = mn[start]
        with_m_first = 1
        n_set = False
        for j in range(start + 1, S):
            m2, n2 = mn[j]
            if not n_set:
                if m2 != m:
                    n, n_set = n2, True
            else:
                if not (m == m2 or n == n2):
                    break
            with_m_first += 1

        # n of first
        m, n = mn[start]
        with_n_first = 1
        m_set = False
        for j in range(start + 1, S):
            m2, n2 = mn[j]
            if not m_set:
                if n2 != n:
                    m, m_set = m2, True
            else:
                if not (m == m2 or n == n2):
                    break
            with_n_first += 1

        curr_length = max(with_m_first, with_n_first)
        lengths[start] = curr_length
        longest = max(longest, curr_length)

    return longest, lengths.count(longest)


def main():
    T = int(input().strip())
    for caseno in range(1, T + 1):
        S = int(input().strip())
        signs = [list(map(int, input().strip().split())) for _ in range(S)]
        mn = [(d + a, d - b) for d, a, b in signs]
        result = " ".join(str(x) for x in solve(S, mn))
        print("Case #{}: {}".format(caseno, result))


main()

# ==============================================================

if debug_mode:
    infile.close()
