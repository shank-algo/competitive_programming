import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================


def solve(S, mn):
    def rec_solve(signs):
        N = len(signs)
        if N == 1:
            return 1, 1
        elif N == 2:
            return 2, 1

        mid = N // 2

        # containing mid-point
        # mid's m
        m, n = mn[mid]
        mid_m_length = 1
        idx = mid - 1
        start_idx_valid = False
        while idx >= 0 and mn[idx][0] == m:
            mid_m_length += 1
            start_idx_valid = True
            idx -= 1
        mid_m_start = idx if start_idx_valid else mid
        idx = mid + 1
        while idx < N and mn[idx][0] == m:
            mid_m_length += 1
            idx += 1
        n = mn[idx][1]
        mid_m_length += 1
        idx += 1
        while idx < N and mn[idx][1] == n:
            mid_m_length += 1
            idx += 1

        # mid's n
        m, n = mn[mid]
        mid_n_length = 1
        idx = mid - 1
        start_idx_valid = False
        while idx >= 0 and mn[idx][1] == n:
            mid_n_length += 1
            start_idx_valid = True
            idx -= 1
        mid_n_start = idx if start_idx_valid else mid
        idx = mid + 1
        while idx < N and mn[idx][1] == m:
            mid_n_length += 1
            idx += 1
        m = mn[idx][0]
        mid_n_length += 1
        idx += 1
        while idx < N and mn[idx][0] == m:
            mid_n_length += 1
            idx += 1

        longest_mid = max(mid_m_length, mid_n_length)
        cnt_mid = 2 if mid_m_length == mid_n_length and mid_m_start != mid_n_start else 1

        # left half
        longest_left, cnt_left = rec_solve(signs[:mid])

        # right half
        longest_right, cnt_right = rec_solve(signs[mid + 1:])

        longest = max(longest_mid, longest_left, longest_right)
        cnt = 0
        if longest == longest_left:
            cnt += cnt_left
        if longest == longest_mid:
            cnt += cnt_mid
        if longest == longest_right:
            cnt += cnt_right

        return longest, cnt

    return rec_solve(mn)


def main():
    T = int(input().strip())
    for caseno in range(1, T + 1):
        S = int(input().strip())
        signs = [list(map(int, input().strip().split())) for _ in range(S)]
        mn = [(d + a, d - b) for d, a, b in signs]
        result = " ".join(str(x) for x in solve(S, mn))
        print("Case #{}: {}".format(caseno, result))


main()

# ==============================================================

if debug_mode:
    infile.close()
