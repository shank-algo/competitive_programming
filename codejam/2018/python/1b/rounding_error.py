import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================

from math import ceil, floor


def solve(N, L, LS):
    result = 0
    P = [100 * x / N for x in LS]
    already_rounding_up = [
        ceil(x) for x in P if x - int(x) >= 0.5 or x == int(x)
    ]
    result += sum(already_rounding_up)

    not_rounding_up = [(x - int(x), x) for x in P
                       if x - int(x) < 0.5 and x != int(x)]
    not_rounding_up.sort()

    w = 100 / N  # weightage per vote
    balance = N - sum(LS)
    if w == int(w):
        result += balance * w
        return int(result)

    while balance > 0 and not_rounding_up:
        d, curr = not_rounding_up.pop()
        rounded = False
        while balance > 0:
            curr += w
            balance -= 1
            d = curr - int(curr)
            if d >= 0.5:
                result += ceil(curr)
                rounded = True
                break
        if not rounded:
            result += floor(curr)

    # balance left but not list to round
    while balance > 0:
        curr = 0
        rounded = False
        while balance > 0:
            curr += w
            balance -= 1
            d = curr - int(curr)
            if d >= 0.5:
                result += ceil(curr)
                rounded = True
                break
        if not rounded:
            result += floor(curr)

    # no balance but list still pending
    while not_rounding_up:
        d, p = not_rounding_up.pop()
        result += floor(p)

    return result


def main():
    T = int(input())
    for caseno in range(1, T + 1):
        N, L = list(map(int, input().strip().split()))
        LS = list(map(int, input().strip().split()))
        result = solve(N, L, LS)
        print("Case #{}: {}".format(caseno, result))


main()

# ==============================================================

if debug_mode:
    infile.close()
