package solutions;

/*
 ID: shashan20
 LANG: JAVA
 PROG: friday
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Calendar;

public class friday {
	/** **/

	public static void main(String[] args) throws Exception {
		friday main = new friday();
		/** **/
		String path = "C:\\Users\\6008321\\git\\usaco\\src\\solutions\\";
		// String path = "";
		/** **/
		main.solve(path);
		System.exit(0);
	}

	public void solve(String path) throws Exception {

		String infileName = path + this.getClass().getSimpleName() + ".in";
		String outfileName = path + this.getClass().getSimpleName() + ".out";

		BufferedReader f = new BufferedReader(new FileReader(infileName));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				outfileName)));

		// READ the data
		// String a = f.readLine();
		int N = Integer.parseInt(f.readLine());

		// close the input file after reading
		f.close();

		String result = actualSolve(N);

		out.write(result + "\n");
		out.close();
		System.out.println("Done..");
	}

	private String actualSolve(int N) {
		// SOLVE the data
		// Jan 1,1900 -> Dec 31, 1900+N-1
		int startYear = 1900;
		int endYear = 1900 + N - 1;
		String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
				"Aug", "Sep", "Oct", "Nov", "Dec" };
		int[] daysInMonth = { 31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		int firstOfMonth = 1; // index of the day in array of [Sun..Sat]
		String[] days = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
		int[] frequencyOfDay = { 0, 0, 0, 0, 0, 0, 0 };

		Calendar c = Calendar.getInstance();
		c.set(1900, 0, 1);

		for (int year = startYear; year <= endYear; year++) {
			System.out.println(year + " - "
					+ (isLeapYear(year) ? "" : "Not a ") + "Leap Year");
			for (int month = 0; month < 12; month++) {
				System.out.println(months[month]);

				System.out.println("01 " + days[firstOfMonth] + " "
						+ days[c.get(Calendar.DAY_OF_WEEK) - 1]);

				if (days[firstOfMonth] != days[c.get(Calendar.DAY_OF_WEEK) - 1])
					System.out.println("------ MISMATCH ------");

				int thirteenthOfMonth = circularIncrement(firstOfMonth, 5);
				c.add(Calendar.DATE, 12);
				System.out.println("13 " + days[thirteenthOfMonth] + " "
						+ days[c.get(Calendar.DAY_OF_WEEK) - 1]);
				frequencyOfDay[thirteenthOfMonth] += 1;

				int daysThisMonth = 0;
				if (month == 1)
					daysThisMonth = isLeapYear(year) ? 29 : 28;
				else
					daysThisMonth = daysInMonth[month];

				int remainderBy7 = daysThisMonth % 7;
				firstOfMonth = circularIncrement(firstOfMonth, remainderBy7);
				c.add(Calendar.MONTH, 1);
				c.set(Calendar.DATE, 1);
			}
		}

		StringBuilder resultBuilder = new StringBuilder();
		resultBuilder.append(frequencyOfDay[indexOf(days, "Sat")]);
		resultBuilder.append(" ");
		resultBuilder.append(frequencyOfDay[indexOf(days, "Sun")]);
		resultBuilder.append(" ");
		resultBuilder.append(frequencyOfDay[indexOf(days, "Mon")]);
		resultBuilder.append(" ");
		resultBuilder.append(frequencyOfDay[indexOf(days, "Tue")]);
		resultBuilder.append(" ");
		resultBuilder.append(frequencyOfDay[indexOf(days, "Wed")]);
		resultBuilder.append(" ");
		resultBuilder.append(frequencyOfDay[indexOf(days, "Thu")]);
		resultBuilder.append(" ");
		resultBuilder.append(frequencyOfDay[indexOf(days, "Fri")]);
		String result = resultBuilder.toString();

		return result;
	}

	public int indexOf(String[] strArr, String day) {
		for (int i = 0; i < strArr.length; i++) {
			if (strArr[i] == day)
				return i;
		}
		return -1;
	}

	public boolean isLeapYear(int year) {
		boolean divBy4 = year % 4 == 0 ? true : false;
		boolean divBy100 = year % 100 == 0 ? true : false;
		boolean divBy400 = year % 400 == 0 ? true : false;
		if (divBy4) {
			if (divBy100 && divBy400)
				return true;
			else if (divBy100 && !divBy400)
				return false;
			else
				return true;
		} else {
			return false;
		}
	}

	public int circularIncrement(int initial, int increment) {
		// System.out.println("initial: " + initial + "  increment: " +
		// increment);
		if (initial + increment < 7)
			return initial + increment;
		else {
			int maxInc = 6 - initial;
			return increment - maxInc - 1;
		}
	}

}