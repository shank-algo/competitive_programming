package solutions;

/*
 ID: shashan20
 LANG: JAVA
 PROG: milk2
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class milk2 {
	/** **/

	public static void main(String[] args) throws Exception {
		milk2 main = new milk2();
		/** **/
		// String path = "C:\\Users\\6008321\\git\\usaco\\src\\solutions\\";
		String path = "/Users/shank/git/usaco/src/solutions/";
		// String path = "/home/shank/git//usaco/src/solutions/";
		// String path = "";
		/** **/
		main.solve(path);
		System.exit(0);
	}

	public void solve(String path) throws Exception {

		String infileName = path + this.getClass().getSimpleName() + ".in";
		String outfileName = path + this.getClass().getSimpleName() + ".out";

		BufferedReader f = new BufferedReader(new FileReader(infileName));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				outfileName)));

		// READ the data
		// String a = f.readLine();
		int n = Integer.parseInt(f.readLine());

		List<String> startEndTimes = new ArrayList<String>();
		for (int i = 0; i < n; i++) {
			startEndTimes.add(f.readLine());
		}

		// close the input file after reading
		f.close();

		Integer[][] times = new Integer[startEndTimes.size()][2];
		int i = 0;
		for (Iterator<String> iterator = startEndTimes.iterator(); iterator
				.hasNext();) {
			String startEndTime = (String) iterator.next();
			String[] startEnd = startEndTime.split(" ");
			times[i][0] = Integer.parseInt(startEnd[0]);
			times[i][1] = Integer.parseInt(startEnd[1]);
			i++;
		}

		String result = actualSolve(n, times);

		out.write(result + "\n");
		out.close();
		System.out.println("Done..");
	}

	private String actualSolve(int n, Integer[][] times) {
		System.out.println(times);

		// SOLVE the data
		String result = "";
		
		// sort the times[] by start times
		Arrays.sort(times, new Comparator<Integer[]>() {
			@Override
			public int compare(Integer[] o1, Integer[] o2) {
				return o1[0].compareTo(o2[0]);
			}
		});
		
		int currStart = times[0][0];
		int currEnd = times[0][1];
		int maxContinuousMilkingTime = currEnd - currStart;
		int maxIdleTime = 0;

		if (times.length > 1) {
			for (int i = 1; i < times.length; i++) {
				int start = times[i][0];
				int end = times[i][1];

				if (start > currEnd) {
					int continuousMilkingTime = currEnd - currStart;
					maxContinuousMilkingTime = Math.max(
							maxContinuousMilkingTime, continuousMilkingTime);

					maxIdleTime = Math.max(maxIdleTime, start - currEnd);

					// reset the currStart and currEnd times
					currStart = start;
					currEnd = end;
				} else {
					currEnd = Math.max(currEnd, end);
				}

			}
		}

		result = Integer.toString(maxContinuousMilkingTime) + " "
				+ Integer.toString(maxIdleTime);

		System.out.println(result);

		return result;
	}

}