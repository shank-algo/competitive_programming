package solutions;

/*
 ID: shashan20
 LANG: JAVA
 PROG: palsquare
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class palsquare {

	private static final String ALPHABETS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	/** **/

	public static void main(String[] args) throws Exception {
		palsquare main = new palsquare();
		/** **/
		// String path = "C:\\Users\\6008321\\git\\usaco\\src\\solutions\\";
		// String path = "/Users/shank/git/usaco/src/solutions/";
		String path = "/home/shank/git//usaco/src/solutions/";
		// String path = "";
		/** **/
		main.solve(path);
		System.exit(0);
	}

	public void solve(String path) throws Exception {

		String infileName = path + this.getClass().getSimpleName() + ".in";
		String outfileName = path + this.getClass().getSimpleName() + ".out";

		BufferedReader f = new BufferedReader(new FileReader(infileName));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				outfileName)));

		// READ the data
		// String a = f.readLine();
		int n = Integer.parseInt(f.readLine());

		// close the input file after reading
		f.close();

		String result = actualSolve(n);

		out.write(result + "\n");
		out.close();
		System.out.println("Done..");
	}

	private String actualSolve(int n) {
		System.out.println(n);

		// SOLVE the data
		String result = "";

		// get the base in form of a string
		String b = charFromIntForBase(n);
		System.out.println("Base > " + b);

		List<String> resultList = new ArrayList<String>();

		for (int i = 1; i < 301; i++) {
			System.out.println(String.valueOf(i));
			String ib = Integer.toString(i, n).toUpperCase();
			int i2 = i * i;
			String i2b = Integer.toString(i2, n).toUpperCase();
			if (isPalindrome(i2b))
				resultList.add(ib + " " + i2b);
		}

		result = join("\n", resultList);

		System.out.println(result);

		return result;
	}

	public static <T> String join(String joinStr, List<T> list) {
		StringBuilder builder = new StringBuilder();
		int size = list.size();
		for (int i = 0; i < size; i++) {
			T t = list.get(i);
			builder.append(t.toString());
			if (i != size - 1)
				builder.append(joinStr);
		}

		return builder.toString();
	}

	public static String charFromIntForBase(int n) {
		if (n < 10)
			return String.valueOf(n);
		else {
			return String.valueOf(ALPHABETS.charAt(n - 10));
		}
	}

	public static int intFromCharForBase(String s) {
		try {
			int n = Integer.parseInt(s);
			return n;
		} catch (NumberFormatException nfe) {
			int n = 10 + ALPHABETS.indexOf(s);
			return n;
		}
	}

	public static boolean isPalindrome(String s) {
		int len = s.length();
		for (int i = 0; i < len / 2; i++) {
			if (s.charAt(i) != s.charAt(len - 1 - i)) {
				return false;
			}
		}
		return true;
	}

}