package solutions;

/*
 ID: shashan20
 LANG: JAVA
 PROG: transform
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.MessageFormat;

public class transform {
	/** **/

	public static void main(String[] args) throws Exception {
		transform main = new transform();
		/** **/
		String path = "C:\\Users\\6008321\\git\\usaco\\src\\solutions\\";
		// String path = "/Users/shank/git/usaco/src/solutions/";
		// String path = "/home/shank/git//usaco/src/solutions/";
		// String path = "";
		/** **/
		main.solve(path);
		System.exit(0);
	}

	public void solve(String path) throws Exception {

		String infileName = path + this.getClass().getSimpleName() + ".in";
		String outfileName = path + this.getClass().getSimpleName() + ".out";

		BufferedReader f = new BufferedReader(new FileReader(infileName));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				outfileName)));

		// READ the data
		// String a = f.readLine();
		String nStr = f.readLine();
		int N = Integer.parseInt(nStr);

		char[][] original = new char[N][N];
		for (int i = 0; i < N; i++) {
			original[i] = f.readLine().toCharArray();
		}

		char[][] transformed = new char[N][N];
		for (int i = 0; i < N; i++) {
			transformed[i] = f.readLine().toCharArray();
		}

		// close the input file after reading
		f.close();

		String result = actualSolve(N, original, transformed);

		out.write(result + "\n");
		out.close();
		System.out.println("Done..");
	}

	private String actualSolve(int N, char[][] original, char[][] transformed) {
		System.out.println(MessageFormat.format("N={0}", Integer.toString(N)));

		// SOLVE the data
		int transformation = 0;
		if (is90DegRotation(N, original, transformed))
			transformation = 1;
		else if (is180DegRotation(N, original, transformed))
			transformation = 2;
		else if (is270DegRotation(N, original, transformed))
			transformation = 3;
		else if (isReflection(N, original, transformed))
			transformation = 4;
		else if (isCombination(N, original, transformed))
			transformation = 5;
		else if (isNoChange(N, original, transformed))
			transformation = 6;
		else
			transformation = 7;

		String result = Integer.toString(transformation);
		System.out.println(result);

		return result;
	}

	private boolean isNoChange(int n, char[][] original, char[][] transformed) {
		if (isEqual(transformed, original))
			return true;
		else
			return false;
	}

	private boolean isCombination(int n, char[][] original, char[][] transformed) {
		// reflection + [1-3]
		char[][] reflected = getReflected(original);

		char[][] reflectedAnd90Rotated = getRotatedClkwise90(reflected);
		if (isEqual(transformed, reflectedAnd90Rotated))
			return true;

		char[][] reflectedAnd180Rotated = getRotatedClkwise90(reflectedAnd90Rotated);
		if (isEqual(transformed, reflectedAnd180Rotated))
			return true;

		char[][] reflectedAnd270Rotated = getRotatedClkwise90(reflectedAnd180Rotated);
		if (isEqual(transformed, reflectedAnd270Rotated))
			return true;

		return false;
	}

	private boolean isReflection(int n, char[][] original, char[][] transformed) {
		if (isEqual(transformed, getReflected(original)))
			return true;
		else
			return false;
	}

	private boolean is270DegRotation(int n, char[][] original,
			char[][] transformed) {
		char[][] rotated90 = getRotatedClkwise90(original);
		char[][] rotated180 = getRotatedClkwise90(rotated90);
		char[][] rotated270 = getRotatedClkwise90(rotated180);
		if (isEqual(transformed, rotated270))
			return true;
		else
			return false;
	}

	private boolean is180DegRotation(int n, char[][] original,
			char[][] transformed) {
		char[][] rotated90 = getRotatedClkwise90(original);
		char[][] rotated180 = getRotatedClkwise90(rotated90);
		if (isEqual(transformed, rotated180))
			return true;
		else
			return false;
	}

	private boolean is90DegRotation(int n, char[][] original,
			char[][] transformed) {
		char[][] rotated90 = getRotatedClkwise90(original);
		if (isEqual(rotated90, transformed))
			return true;
		else
			return false;
	}

	private boolean isEqual(char[][] mat1, char[][] mat2) {
		if (mat1.length != mat2.length)
			return false;

		if ((mat1.length == 0 && mat2.length != 0)
				|| (mat2.length == 0 && mat1.length != 0))
			return false;

		if (mat1.length == 0 && mat2.length == 0)
			return true;

		if ((mat1[0].length == 0 && mat2[0].length != 0)
				|| (mat2[0].length == 0 && mat1[0].length != 0))
			return false;

		if (mat1[0].length == 0 && mat2[0].length == 0)
			return true;

		if (mat1[0].length != mat2[0].length)
			return false;

		// when control reaches here, we have verified that the two matrices are
		// of the same dimensions
		// System.out.println(MessageFormat.format(
		// "mat1.length={0} mat1[0].length={1}", new Integer(mat1.length),
		//		new Integer(mat1[0].length)));
		for (int i = 0; i < mat1.length; i++)
			for (int j = 0; j < mat1[0].length; j++) {
				if (mat1[i][j] != mat2[i][j])
					return false;
			}

		return true;
	}

	private char[][] getRotatedClkwise90(char[][] original) {
		// Ri > Cn-1-i
		int N = original.length;

		char[][] result = new char[N][N];
		for (int i = 0; i < N; i++)
			for (int j = 0; j < N; j++) {
				result[j][N - 1 - i] = original[i][j];
			}
		return result;
	}

	private char[][] getReflected(char[][] original) {
		int N = original.length;
		char[][] result = new char[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				result[i][j] = original[i][N - 1 - j];
			}
		}
		return result;
	}

}