package solutions;

import java.util.List;

public class Utils {

	private static final String ALPHABETS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public static <T> String join(String joinStr, List<T> list) {
		StringBuilder builder = new StringBuilder();
		int size = list.size();
		for (int i = 0; i < size; i++) {
			T t = list.get(i);
			builder.append(t.toString());
			if (i != size-1)
				builder.append(joinStr);
		}

		return builder.toString();
	}

	public static String charFromIntForBase(int n) {
		if (n < 10)
			return String.valueOf(n);
		else {
			return String.valueOf(ALPHABETS.charAt(n - 10));
		}
	}

	public static int intFromCharForBase(String s) {
		try {
			int n = Integer.parseInt(s);
			return n;
		} catch (NumberFormatException nfe) {
			int n = 10 + ALPHABETS.indexOf(s);
			return n;
		}
	}

	public static boolean isPalindrome(String s) {
		int len = s.length();
		for (int i = 0; i < len / 2; i++) {
			if (s.charAt(i) != s.charAt(len - 1 - i)) {
				return false;
			}
		}
		return true;
	}

}
