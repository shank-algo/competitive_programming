package solutions;

/*
 ID: shashan20
 LANG: JAVA
 PROG: milk
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class milk {
	/** **/

	public static void main(String[] args) throws Exception {
		milk main = new milk();
		/** **/
		// String path = "C:\\Users\\6008321\\git\\usaco\\src\\solutions\\";
		String path = "/Users/surabhi/git/usaco/src/solutions/";
		// String path = "/home/shank/git//usaco/src/solutions/";
		// String path = "";
		/** **/
		main.solve(path);
		System.exit(0);
	}

	public void solve(String path) throws Exception {

		String infileName = path + this.getClass().getSimpleName() + ".in";
		String outfileName = path + this.getClass().getSimpleName() + ".out";

		BufferedReader f = new BufferedReader(new FileReader(infileName));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				outfileName)));

		// READ the data
		String firstLine = f.readLine();
		String[] NM = firstLine.split(" ");
		int N = Integer.parseInt(NM[0]);
		int M = Integer.parseInt(NM[1]);

		Integer[][] farmerData = new Integer[M][2];
		for (int i = 0; i < M; i++) {
			String strPA = f.readLine();
			String[] PA = strPA.split(" ");
			farmerData[i][0] = Integer.parseInt(PA[0]);
			farmerData[i][1] = Integer.parseInt(PA[1]);
		}

		// close the input file after reading
		f.close();

		String result = actualSolve(N, M, farmerData);

		out.write(result + "\n");
		out.close();
		System.out.println("Done..");
	}

	private String actualSolve(int n, int m, Integer[][] farmerData) {

		// SOLVE the data
		String result = "";

		// sort the times[] by start times
		Arrays.sort(farmerData, new Comparator<Integer[]>() {
			@Override
			public int compare(Integer[] o1, Integer[] o2) {
				return o1[0].compareTo(o2[0]);
			}
		});
		
//		for(int i=0; i<m;i++){
//			System.out.println(farmerData[i][0]+" "+farmerData[i][1]);
//		}

		long totalBought = 0;
		long totalAmount = 0;	// in paise
		int i = 0;
		while(totalBought < n){
			int amountToBuy = (int) Math.min(n-totalBought, farmerData[i][1]);
			System.out.println(MessageFormat.format("Amount to be bought from {0}th farmer is {1}", i, amountToBuy));
			totalBought += amountToBuy;
			totalAmount += amountToBuy * farmerData[i][0];
			i++;
		}
		
		
		result = Long.toString(totalAmount);

		System.out.println(result);

		return result;
	}

}