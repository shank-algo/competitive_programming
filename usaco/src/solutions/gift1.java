package solutions;

/*
 ID: shashan20
 LANG: JAVA
 PROG: gift1
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class gift1 {
	/** **/

	public static void main(String[] args) throws Exception {
		gift1 main = new gift1();
		/** **/
		String path = "C:\\Users\\6008321\\git\\usaco\\src\\solutions\\";
		/** **/
		main.solve(path);
		System.exit(0);
	}

	public void solve(String path) throws Exception {

		String infileName = path + this.getClass().getSimpleName() + ".in";
		String outfileName = path + this.getClass().getSimpleName() + ".out";

		BufferedReader f = new BufferedReader(new FileReader(infileName));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				outfileName)));
		// String a = f.readLine();
		// String b = f.readLine();

		String strNumPersons = f.readLine();
		int numP = Integer.parseInt(strNumPersons);

		List<String> names = new ArrayList<String>();
		for (int i = 0; i < numP; i++) {
			names.add(f.readLine());
		}
		
		String line = "";
		Map<String, PersonData> data = new HashMap<String, gift1.PersonData>();
		while ((line = f.readLine()) != null) {
			// line contains the first line of the group when the control
			// reaches here
			String giverName = line;

			String secondLine = f.readLine();
			String[] secondLineParts = secondLine.split(" ");
			int maxAmountGiving = Integer.parseInt(secondLineParts[0]);
			int numGiftToPersons = Integer.parseInt(secondLineParts[1]);

			List<String> recipientNames = new ArrayList<String>();
			for (int i = 0; i < numGiftToPersons; i++) {
				recipientNames.add(f.readLine());
			}

			PersonData personData = new PersonData(maxAmountGiving,
					recipientNames);

			data.put(giverName, personData);
		}
		
		f.close();

		for (Entry<String, PersonData> entry : data.entrySet()) {
			System.out.println(entry.getKey() + " > " + entry.getValue());
		}

		for (Entry<String, PersonData> entry : data.entrySet()) {
			System.out.println("processing " + entry.getKey() + "...");
			PersonData personData = entry.getValue();
			int actualAmountGiven = personData.actualAmountGiven;
			List<String> recipientNames = personData.recipientNames;
			int numToPersons = recipientNames.size();

			int amountPerPerson = 0;
			if (numToPersons != 0) {
				amountPerPerson = actualAmountGiven / numToPersons;
			}

			System.out.println("amount given to each recipient "
					+ amountPerPerson);

			for (String receiverName : recipientNames) {
				PersonData receiverData = data.get(receiverName);
				receiverData.amountReceived += amountPerPerson;
				System.out.println(receiverName + " received "
						+ amountPerPerson
						+ " and his total received amount is now "
						+ receiverData.amountReceived);
			}

		}

		for (String name : names) {
			PersonData personData = data.get(name);
			int profit = personData.amountReceived
					- personData.actualAmountGiven;
			System.out.println("writing to out > " + name + " " + profit);
			out.write(name + " " + profit + "\n");
		}

		// if (score(a) == score(b))
		// out.write("GO\n");
		// else
		// out.write("STAY\n");
		out.close();

		System.out.println("Done..");
	}

	public int score(String a) {
		int sum = 1;
		for (int i = 0; i < a.length(); i++) {
			sum *= a.charAt(i) - 'A' + 1;
		}
		return sum % 47;
	}

	public class PersonData {
		int maxAmountGiving;
		int actualAmountGiven;
		int amountReceived;
		List<String> recipientNames;

		public PersonData(int maxAmountGiving, List<String> recipientNames) {
			super();
			this.amountReceived = 0;
			this.maxAmountGiving = maxAmountGiving;
			this.recipientNames = recipientNames;

			if (recipientNames.size() != 0) {
				int amountPerPerson = maxAmountGiving / recipientNames.size();
				this.actualAmountGiven = amountPerPerson
						* recipientNames.size();
			} else {
				this.actualAmountGiven = 0;
			}

		}

		@Override
		public String toString() {
			return "maxAmountGiving:" + maxAmountGiving + " actualAmountGiven:"
					+ actualAmountGiven + "  amountReceived:" + amountReceived
					+ "  recepients:" + recipientNames.toString();
		}
	}
}