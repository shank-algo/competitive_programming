package solutions;

/*
 ID: shashan20
 LANG: JAVA
 PROG: ride
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

public class ride {

	public static void main(String[] args) throws Exception {
		ride main = new ride();
		/** **/
		// String path = "C:\\Users\\6008321\\git\\usaco\\src\\solutions\\";
		String path = "/Users/surabhi/git/usaco/src/solutions/";
		// String path = "/home/shank/git//usaco/src/solutions/";
		// String path = "";
		/** **/
		main.solve(path);
		System.exit(0);
	}

	public void solve(String path) throws Exception {

		String infileName = path + this.getClass().getSimpleName() + ".in";
		String outfileName = path + this.getClass().getSimpleName() + ".out";

		BufferedReader f = new BufferedReader(new FileReader(infileName));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				outfileName)));
		String a = f.readLine();
		String b = f.readLine();

		f.close();

		if (score(a) == score(b))
			out.write("GO\n");
		else
			out.write("STAY\n");
		out.close();

		System.out.println("Done..");
	}

	public int score(String a) {
		int sum = 1;
		for (int i = 0; i < a.length(); i++) {
			sum *= a.charAt(i) - 'A' + 1;
		}
		return sum % 47;
	}
}