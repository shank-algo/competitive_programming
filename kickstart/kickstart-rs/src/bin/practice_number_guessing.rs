use std::io::{stdin, stdout};

#[derive(Default)]
struct Scanner {
    buffer: Vec<String>,
}

impl Scanner {
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Parse failed");
            }
            let mut input = String::new();
            stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

enum Response {
    Correct,
    TooSmall,
    TooBig,
    WrongAnswer,
}

fn main() {
    let mut scan = Scanner::default();

    let t = scan.next::<usize>();

    for caseno in 1..=t {
        let lower = scan.next::<usize>();
        let upper = scan.next::<usize>();
        let max_guesses = scan.next::<usize>();

        match solve(lower, upper, max_guesses) {
            Response::Correct => {
                continue;
            }
            Response::WrongAnswer => {
                break;
            }
            _ => unreachable!(),
        }
    }
}

fn solve(lower: usize, upper: usize, max_guesses: usize) -> Response {
    let stdin = stdin();
    let mut curr_lower = lower;
    let mut curr_upper = upper;

    for g in 0..max_guesses {
        if curr_lower > curr_upper {
            return Response::WrongAnswer;
        }

        let guess: usize = (curr_lower + curr_upper) / 2;
        println!("{}", guess);

        let mut resp = String::new();
        stdin
            .read_line(&mut resp)
            .ok()
            .expect("Failed to read response");

        match resp.as_str() {
            "CORRECT" => return Response::Correct,
            "TOO_SMALL" => {
                curr_lower = guess;
            }
            "TOO_BIG" => {
                curr_upper = guess;
            }
            _ => return Response::WrongAnswer,
        }
    }

    Response::WrongAnswer
}
