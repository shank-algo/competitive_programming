import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================

def main():
    n, k = [int(x) for x in input().strip().split()]
    d = [x for x in input().strip().split()]

    while True:
        str_n = str(n)
        if any(i in str_n for i in d):
            n += 1
        else:
            break
    
    print(n)


main()

# ==============================================================

if debug_mode:
    inf.close()
