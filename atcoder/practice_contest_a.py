import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================

def solve():
    a = int(inf.readline())
    b, c = [int(x) for x in inf.readline().split()]
    s = inf.readline()
    print(a+b+c, s)


solve()

# ==============================================================

if debug_mode:
    inf.close()
