//! Generic utility for reading data from standard input, based on [voxl's
//! stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufWriter, Write};

#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    // parse input
    let n: usize = scan.next();
    let w: i64 = scan.next();
    let mut ws: Vec<i64> = Vec::new();
    let mut vs: Vec<i64> = Vec::new();
    for _ in 0..n {
        ws.push(scan.next());
        vs.push(scan.next());
    }

    // call the actual solve function
    let result = solution(n, w, &ws, &vs);

    // print the result
    writeln!(out, "{}", result).ok();
}

fn solution(n: usize, w: i64, ws: &Vec<i64>, vs: &Vec<i64>) -> i64 {
    let mut cache: HashMap<(usize, i64), i64> = HashMap::new();
    best(n, w, ws, vs, &mut cache)
}

/// max value for first `items` cnt and `max_weight` weight
fn best(
    items: usize,
    max_weight: i64,
    ws: &Vec<i64>,
    vs: &Vec<i64>,
    cache: &mut HashMap<(usize, i64), i64>,
) -> i64 {
    if let Some(best_value) = cache.get(&(items, max_weight)) {
        return *best_value;
    }

    if items == 0 || max_weight <= 0 {
        return 0;
    }

    let best_not_including = if items > 1 && max_weight > 0 {
        best(items - 1, max_weight, ws, vs, cache)
    } else {
        0
    };

    let idx = items - 1;
    let best_including = if max_weight >= ws[idx] {
        vs[idx] + best(items - 1, max_weight - ws[idx], ws, vs, cache)
    } else {
        0
    };

    let best_value = best_not_including.max(best_including);
    cache.insert((items, max_weight), best_value);
    best_value
}

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}
