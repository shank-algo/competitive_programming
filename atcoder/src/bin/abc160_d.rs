//! Generic utility for reading data from standard input, based on [voxl's
//! stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufWriter, Write};

#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

fn bfs(g: &HashMap<usize, Vec<usize>>, from: usize) -> HashMap<usize, usize> {
    // (node, distance)
    let mut to_visit: VecDeque<(usize, usize)> = VecDeque::new();
    to_visit.push_back((from, 0));
    let mut visited: HashMap<usize, usize> = HashMap::new();
    while !to_visit.is_empty() {
        let (curr, d) = to_visit.pop_front().unwrap();
        if visited.contains_key(&curr) {
            continue;
        }
        visited.insert(curr, d);
        for &n in &g[&curr] {
            to_visit.push_back((n, d + 1));
        }
    }

    visited
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    // CODE HERE
    let n: usize = scan.next();
    let x: usize = scan.next();
    let y: usize = scan.next();

    let mut g: HashMap<usize, Vec<usize>> = HashMap::new();
    for i in 1..n {
        let inode = g.entry(i).or_insert(vec![]);
        (*inode).push(i + 1);
        let next_node = g.entry(i + 1).or_insert(vec![]);
        (*next_node).push(i);
    }
    let xnode = g.entry(x).or_insert(vec![]);
    (*xnode).push(y);
    let ynode = g.entry(y).or_insert(vec![]);
    (*ynode).push(x);

    let mut npairs: HashMap<usize, usize> = HashMap::new();
    for i in 1..n + 1 {
        let dists = bfs(&g, i);
        for (_, &dist) in &dists {
            let cnt = npairs.entry(dist).or_insert(0);
            *cnt += 1;
        }
    }

    // dbg!(&npairs);
    for i in 1..n {
        writeln!(out, "{}", npairs.get(&i).unwrap_or(&0) / 2).ok();
    }
}

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}
