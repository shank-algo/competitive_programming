//! Generic utility for reading data from standard input, based on [voxl's
//! stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufWriter, Write};

#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    let n: usize = scan.next();
    let mut abc: Vec<Vec<i64>> = vec![vec![0; 3]; n];
    for i in 0..n {
        abc[i][0] = scan.next();
        abc[i][1] = scan.next();
        abc[i][2] = scan.next();
    }

    let result = solution(n, abc);

    writeln!(out, "{}", result).ok();
}

fn solution(n: usize, abc: Vec<Vec<i64>>) -> i64 {
    let mut cache: HashMap<(usize, usize), i64> = HashMap::new();

    if n == 1 {
        return abc[0].iter().max().cloned().unwrap();
    }

    let mut best = 0;
    for using in 0..3 {
        let mut prev_best = 0;
        let others = match using {
            0 => [1, 2],
            1 => [0, 2],
            2 => [0, 1],
            _ => unreachable!(),
        };
        for &ch in others.iter() {
            prev_best = prev_best.max(solution_memo(n - 2, ch, &abc, &mut cache));
        }
        best = best.max(abc[n - 1][using] + prev_best);
    }

    best
}

fn solution_memo(
    pos: usize,
    using: usize,
    abc: &Vec<Vec<i64>>,
    cache: &mut HashMap<(usize, usize), i64>,
) -> i64 {
    if cache.contains_key(&(pos, using)) {
        return cache[&(pos, using)];
    }

    if pos == 0 {
        return abc[0][using];
    }

    let others = match using {
        0 => [1, 2],
        1 => [0, 2],
        2 => [0, 1],
        _ => unreachable!(),
    };

    let mut prev_best = 0;
    for &ch in others.iter() {
        prev_best = prev_best.max(solution_memo(pos - 1, ch, abc, cache));
    }
    cache.insert((pos, using), abc[pos][using] + prev_best);

    cache[&(pos, using)]
}

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}
