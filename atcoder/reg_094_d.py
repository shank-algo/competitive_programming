import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================

from math import sqrt


def main():
    Q = int(input().strip())
    AB = [list(map(int, input().strip().split())) for _ in range(Q)]
    for a, b in AB:
        a, b = min((a, b)), max((a, b))
        cnt = 0
        if a == b:
            cnt = 2 * (a - 1)
        elif a + 1 == b:
            cnt = 2 * (a - 1)
        else:
            ab = a * b
            c = int(sqrt(ab)) if sqrt(ab) > int(sqrt(ab)) else int(sqrt(
                ab)) - 1
            if c * (c + 1) >= ab:
                cnt = 2 * (c - 1)
            elif c * (c + 1) < ab:
                cnt = 2 * c - 1
        print(cnt)


main()

# ==============================================================

if debug_mode:
    infile.close()
