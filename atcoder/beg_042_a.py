import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================

def solve():
    abc = input()
    if abc.count("7") == 1 and abc.count("5") == 2:
        print("YES")
    else:
        print("NO")


solve()

# ==============================================================

if debug_mode:
    inf.close()
