import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================


def sieve(n):
    not_prime = set()
    primes = []
    for i in range(2, n + 1):
        if i not in not_prime:
            primes.append(i)
            for j in range(i * i, n + 1, i):
                not_prime.add(j)
    return primes


def main():
    primes = set(sieve(10**6))

    n = int(input().strip())
    for i in range(n, 10**6 + 2):
        if i in primes:
            continue
        if (i - 1) % n == 0 and (i - 1) // n >= 1:
            print((i - 1) // n)
            return


main()

# ==============================================================

if debug_mode:
    infile.close()
