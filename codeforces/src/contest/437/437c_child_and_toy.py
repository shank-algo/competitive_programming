import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================

from heapq import heappush, heappop


def main():
    n, m = list(map(int, input().strip().split()))
    v = [0] + list(map(int, input().strip().split()))
    G = {i: [] for i in range(1, n + 1)}
    C = {i: 0 for i in range(1, n + 1)}  # cost of removal of ith part
    for _ in range(m):
        a, b = list(map(int, input().strip().split()))
        G[a].append(b)
        C[a] += v[b]
        G[b].append(a)
        C[b] += v[a]

    H = []
    for i, c in C.items():
        heappush(H, (c, -v[i], i))

    S = set()  # already popped
    removed = 0  # parts removed
    cost = 0
    while removed < n - 1:
        c, vi, i = heappop(H)
        if i in S: continue
        S.add(i)
        removed += 1
        cost += c
        for u in G[i]:
            if u in S: continue
            C[u] -= v[i]
            heappush(H, (C[u], -v[u], u))

    print(cost)


main()

# ==============================================================

if debug_mode:
    infile.close()
