import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================

from collections import defaultdict, deque


def main():
    n, m = list(map(int, input().strip().split()))

    if m <= n:
        print(n - m)
        return

    # do bfs from n to m
    Q, S = deque([(n, 0)]), set()
    while Q:
        curr, clicks = Q.popleft()
        if curr in S:
            continue
        if curr == m:
            print(clicks)
            return
        S.add(curr)
        clicks += 1
        if curr > 1:
            Q.append((curr - 1, clicks))
        if curr < m:
            Q.append((2 * curr, clicks))


main()

# ==============================================================

if debug_mode:
    infile.close()
