//! Generic utility for reading data from standard input, based on [voxl's
//! stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufWriter, Write};

#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    // CODE HERE
    let t: usize = scan.next();
    for _ in 0..t {
        let n: u32 = scan.next();
        let a: Vec<u32> = (0..n).map(|_| scan.next()).collect();

        let uniques: HashSet<u32> = a.iter().cloned().collect();
        let uniques_len = uniques.len() as u32;
        let mut counts: HashMap<u32, u32> = HashMap::new();
        for &ai in &a {
            let cnt = counts.entry(ai).or_default();
            *cnt += 1;
        }
        let max_count = *counts.values().max().unwrap();

        let result: u32 = if uniques_len == max_count {
            uniques_len - 1
        } else {
            min(uniques_len, max_count)
        };
        writeln!(out, "{}", result).ok();
    }
}

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}
