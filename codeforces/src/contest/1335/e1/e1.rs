#![allow(unused_imports)]

use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufWriter, Write};

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    // CODE HERE
    let t: usize = scan.next();
    for _ in 0..t {
        let n: usize = scan.next();
        let mut a: Vec<usize> = (0..n).map(|_| scan.next()).collect();
        a.insert(0, 0);
        // prefix counts: prefix counts of value c till idx (1 indexed)
        let mut pref: Vec<Vec<i32>> = vec![vec![0; n + 2]; 201];
        // positions at which `v` occurs in `a` (1 indexed)
        let mut pos: Vec<Vec<usize>> = vec![Vec::new(); 201];
        for i in 1..n + 1 {
            let num = a[i];
            pos[num].push(i);
            for j in 1..pref.len() {
                if num == j {
                    pref[j][i + 1] = pref[j][i] + 1;
                } else {
                    pref[j][i + 1] = pref[j][i];
                }
            }
        }
        let mut maxcnt = max_count(1, n, &pref);
        for positions in pos {
            if positions.len() <= 1 {
                continue;
            }
            for i in 0..positions.len() / 2 {
                let l = positions[i];
                let r = positions[positions.len() - 1 - i];
                let cntout = (i + 1) as i32;
                let cntin = max_count(l + 1, r - 1, &pref);
                maxcnt = max(maxcnt, 2 * cntout + cntin);
            }
        }
        writeln!(out, "{}", maxcnt).ok();
    }
}

/// item having max count in the range [l; r] inclusive
fn max_count(l: usize, r: usize, pref: &Vec<Vec<i32>>) -> i32 {
    let mut m_cnt = 0;
    for v in 1..pref.len() {
        let cnt = pref[v][r + 1] - pref[v][l];
        if cnt > m_cnt {
            m_cnt = cnt;
        }
    }
    m_cnt
}

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}

// Generic utility for reading data from standard input, based on [voxl's
// stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}
