#![allow(non_snake_case)]
#![allow(unused_imports)]

use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufWriter, Write};

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}

fn solve_case<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    let n: usize = scan.next();
    let m: usize = scan.next();

    let mut grid: Vec<Vec<char>> = Vec::with_capacity(n);
    (0..n).for_each(|_| grid.push(scan.next::<String>().chars().collect()));

    // find logmn
    let k = n * m;
    let mut logmn = 0;
    let mut v = 1;
    while v <= k {
        v <<= 1;
        logmn += 1
    }
    logmn += 1;

    let mut nxt: Vec<Vec<Vec<usize>>> = vec![vec![vec![0; logmn]; m]; n];

    for r in 0..n {
        for (c, ch) in scan.next::<String>().chars().enumerate() {
            let (nr, nc) = match ch {
                'U' => (r - 1, c),
                'R' => (r, c + 1),
                'D' => (r + 1, c),
                'L' => (r, c - 1),
                _ => unimplemented!(),
            };
            nxt[r][c][0] = nr * m + nc;
        }
    }

    // find nxt[r][c][logmn] for all (r, c);
    // using binary lifting: nxt[r][c][deg] = nxt[get_rc(next[r][c][deg-1])][deg-1]
    for deg in 1..logmn {
        for r in 0..n {
            for c in 0..m {
                let (nr, nc) = get_rc(nxt[r][c][deg - 1], m);
                nxt[r][c][deg] = nxt[nr][nc][deg - 1];
            }
        }
    }

    // used is the ending locations of the robots
    let mut starts: Vec<Vec<HashSet<char>>> = vec![vec![HashSet::new(); m]; n];
    for r in 0..n {
        for c in 0..m {
            let (fr, fc) = get_rc(nxt[r][c][logmn - 1], m);
            starts[fr][fc].insert(grid[r][c]);
        }
    }
    let mut white = 0;
    let mut black = 0;
    for r in 0..n {
        for c in 0..m {
            if starts[r][c].contains(&'0') {
                black += 1;
            } else if starts[r][c].contains(&'1') {
                white += 1;
            }
        }
    }

    writeln!(out, "{} {}", black + white, black).ok();
}

fn get_rc(i: usize, m: usize) -> (usize, usize) {
    let row = i / m;
    let col = i % m;
    (row, col)
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    // CODE HERE
    let t: usize = scan.next();
    for _ in 0..t {
        solve_case(scan, out);
    }
}

// Generic utility for reading data from standard input, based on [voxl's
// stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}
