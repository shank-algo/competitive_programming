#![allow(non_snake_case)]
#![allow(unused_imports)]

use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufWriter, Write};

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}

fn solve_case<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    let n: usize = scan.next();
    let m: usize = scan.next();

    let mut grid: Vec<Vec<char>> = Vec::with_capacity(n);
    (0..n).for_each(|_| grid.push(scan.next::<String>().chars().collect()));

    let mut next: Vec<Vec<(usize, usize)>> = Vec::with_capacity(n);
    (0..n).for_each(|r| {
        next.push(
            scan.next::<String>()
                .chars()
                .enumerate()
                .map(|(c, ch)| match ch {
                    'U' => (r - 1, c),
                    'R' => (r, c + 1),
                    'D' => (r + 1, c),
                    'L' => (r, c - 1),
                    _ => unimplemented!(),
                })
                .collect(),
        )
    });

    let mut robots: Vec<(char, (usize, usize))> = (0..n * m)
        .map(|i| {
            let (r, c) = get_rc(i, m);
            let color = match grid[r][c] {
                '0' => 'b',
                '1' => 'w',
                _ => unimplemented!(),
            };
            (color, (r, c))
        })
        .collect();

    // start with a robot at each location and do nm iterations for all robots
    for _ in 0..n * m {
        for robot in robots.iter_mut() {
            let (color, (r, c)) = *robot;
            *robot = (color, next[r][c]);
        }
    }

    let mut endings: HashMap<(usize, usize), HashSet<char>> = HashMap::new();
    for &robot in &robots {
        let ending = endings.entry(robot.1).or_default();
        (*ending).insert(robot.0);
    }

    let mut white = 0;
    let mut black = 0;
    for (_, colors) in &endings {
        if colors.contains(&'b') {
            black += 1;
        } else if colors.contains(&'w') {
            white += 1
        }
    }
    writeln!(out, "{} {}", black + white, black).ok();
}

fn get_rc(i: usize, m: usize) -> (usize, usize) {
    let row = i / m;
    let col = i % m;
    (row, col)
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    // CODE HERE
    let t: usize = scan.next();
    for _ in 0..t {
        solve_case(scan, out);
    }
}

// Generic utility for reading data from standard input, based on [voxl's
// stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}
