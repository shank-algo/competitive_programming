//! Generic utility for reading data from standard input, based on [voxl's
//! stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
use std::io;
use std::str;

/// Reads white-space separated tokens one at a time.
pub struct Scanner<R> {
    reader: R,
    buffer: Vec<String>,
}

impl<R: io::BufRead> Scanner<R> {
    pub fn new(reader: R) -> Self {
        Self {
            reader,
            buffer: Vec::new(),
        }
    }

    /// Use "turbofish" syntax token::<T>() to select data type of next token.
    ///
    /// # Panics
    ///
    /// Panics if there's an I/O error or if the token cannot be parsed as T.
    pub fn token<T: str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            self.reader.read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

pub fn scanner_from_file(filename: &str) -> Scanner<io::BufReader<std::fs::File>> {
    let file = std::fs::File::open(filename).expect("Input file not found");
    Scanner::new(io::BufReader::new(file))
}

pub fn writer_to_file(filename: &str) -> io::BufWriter<std::fs::File> {
    let file = std::fs::File::create(filename).expect("Output file not found");
    io::BufWriter::new(file)
}

// START =====================================================

use std::collections::HashSet;

fn solve(s: String, keys: HashSet<char>) -> u64 {
    let mut result = 0;

    let mut cnt = 0;
    for ch in s.chars() {
        if keys.contains(&ch) {
            cnt += 1;
        } else {
            result += cnt * (cnt + 1) / 2;
            cnt = 0;
        }
    }
    result += cnt * (cnt + 1) / 2;

    result
}

fn main() {
    let (stdin, stdout) = (io::stdin(), io::stdout());
    let mut scan = Scanner::new(stdin.lock());
    // let mut scan = scanner_from_file("input/1272c_yet_another_broken_keyboard.in");
    let mut out = io::BufWriter::new(stdout.lock());
    use io::Write;

    let _n: u64 = scan.token();
    let k: u64 = scan.token();
    let s: String = scan.token();
    let mut keys: HashSet<char> = HashSet::new();
    for _ in 0..k {
        keys.insert(scan.token());
    }

    let result = solve(s, keys);
    let _ = writeln!(out, "{}", result);
}

// END =====================================================
