//! Generic utility for reading data from standard input, based on [voxl's
//! stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
use std::io;
use std::str;

/// Reads white-space separated tokens one at a time.
pub struct Scanner<R> {
    reader: R,
    buffer: Vec<String>,
}

impl<R: io::BufRead> Scanner<R> {
    pub fn new(reader: R) -> Self {
        Self {
            reader,
            buffer: Vec::new(),
        }
    }

    /// Use "turbofish" syntax token::<T>() to select data type of next token.
    ///
    /// # Panics
    ///
    /// Panics if there's an I/O error or if the token cannot be parsed as T.
    pub fn token<T: str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            self.reader.read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

/// Same API as Scanner but nearly twice as fast, using horribly unsafe dark arts
/// **REQUIRES** Rust 1.34 or higher
pub struct UnsafeScanner<R> {
    reader: R,
    buf_str: Vec<u8>,
    buf_iter: str::SplitAsciiWhitespace<'static>,
}

impl<R: io::BufRead> UnsafeScanner<R> {
    pub fn new(reader: R) -> Self {
        Self {
            reader,
            buf_str: Vec::new(),
            buf_iter: "".split_ascii_whitespace(),
        }
    }

    /// This function should be marked unsafe, but noone has time for that in a
    /// programming contest. Use at your own risk!
    pub fn token<T: str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buf_iter.next() {
                return token.parse().ok().expect("Failed parse");
            }
            self.buf_str.clear();
            self.reader
                .read_until(b'\n', &mut self.buf_str)
                .expect("Failed read");
            self.buf_iter = unsafe {
                let slice = str::from_utf8_unchecked(&self.buf_str);
                std::mem::transmute(slice.split_ascii_whitespace())
            }
        }
    }
}

pub fn scanner_from_file(filename: &str) -> Scanner<io::BufReader<std::fs::File>> {
    let file = std::fs::File::open(filename).expect("Input file not found");
    Scanner::new(io::BufReader::new(file))
}

pub fn writer_to_file(filename: &str) -> io::BufWriter<std::fs::File> {
    let file = std::fs::File::create(filename).expect("Output file not found");
    io::BufWriter::new(file)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_in_memory_io() {
        let input = "50 8".as_bytes();
        let mut scan = Scanner::new(input);
        let mut out = String::new();
        use std::fmt::Write; // needed for writeln!()

        let x = scan.token::<i32>();
        let y = scan.token::<i32>();
        writeln!(out, "Test {}", x - y).ok();

        assert_eq!(out, "Test 42\n");
    }

    #[test]
    fn test_in_memory_unsafe() {
        let input = "50 8".as_bytes();
        let mut scan = UnsafeScanner::new(input);
        let mut out = String::new();
        use std::fmt::Write; // needed for writeln!()

        let x = scan.token::<i32>();
        let y = scan.token::<i32>();
        writeln!(out, "Test {}", x - y).ok();

        assert_eq!(out, "Test 42\n");
    }

    #[test]
    fn test_compile_stdio() {
        let (stdin, stdout) = (io::stdin(), io::stdout());
        let mut scan = Scanner::new(stdin.lock());
        let mut out = io::BufWriter::new(stdout.lock());
        use io::Write; // needed for writeln!()

        if false {
            let x = scan.token::<i32>();
            let y = scan.token::<i32>();
            writeln!(out, "Test {}", x - y).ok();
        }
    }

    #[test]
    #[should_panic(expected = "Input file not found")]
    fn test_panic_file() {
        let mut scan = scanner_from_file("input_file.txt");
        let mut out = writer_to_file("output_file.txt");
        use io::Write; // needed for writeln!()

        let x = scan.token::<i32>();
        let y = scan.token::<i32>();
        writeln!(out, "Test {}", x - y).ok();
    }
}

// START =====================================================

fn solve(n: usize, a: Vec<u64>) -> u64 {
    let mut r: Vec<u64> = vec![1; n];
    let mut l: Vec<u64> = vec![1; n];

    for i in (0..n - 1).rev() {
        if a[i] < a[i + 1] {
            r[i] = r[i + 1] + 1;
        } else {
            r[i] = 1;
        }
    }
    for i in 1..n {
        if a[i] > a[i - 1] {
            l[i] = l[i - 1] + 1;
        } else {
            l[i] = 1;
        }
    }

    let mut m = l.iter().max().map(|&v| v).unwrap();
    for i in 1..n - 1 {
        if a[i - 1] < a[i + 1] {
            m = std::cmp::max(m, l[i - 1] + r[i + 1]);
        }
    }

    m
}

fn main() {
    let (stdin, stdout) = (io::stdin(), io::stdout());
    let mut scan = Scanner::new(stdin.lock());
    // let mut scan = scanner_from_file("input/1272_three_friends.in");
    let mut out = io::BufWriter::new(stdout.lock());
    use io::Write;

    let n: usize = scan.token();
    let a: Vec<u64> = (0..n).map(|_| scan.token()).collect();
    let result = solve(n, a);
    let _ = writeln!(out, "{}", result);
}

// END =====================================================

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_1272d() {
        let n = 5;
        let a = vec![1, 2, 5, 3, 4];
        assert_eq!(4, solve(n, a));
        assert_eq!(2, solve(2, vec![1, 2]));
        assert_eq!(2, solve(7, vec![6, 5, 4, 3, 2, 4, 3]));
    }
}
