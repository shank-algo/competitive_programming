//! Generic utility for reading data from standard input, based on [voxl's
//! stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
use std::io::{self, BufRead, Read};
use std::path::Path;
use std::str;
use std::{env, fs};

/// Wraps around the possible input sources (a file and the stdin)
enum Input<'a> {
    File(io::BufReader<fs::File>),
    Stdin(io::StdinLock<'a>),
}

impl<'a> Input<'a> {
    fn from_file<P: AsRef<Path>>(path: P) -> Input<'a> {
        let path = path.as_ref();
        let file = fs::File::open(path).unwrap();
        let reader = io::BufReader::new(file);
        Input::File(reader)
    }
}

impl<'a> Read for Input<'a> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        match self {
            Input::File(reader) => reader.read(buf),
            Input::Stdin(guard) => guard.read(buf),
        }
    }
}

impl<'a> BufRead for Input<'a> {
    fn fill_buf(&mut self) -> io::Result<&[u8]> {
        match self {
            Input::File(reader) => reader.fill_buf(),
            Input::Stdin(guard) => guard.fill_buf(),
        }
    }
    fn consume(&mut self, amt: usize) {
        match self {
            Input::File(reader) => reader.consume(amt),
            Input::Stdin(guard) => guard.consume(amt),
        }
    }
}

/// Reads white-space separated tokens one at a time.
pub struct Scanner<R> {
    reader: R,
    buffer: Vec<String>,
}

impl<R: io::BufRead> Scanner<R> {
    pub fn new(reader: R) -> Self {
        Self {
            reader,
            buffer: Vec::new(),
        }
    }

    /// Use "turbofish" syntax token::<T>() to select data type of next token.
    ///
    /// # Panics
    ///
    /// Panics if there's an I/O error or if the token cannot be parsed as T.
    pub fn token<T: str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            self.reader.read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

// START =====================================================

use std::collections::{HashMap, HashSet, VecDeque};

fn is_even(n: u64) -> bool {
    n % 2 == 0
}

/// returns: steps
fn bfs(
    nodes: &Vec<u64>,
    graph: &HashMap<usize, HashSet<usize>>,
    start: &Vec<usize>,
    end: &Vec<usize>,
    steps: &mut Vec<i64>,
) {
    let mut d = vec![-1; nodes.len()];

    let mut queue = VecDeque::<usize>::new();

    for i in start {
        d[*i] = 0;
        queue.push_back(*i);
    }

    while !queue.is_empty() {
        let curr = queue.pop_front().unwrap();

        for next in &graph[&curr] {
            if d[*next] == -1 {
                d[*next] = d[curr] + 1;
                queue.push_back(*next);
            }
        }
    }

    for i in end {
        if d[*i] != -1 {
            steps[*i] = d[*i];
        }
    }
}

fn reverse(graph: HashMap<usize, HashSet<usize>>) -> HashMap<usize, HashSet<usize>> {
    let mut rev: HashMap<usize, HashSet<usize>> = HashMap::new();
    for k in graph.keys() {
        rev.insert(*k, HashSet::new());
    }
    for (i, neighbours) in graph {
        for next in neighbours {
            rev.entry(next).and_modify(|adj| {
                adj.insert(i);
            });
        }
    }
    rev
}

fn solve(n: usize, a: Vec<u64>) -> Vec<i64> {
    // create a graph based on the positions you can jump from each position
    // run multi source bfs starting with odd nos. and ending at even nos.
    //      This gives the depths for even nos.
    // run multi source bfs starting with even nos. and ending at odd nos.
    //      This gives the depths for odd nos.

    // graph: idx -> next_idx(s)
    let mut graph: HashMap<usize, HashSet<usize>> = HashMap::new();
    for k in 0..n as i64 {
        let adj: HashSet<usize> = [k + a[k as usize] as i64, k - a[k as usize] as i64]
            .iter()
            .filter(|&&idx| idx >= 0 && idx <= (n - 1) as i64)
            .map(|&idx| idx as usize)
            .collect();
        graph.insert(k as usize, adj);
    }
    let rev_graph = reverse(graph);
    let mut evens: Vec<usize> = Vec::new();
    let mut odds: Vec<usize> = Vec::new();
    for i in 0..n {
        if is_even(a[i]) {
            evens.push(i);
        } else {
            odds.push(i);
        }
    }

    let mut result = vec![-1; n];
    bfs(&a, &rev_graph, &evens, &odds, &mut result);
    bfs(&a, &rev_graph, &odds, &evens, &mut result);

    result
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let stdin = io::stdin();

    let input = if args.len() == 2 {
        Input::from_file(&args[1])
    } else {
        let guard = stdin.lock();
        Input::Stdin(guard)
    };

    let mut scan = Scanner::new(input);

    let n: usize = scan.token();
    let a: Vec<u64> = (0..n).map(|_| scan.token()).collect();

    let result = solve(n, a);
    println!(
        "{}",
        result
            .into_iter()
            .map(|i| i.to_string())
            .collect::<Vec<String>>()
            .join(" ")
    );
}

// END =====================================================

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        unimplemented!();
    }
}
