//! Generic utility for reading data from standard input, based on [voxl's
//! stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::io::{self, BufWriter, Write};

#[derive(Default)]
struct Scanner {
    buffer: Vec<String>,
}

impl Scanner {
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

// checkout: https://codeforces.com/contest/1324/submission/73066694

fn main() {
    let mut scan = Scanner::default();
    let out = &mut BufWriter::new(io::stdout());

    // start solution

    let n: usize = scan.next();
    let h: usize = scan.next();
    let l: usize = scan.next();
    let r: usize = scan.next();
    let a: Vec<usize> = (0..n).map(|_| scan.next()).collect();

    let result = best(n, h, l, r, &a, n-1);

    writeln!(out, "{}", result).ok();
}

/// returns (end_hour, max_good_sleeps)
fn best(n:usize, h: usize, l: usize, r: usize, a: &[usize], i: usize) -> (usize, usize){
    if i == 0{
        if (a[0] >= l && a[0] <= r) || (a[0]-1 >=l && a[0]-1 <= r){
            return 1;
        }else{
            return 0;
        }
    }
    
    let (ph, pb) = best(n, h, l, r, &a, i-1);
    let end1 = (ph+a[i]) % h;
    let end2 = (ph+a[i]-1) % h;
    if end1 >= l && end1 <= r{
        return (end1, p + 1);
    } else if end2 >=l && end2 <= r {
        return (end2, p+1);
    }else{
        return (p;
    }
}