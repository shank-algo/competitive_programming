//! Generic utility for reading data from standard input, based on [voxl's
//! stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#[allow(unused_imports)]
use std::cmp::{max, min};
#[allow(unused_imports)]
use std::io::{self, BufWriter, Write};

#[derive(Default)]
struct Scanner {
    buffer: Vec<String>,
}
impl Scanner {
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

// https://codeforces.com/contest/1324/problem/A

fn main() {
    let mut scan = Scanner::default();
    let out = &mut BufWriter::new(io::stdout());

    // start solution

    let t: usize = scan.next();
    for _ in 0..t {
        let n: usize = scan.next();
        let a: Vec<usize> = (0..n).map(|_| scan.next()).collect();

        let m = a.iter().min().unwrap();
        let adj: Vec<_> = a.iter().map(|&h| h - m).collect();

        if adj.iter().all(|&x| x % 2 == 0) {
            writeln!(out, "YES").ok();
        } else {
            writeln!(out, "NO").ok();
        }
    }
}
