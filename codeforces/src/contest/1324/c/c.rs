//! Generic utility for reading data from standard input, based on [voxl's
//! stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::io::{self, BufWriter, Write};

#[derive(Default)]
struct Scanner {
    buffer: Vec<String>,
}

impl Scanner {
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

// https://codeforces.com/contest/1324/problem/C

fn main() {
    let mut scan = Scanner::default();
    let out = &mut BufWriter::new(io::stdout());

    // start solution

    let t: usize = scan.next();
    for _ in 0..t {
        let s: Vec<char> = scan.next::<String>().chars().collect();
        let n = s.len();

        let mut ridx: Vec<usize> = vec![0];
        ridx.extend(
            s.iter()
                .enumerate()
                .filter(|(_i, &c)| c == 'R')
                .map(|(i, _)| i + 1),
        );
        ridx.push(n + 1);

        let d = ridx
            .iter()
            .zip(ridx.iter().skip(1))
            .map(|(i, j)| j - i)
            .max()
            .unwrap();

        writeln!(out, "{}", d).ok();
    }
}
