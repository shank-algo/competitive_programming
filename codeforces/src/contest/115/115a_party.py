import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================

from collections import defaultdict


def main():
    n = int(input().strip())
    p = [None] * (n + 1)
    for i in range(1, n + 1):
        p[i] = int(input().strip())

    m = -1
    for i in range(1, n + 1):
        v = p[i]
        curr_len = 1
        curr = v
        while curr != -1:
            curr = p[curr]
            curr_len += 1
        m = max(m, curr_len)

    print(m)


main()

# ==============================================================

if debug_mode:
    inf.close()