import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================


def main():
    n, m = list(map(int, input().strip().split()))
    G = {i: [] for i in range(1, n + 1)}
    S = set()

    for _ in range(m):
        a, b = list(map(int, input().strip().split()))
        S.add(a)
        S.add(b)

    center = -1
    for u in G:
        if u in S:
            continue
        center = u
        break

    n_roads = n - 1
    print(n_roads)
    for u in G:
        if u != center:
            print(center, u)


main()

# ==============================================================

if debug_mode:
    infile.close()
