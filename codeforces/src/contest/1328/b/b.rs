//! Generic utility for reading data from standard input, based on [voxl's
//! stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::io::{self, BufWriter, Write};

#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

fn solve_case<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    let n: usize = scan.next();
    let k: usize = scan.next();

    let mut sum = 0;
    let mut curr = 1;
    loop {
        sum += curr;
        if sum >= k {
            break;
        }
        curr += 1;
    }

    // index from right
    let b1_idx = curr;
    let till_prev = curr * (curr - 1) / 2;
    let b2_idx = k - till_prev - 1;

    // index from left, zero based
    let b1_idx = n - 1 - b1_idx;
    let b2_idx = n - 1 - b2_idx;
    for i in 0..n {
        if i == b1_idx || i == b2_idx {
            write!(out, "b").ok();
        } else {
            write!(out, "a").ok();
        }
    }
    writeln!(out, "").ok();
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    // CODE HERE
    let t: usize = scan.next();
    for _ in 0..t {
        solve_case(scan, out);
    }
}

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}
