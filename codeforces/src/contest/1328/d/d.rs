//! Generic utility for reading data from standard input, based on [voxl's
//! stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::io::{self, BufWriter, Write};

#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

use std::collections::{HashMap, HashSet};

fn solve_case<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    let n: usize = scan.next();
    let t: Vec<usize> = (0..n).map(|_| scan.next()).collect();

    // if all same, 1 1 1 1 ... 1 1

    // if n is even, 1 2 1 2 1 2 ... 1 2

    // if n is odd
    //      if same type pair
    //          1 2 1 (2 2) 1 2 ... 1 2
    //      else if t[n-1] == t[0]
    //          1 2 1 2 1 2 1 ... 1 2
    //      else
    //          1 2 1 2 ......... 1 2 3

    let types: HashSet<usize> = t.iter().cloned().collect();
    if types.len() == 1 {
        writeln!(out, "1").ok();
        writeln!(out, "{}", join(vec![1; n].iter(), " ")).ok();
        return;
    }

    let colors: Vec<u8> = if n % 2 == 0 {
        [1, 2].iter().cycle().take(n).cloned().collect()
    } else {
        let mut pair: Option<(usize, usize)> = None;
        for i in 1..n {
            if t[i] == t[i - 1] {
                pair = Some((i - 1, i));
                break;
            }
        }

        match pair {
            Some((_i, j)) => {
                let mut colors: Vec<u8> = vec![0; n];
                let mut curr_color = 1;
                colors[0] = curr_color;
                for idx in 1..n {
                    if idx != j {
                        curr_color = 3 - curr_color;
                    }
                    colors[idx] = curr_color;
                }

                colors
            }
            None => {
                if t[n - 1] == t[0] {
                    [1, 2].iter().cycle().take(n).cloned().collect()
                } else {
                    [1, 2]
                        .iter()
                        .cycle()
                        .take(n - 1)
                        .chain(&[3])
                        .cloned()
                        .collect()
                }
            }
        }
    };

    let cnt = colors.iter().max().unwrap().clone();
    writeln!(out, "{}", cnt).ok();
    writeln!(out, "{}", join(colors.iter(), " ")).ok();
}

fn join<I, S, T>(data: T, sep: S) -> String
where
    I: ToString,
    S: ToString,
    T: Iterator<Item = I>,
{
    let iter = data.into_iter();
    let sep = sep.to_string();
    let sep = sep.as_str();
    iter.fold(String::new(), |mut acc, d| {
        if acc.len() > 0 {
            acc.push_str(sep);
        }
        acc.push_str(d.to_string().as_str());
        acc
    })
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    // CODE HERE
    let q: usize = scan.next();
    for _ in 0..q {
        solve_case(scan, out);
    }
}

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}
