//! Generic utility for reading data from standard input, based on [voxl's
//! stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufWriter, Write};

#[derive(Default)]
struct Scanner {
    #[allow(dead_code)]
    buffer: Vec<String>,
}

impl Scanner {
    #[allow(dead_code)]
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

const MAX: usize = std::usize::MAX;

#[derive(Debug)]
struct DFS {
    tree: Vec<Vec<usize>>,
    parent: Vec<usize>,
    depth: Vec<usize>,
    t: u64,
    tin: Vec<u64>,
    tout: Vec<u64>,
}

impl DFS {
    fn new(tree: Vec<Vec<usize>>) -> Self {
        let n = tree.len();
        Self {
            tree,
            parent: vec![MAX; n],
            depth: vec![MAX; n],
            t: 0,
            tin: vec![std::u64::MAX; n],
            tout: vec![std::u64::MAX; n],
        }
    }

    fn dfs(&mut self) {
        self.dfs_helper(0, 0, 0);
    }

    fn dfs_helper(&mut self, curr: usize, par: usize, dep: usize) {
        self.parent[curr] = par;
        self.depth[curr] = dep;
        self.tin[curr] = self.t;
        self.t += 1;
        for &next in &self.tree[curr].clone() {
            if next != par {
                self.dfs_helper(next, curr, dep + 1);
            }
        }
        self.tout[curr] = self.t;
        self.t += 1;
    }

    // whether u is an ancestor of v
    fn is_ancestor(&self, u: usize, v: usize) -> bool {
        self.tin[u] <= self.tin[v] && self.tout[u] >= self.tout[v]
    }
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    let n: usize = scan.next();
    let m: usize = scan.next();

    let mut tree: Vec<Vec<usize>> = vec![vec![]; n];
    for _ in 0..n - 1 {
        let u: usize = scan.next::<usize>() - 1;
        let v: usize = scan.next::<usize>() - 1;
        tree[u].push(v);
        tree[v].push(u);
    }

    // dbg!(&tree);

    let mut sol = DFS::new(tree);
    sol.dfs();

    // dbg!(&sol);
    for _ in 0..m {
        let k: usize = scan.next();
        let nodes: Vec<usize> = (0..k).map(|_| scan.next::<usize>() - 1).collect();

        // except root and node at the max depth, replace each node by its parent
        // now check that all the (transformed) nodes are ancestors of the deepest node
        // note that a node is its own parent

        let mut deepest_node = nodes[0];
        for &node in &nodes {
            if sol.depth[node] > sol.depth[deepest_node] {
                deepest_node = node;
            }
        }

        let possible = nodes
            .iter()
            .map(|&v| if v != deepest_node { sol.parent[v] } else { v })
            .all(|v| sol.is_ancestor(v, deepest_node));

        if possible {
            writeln!(out, "YES").ok();
        } else {
            writeln!(out, "NO").ok();
        }
    }
}

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}
