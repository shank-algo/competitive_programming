import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================


def main():
    n = int(input().strip())
    A = list(map(int, input().strip().split()))
    sA = set(A)

    last = None
    for a in A:
        if a * 2 not in sA and ((a % 3 == 0 and a // 3 not in sA) or
                                a % 3 != 0):
            last = a
            break

    curr, res, cnt = last, [last], 1
    while cnt < n:
        if curr % 2 == 0 and curr // 2 in sA:
            curr = curr // 2
        elif curr * 3 in sA:
            curr = curr * 3
        res.append(curr)
        cnt += 1

    res.reverse()

    print(" ".join(map(str, res)))


main()

# ==============================================================

if debug_mode:
    infile.close()
