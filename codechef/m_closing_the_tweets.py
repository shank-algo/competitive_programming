from collections import namedtuple, defaultdict


# 0 implies that the tweet is closed
# 1 implies that the tweet is open

Status = namedtuple('Status', 'open close')
status = Status(close=0, open=1)


def iter_range_n(count, initial_value):
    return (tuple([i, initial_value]) for i in range(1, count+1))


def init_tweets(n_tweets):
    return dict(iter_range_n(n_tweets, status.close))


def close_tweet(n, tweets_status):
    tweets_status[n] = status.close


def open_tweet(n, tweets_status):
    tweets_status[n] = status.open


def toggle(n, tweets):
    tweets[n] = status.close if tweets[n] == status.open else status.open


def strip(s):
    return s.strip()


def tweet_number(_, num):
    return int(num)


def apply_click(tweet_number, tweets_status):
    toggle(tweet_number, tweets_status)


def apply_closeall(tweets_status):
    for n in tweets_status:
        close_tweet(n, tweets_status)


def apply_command(cmd, tweets_status):
    if 'CLICK' in cmd:
        num = tweet_number(*(cmd.strip().split()))
        apply_click(num, tweets_status)
    elif 'CLOSEALL' in cmd:
        apply_closeall(tweets_status)
    else:
        raise ValueError('unparseable line > {}'.format(cmd))


def count_tweets_with_status(s, tweets_status):
    return sum(1 for t in tweets_status if tweets_status[t] == s)


def print_open_tweets_count(tweets_status):
    # print('tweets_status > {}'.format(tweets_status))
    print(count_tweets_with_status(status.open, tweets_status))


def solve(cipher):
    n_tweets, n_clicks = tuple(int(i) for i in cipher.strip().split())

    tweets_status = init_tweets(n_tweets)
    # print('initial status > {}'.format(tweets_status))

    for i in range(n_clicks):
        cmd = input()
        if cmd:
            cmd = strip(cmd)
            apply_command(cmd, tweets_status)
            print_open_tweets_count(tweets_status)
        else:
            return


if __name__ == '__main__':
    first_line = input()
    solve(first_line)
