# misc utils

from typing import Iterable
from my_types import Graph, Node

# =======================================
# GRAPH
# (dict of adjacency sets)
# =======================================


def create_graph() -> Graph:
    return {}


def add_node(graph: Graph, node: Node) -> None:
    if node not in graph: graph[node] = set()


def add_nodes(graph: Graph, nodes: Iterable[Node]) -> None:
    [add_node(graph, node) for node in nodes]


def add_edge(graph: Graph, a: Node, b: Node, directed=False) -> None:
    graph[a].add(b)
    if not directed:
        graph[b].add(a)
