# Kosaraju's algorithm for finding Strongly Connected Components

from typing import Set, Iterable, List
from my_types import Graph, Node

from utils import create_graph, add_edge, add_nodes
from dfs_topsort import dfs_topsort
from walk import walk


def transpose(G: Graph) -> Graph:
    GT = create_graph()
    add_nodes(GT, G.keys())
    for u in G:
        for v in G[u]:
            add_edge(GT, v, u, directed=True)
    return GT


def scc(G: Graph) -> List:
    seen: Set = set()
    sccs = []
    GT = transpose(G)
    u: Node
    for u in dfs_topsort(G):
        if u in seen:
            continue
        C = walk(GT, u, seen)
        seen.update(C)
        sccs.append(set(C.keys()))
    return sccs


# TEST
if __name__ == "__main__":
    from utils import add_nodes, add_edge

    graph = create_graph()
    a, b, c, d, e, f, g = range(7)
    add_nodes(graph, [a, b, c, d, e, f, g])
    add_edge(graph, a, b, directed=True)
    add_edge(graph, b, c, directed=True)
    add_edge(graph, c, a, directed=True)
    add_edge(graph, c, d, directed=True)
    add_edge(graph, d, e, directed=True)
    add_edge(graph, e, c, directed=True)
    add_edge(graph, e, f, directed=True)
    add_edge(graph, f, g, directed=True)

    # transpose or the graph
    transposed = transpose(graph)
    assert a in transposed[b]
    assert b in transposed[c]
    assert c in transposed[a]
    assert c in transposed[d]
    assert d in transposed[e]
    assert e in transposed[c]
    assert e in transposed[f]
    assert f in transposed[g]

    # strongly connected components
    comp = scc(graph)
    assert len(comp) == 3
    assert set([a, b, c, d, e]) in comp
    assert set([f]) in comp
    assert set([g]) in comp
