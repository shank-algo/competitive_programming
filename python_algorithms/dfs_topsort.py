# Topological Sorting based in DFS

from typing import Iterable, List, Set
from my_types import Graph, Node
from utils import create_graph, add_edge, add_nodes


def dfs_topsort(G: Graph) -> List[Node]:
    S: Set[Node] = set()
    finished: List[Node] = []

    def recurse(u: Node) -> None:
        if u in S:
            return
        S.add(u)
        for v in G[u]:
            recurse(v)
        finished.append(u)

    for u in G:
        recurse(u)

    finished.reverse()
    return finished


# Test
if __name__ == "__main__":
    graph = create_graph()
    a, b, c, d, e, f = range(6)
    add_nodes(graph, [a, b, c, d, e, f])
    add_edge(graph, a, b, directed=True)
    add_edge(graph, a, c, directed=True)
    add_edge(graph, b, c, directed=True)
    add_edge(graph, c, d, directed=True)
    add_edge(graph, d, e, directed=True)
    add_edge(graph, f, e, directed=True)
    add_edge(graph, d, f, directed=True)
    assert dfs_topsort(graph) == [0, 1, 2, 3, 5, 4]
    print("test passed...")
