# Types

from typing import Dict, Set, List, Iterable, TypeVar, Optional

Node = TypeVar("Node")
Graph = Dict[Node, Set[Node]]
GraphD = Dict[Node, Dict[Node, int]]  # dict of dicts representation
Predecessors = Dict[Node, Node]

Timestamp = int