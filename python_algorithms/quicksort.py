# Quicksort

from partition_and_select import partition


# [T] -> [T]
# when T: any
def quicksort(seq):
    if len(seq) <= 1: return seq
    lo, pivot, hi = partition(seq)
    return quicksort(lo) + [pivot] + quicksort(hi)


# TEST
if __name__ == "__main__":
    seq = [1, 3, 5, 2, 3]
    assert quicksort(seq) == [1, 2, 3, 3, 5]