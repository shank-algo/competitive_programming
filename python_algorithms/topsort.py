# Topological Sorting of a Dircted, Acyclic Graph

from typing import Dict, Set, TypeVar, List
from my_types import Graph, Node

from utils import create_graph, add_nodes, add_edge


def topsort(G: Graph) -> List[Node]:
    count = dict((u, 0) for u in G)  # in-degree of each node, initialized to 0
    for u in G:
        for v in G[u]:
            count[v] += 1  # count every in-edge
    Q = [u for u in G if count[u] == 0]  # valid initial nodes
    S = []  # result
    while Q:  # while we have start nodes
        u = Q.pop()  # pick one
        S.append(u)  # use it as the first of rest
        for v in G[u]:
            count[v] -= 1  # un-count all its out edges
            if count[v] == 0:  # new valid start nodes?
                Q.append(v)  # deal with them next
    return S


# Test
if __name__ == "__main__":
    graph = create_graph()
    a, b, c, d, e, f = range(6)
    add_nodes(graph, [a, b, c, d, e, f])
    add_edge(graph, a, b, directed=True)
    add_edge(graph, a, c, directed=True)
    add_edge(graph, b, c, directed=True)
    add_edge(graph, c, d, directed=True)
    add_edge(graph, d, e, directed=True)
    add_edge(graph, f, e, directed=True)
    add_edge(graph, d, f, directed=True)
    assert topsort(graph) == [0, 1, 2, 3, 5, 4]
    print("test passed...")
