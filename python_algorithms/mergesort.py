# Merge Sort


# [T] -> [T]
# when T: any
def mergesort(seq):
    mid = len(seq) // 2
    left, right = seq[:mid], seq[mid:]
    if len(left) > 1: left = mergesort(left)
    if len(right) > 1: right = mergesort(right)
    result = []
    while left and right:
        if left[-1] >= right[-1]:
            result.append(left.pop())
        else:
            result.append(right.pop())
    result.reverse()
    return (left or right) + result


# TEST
if __name__ == "__main__":
    arr = [1, 3, 6, 1, 1, 5]
    res = [1, 1, 1, 3, 5, 6]
    assert mergesort(arr) == res
