# Depth-First Search

from typing import Optional, Set, Dict, Iterable
from my_types import Graph, Node, Timestamp


# Recursive DFS
def rec_dfs(G: Graph, start: Node, S: Optional[Set] = None) -> None:
    if S is None:
        S = set()  # initialize history
    S.add(start)  # we've visited start
    for u in G[start]:  # explore neighbours
        if u in S: continue  # already visited? skip
        rec_dfs(G, u, S)  # explore recursively


# Iterative DFS
def iter_dfs(G: Graph, start: Node) -> Iterable[Node]:
    S, Q = set(), []  # visited set, queue
    Q.append(start)  # add start to the queue
    while Q:  # nodes left to visit?
        u = Q.pop()  # get one
        if u in S: continue  # already visited? skip
        S.add(u)  # we've visited it now
        Q.extend(G[u])  # schedule all neighbours
        yield u  # report u as visited


# Recursive DFS with timestamps
def dfs(
        G: Graph,
        start: Node,
        d: Dict[Node, Timestamp],  # discover times
        f: Dict[Node, Timestamp],  # finish times
        S: Set[Node] = None,  # seen nodes
        t: Timestamp = 0) -> Timestamp:
    if S is None: S = set()
    d[start] = t
    t += 1
    S.add(start)
    for u in G[start]:
        if u in S: continue
        t = dfs(G, u, d, S, t)
    f[start] = t
    t += 1
    return t