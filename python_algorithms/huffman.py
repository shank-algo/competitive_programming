# Huffman's Algorithm

from heapq import heapify, heappush, heappop
from itertools import repeat


# a tree structure to minimize the sum depth(u) x weight(u)
# over all leaves u
def huffman(seq, freq):
    n = 0
    items = list(zip(freq, repeat(n), seq))
    heapify(items)

    while len(items) > 1:
        (fa, _, ta) = heappop(items)
        (fb, _, tb) = heappop(items)
        n += 1
        combined = (fa + fb, n, [ta, tb])
        heappush(items, combined)

    return items[0][-1]


# Extracting Huffman Codes from a Huffman Tree
def codes(tree, prefix=""):
    # leaf
    if len(tree) == 1:
        yield (tree, prefix)
        return

    # branch
    # left nodes
    lprefix = prefix + "0"
    for pair in codes(tree[0], lprefix):
        yield pair
    rprefix = prefix + "1"
    for pair in codes(tree[1], rprefix):
        yield pair


if __name__ == "__main__":
    seq = "abcdefghi"
    freq = [4, 5, 6, 9, 11, 12, 15, 16, 20]
    tree = huffman(seq, freq)
    assert tree == [["i", [["a", "b"], "e"]], [["f", "g"], [["c", "d"], "h"]]]
    print("algo passed")

    generated_codes = list(codes(tree))
    assert generated_codes == [('i', '00'), ('a', '0100'), ('b', '0101'),
                               ('e', '011'), ('f', '100'), ('g', '101'),
                               ('c', '1100'), ('d', '1101'), ('h', '111')]
    print("encoding passed")
