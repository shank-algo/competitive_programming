# A General Graph Traversal Function

from typing import Iterable, Set
from my_types import Graph, Node


def traverse(G: Graph, start: Node, qtype=set) -> Iterable[Node]:
    S: Set[Node] = set()
    Q = qtype()
    Q.add(start)
    while Q:
        u = Q.pop()
        if u in S:
            continue
        S.add(u)
        for v in G[u]:
            Q.add(v)
        yield u
