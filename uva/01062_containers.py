if __name__ == "__main__":
    cno = 0
    while True:
        line = input().strip()

        if line == "end":
            break

        cno += 1

        cc = 0
        stacks = []
        for ch in line:
            new_stack_needed = True
            for s in stacks:
                if s[-1] >= ch:
                    s.append(ch)
                    new_stack_needed = False
                    break

            if new_stack_needed:
                stacks.append([ch])
                cc += 1

        print("Case {}: {}".format(cno, cc))
