if __name__ == "__main__":
    while True:
        try:
            a = [int(v) for v in input().strip().split(" ")]
            n, a = a[0], a[1:]

            if len(a) <= 2:
                print("Jolly")
                continue

            d = 0
            for i in range(1, len(a)):
                d = d | 1 << abs(a[i] - a[i - 1])

            if d == int("".join(["1"] * (n)), 2)-1:
                print("Jolly")
            else:
                print("Not jolly")

        except EOFError:
            break
