from collections import deque
from time import sleep
import sys


class Stack(object):
    """ A simple stack ADT with top as the end of a list """

    def __init__(self):
        self.items = []

    def __str__(self):
        # return ("Stack of size: %d" % len(self.items))
        return str(self.items)

    def __repr__(self):
        return str(self.items)

    def isEmpty(self):
        return len(self.items) == 0

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def top(self):
        if self.isEmpty():
            return None
        return self.items[-1]

    def __len__(self):
        return len(self.items)


def all_done(carrier, b_stn):
    if not carrier.isEmpty():
        return False
    for stn_no, b_stn_items in b_stn.items():
        if len(b_stn_items) > 0:
            return False
    return True


def lonesome_cargo(n, s, q, b_stn, count):
    """
    n -> number of countries (stations)
    s -> capacity of the carrier stack
    q -> capacity of the B station queue
    b_stn -> the starting queues of the stations
    """
    # print("n -> {}".format(n))
    # print("s -> {}".format(s))
    # print("q -> {}".format(q))
    # print("b_stn -> {}".format(b_stn))
    # print("count -> {}".format(count))

    t = 0   # time taken
    # start with the first station
    curr_stn = 1
    carrier = Stack()    # carrier stack
    remaining = count
    while True:
        # print("curr_stn -> {}".format(curr_stn))
        # print("b_stn -> {}".format(b_stn))
        # print("carrier -> {}".format(carrier))

        # try unloading items till possible
        while (not carrier.isEmpty()) and (len(b_stn[curr_stn]) < q or carrier.top() == curr_stn):
            if carrier.top() == curr_stn:
                # print("unloading item from carrier to A")
                carrier.pop()
                remaining -= 1
            else:
                # print("unloading item from carrier to B")
                b_stn[curr_stn].append(carrier.pop())
            t += 1

        # load the cargo till possible
        while len(carrier) < s and not len(b_stn[curr_stn]) == 0:
            # print("loading item onto carrier from B")
            t += 1
            carrier.push(b_stn[curr_stn].popleft())

        if remaining == 0:
            break

        # go to the next station
        curr_stn = curr_stn % n + 1
        t += 2
        # sleep(1)

    # print("time -> {}".format(t))
    print(t)


if __name__ == "__main__":
    if len(sys.argv) == 2:
        f = open(sys.argv[1])

        def input():
            return f.readline()

        def close_input():
            f.close()

    else:
        def close_input():
            pass

    sets = int(input().strip())
    for _ in range(sets):
        n, s, q = [int(i) for i in input().strip().split()]
        b_stn = {}
        cargo_count = 0
        for i in range(1, n + 1):
            ith_b_stn_line_parts = [int(i)
                                    for i in input().strip().split()[1:]]
            b_stn[i] = deque(ith_b_stn_line_parts)
            cargo_count += len(ith_b_stn_line_parts)
        lonesome_cargo(n, s, q, b_stn, cargo_count)

    close_input()
