from collections import deque

if __name__ == "__main__":
    try:
        while True:
            w = input().strip()
            t = deque(input().strip())
            s = []
            res = []
            for ch in w:
                s.append(ch)
                res.append("i")
                try:
                    # print("s", s)
                    # print("t", t)
                    while s and t.index(s[-1], 0, 1) == 0:
                        t.popleft()
                        s = s[:-1]
                        res.append("o")
                except ValueError:
                    pass

            print("[")
            print(" ".join(res))
            print("]")

    except EOFError:
        pass
