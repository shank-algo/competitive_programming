from bisect import bisect_left

if __name__ == "__main__":
    arr = []
    while True:
        try:
            v = int(input().strip())
            arr.insert(bisect_left(arr, v), v)
            l = len(arr)
            if l == 1:
                print(arr[0])
            elif l % 2 == 0:
                print((arr[l // 2] + arr[l // 2 - 1]) // 2)
            else:
                print(arr[l//2])
        except EOFError:
            break
