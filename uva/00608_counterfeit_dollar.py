if __name__ == "__main__":
    t = int(input().strip())
    for cno in range(1, t + 1):
        lines = []
        for i in range(3):
            lines.append(input().strip().split())
        true_coins = set()
        for line in lines:
            if line[2] == "even":
                true_coins.update(set(line[0]))
                true_coins.update(set(line[1]))
        for line in lines:
            if line[2] == "up" or line[2] == "down":
                for ch in line[0]:
                    if ch not in true_coins:
                        if line[2] == "up":
                            print(ch, "is the counterfeit coin and it is heavy.")
                        else:
                            print(ch, "is the counterfeit coin and it is light.")
                        break
                for ch in line[1]:
                    if ch not in true_coins:
                        if line[2] == "up":
                            print(ch, "is the counterfeit coin and it is light.")
                        else:
                            print(ch, "is the counterfeit coin and it is heavy.")
                        break
