from collections import defaultdict

if __name__ == "__main__":
    ncases = int(input().strip())
    trees = defaultdict(int)
    for cno in range(1, ncases+1):
        name = input().strip()
        trees[name] += 1

    n = len(trees)
    names = sorted(d.keys())