from sys import stdin, stdout

digits = set("2", "3", "4", "5", "6", "7", "8", "9")

if __name__ == "__main__":
    t = int(stdin.readline().strip())
    for cno in range(1, t + 1):
        cards = stdin.readline().strip().split()
        y = 0
        for step in range(3):
            card = cards[-1]
            x = int(card[0]) if card[0] in digits else 10
            y += x

        stdout.write("Case {}: {}".format(str(cno), card))
