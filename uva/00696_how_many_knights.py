def print_res(res, m, n):
    print("{} knights may be placed on a {} row {} column board.".format(
        res, m, n))

if __name__ == "__main__":
    while True:
        m, n = [int(v) for v in input().strip().split()]

        if m == n == 0:
            break

        elif m == 1 or n == 1:
            print_res(m * n, m, n)

        elif m == 2 or n == 2:
            res = 0
            om, on = m, n
            if m == 2:
                m, n = n, m
            cnt = 0
            for c in range(m):
                cnt += 1
                if cnt <= 2:
                    res += 2
                elif cnt == 4:
                    cnt = 0
            print_res(res, om, on)

        else:
            n_even = n % 2 == 0
            res = 0
            for r in range(m):
                if r % 2 == 0:
                    res += n // 2 if n_even else n // 2 + 1
                else:
                    res += n // 2
            print_res(res, m, n)
