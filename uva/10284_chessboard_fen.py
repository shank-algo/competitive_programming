from sys import stdin

black = set("pnbrqk")
white = set("PNBRQK")
bnw = black.union(white)

att = [[False] * 8 for _ in range(8)]


def reset_att():
    global att
    att = [[False] * 8 for _ in range(8)]


def pawn(r, c, color):
    if color == "B":
        att[r + 1][c] = True
    else:
        att[r - 1][c] = True


def knight(r, c):
    for i in range(8):
        for j in range(8):
            ir = abs(i - r)
            jc = abs(j - c)
            if ir == 1 and jc == 2 or ir == 2 and jc == 1:
                att[i][j] = True


def queen(r, c):
    for j in range(8):
        att[r][j] = True
    for i in range(8):
        att[i][c] = True
    for i in range(8):
        for j in range(8):
            if abs(i - r) == abs(j - c):
                att[i][j] = True


def king(r, c):
    for i in range(8):
        for j in range(8):
            if abs(i - r) == abs(j - c) == 1:
                att[i][j] = True


def rook(r, c):
    for i in range(8):
        att[i][c] = True
    for j in range(8):
        att[r][j] = True


def bishop(r, c):
    for i in range(8):
        for j in range(8):
            if abs(i - r) == abs(j - c):
                att[i][j] = True


if __name__ == "__main__":
    for line in stdin.readlines():
        reset_att()
        rows = line.strip().split("/")
        for r, row in enumerate(rows):
            c = -1
            for ch in row:
                if ch not in bnw:
                    c += int(ch)
                else:
                    c += 1
                    # process pnbrqk
                    att[r][c] = True
                    if c == "p":
                        pawn(r, c, "B")
                    elif c == "P":
                        pawn(r, c, "W")
                    elif c == "n" or c == "N":
                        knight(r, c)
                    elif c == "b" or c == "B":
                        bishop(r, c)
                    elif c == "r" or c == "R":
                        rook(r, c)
                    elif c == "q" or c == "Q":
                        queen(r, c)
                    elif c == "k" or c == "K":
                        king(r, c)
        res = 0
        for i in range(8):
            for j in range(8):
                if not att[i][j]:
                    res += 1
        print(res)
