def solve_rook():
    # max number of rooks that can be places is equal to the size of the
    # shorter edge of the board
    global res
    res = min(len(board), len(board[0]))


def solve_knight():
    global res
    n_even = n % 2 == 0
    for r in range(m):
        if r % 2 == 0:
            res += n // 2 if n_even else n // 2 + 1
        else:
            res += n // 2


def solve_queen():
    # max number of queens that can be placed is equal to shorter edge
    global res
    res = min(len(board), len(board[0]))


def solve_king():
    m = len(board)
    n = len(board[0])
    h = m // 2 if m % 2 == 0 else m // 2 + 1
    w = n // 2 if n % 2 == 0 else n // 2 + 1
    global res
    res = h * w

solver = {
    "r": solve_rook,
    "k": solve_knight,
    "Q": solve_queen,
    "K": solve_king
}

if __name__ == "__main__":
    t = int(input().strip())
    for cno in range(1, t + 1):
        case = input().strip().split(" ")
        global m, n
        piece, m, n = case[0], int(case[1]), int(case[2])
        # reset shared items
        global board
        board = [[False] * n for _ in range(m)]
        global res
        res = 0
        # sovle and print the result
        solver[piece]()
        print(res)
