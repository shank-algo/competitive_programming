def main():
    n = int(input().strip())

    if n == 1:
        print(1)
        return

    i = 2
    res = n
    while n // i >= i:
        res += n // i - i + 1
        i += 1
    print(res)


if __name__ == '__main__':
    main()
