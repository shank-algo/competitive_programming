if __name__ == '__main__':
    f = open("400.in")

    while True:
        nc = int(input().strip())
        # nc = int(f.readline().strip())

        if nc == 0:
            break

        s = input().strip()
        # s = f.readline().strip()

        c = []
        for i in range(nc):
            c.append([])

        np = 0
        l = len(s)
        while np < l:
            for i in range(nc):
                c[i].append(s[np])
                np += 1
            if np < l:
                for i in range(nc - 1, -1, -1):
                    c[i].append(s[np])
                    np += 1

        res = []
        for i in range(nc):
            res.extend(c[i])
        print(''.join(res))

    f.close()
