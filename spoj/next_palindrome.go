package main

import (
	"bufio"
	"fmt"
	"io"
	"math/big"
	"os"
	"strconv"
	"strings"
	"unicode/utf8"
)

func isPalindrome(s string) bool {
	l := len(s)
	for i := 0; i < l/2; i++ {
		if s[i] != s[l-i-1] {
			return false
		}
	}
	return true
}

func readLines(rdr io.Reader) []string {
	lines := []string{}
	r := bufio.NewReader(rdr)
	for {
		line, err := r.ReadString('\n')
		lines = append(lines, strings.TrimSpace(line))
		if err == io.EOF {
			break
		}
	}
	return lines
}

func readLinesFromReader() []string {
	return readLines(os.Stdin)
}

func readLinesFromFile() []string {
	f, _ := os.Open("5_large.in")
	defer f.Close()
	return readLines(f)
}

func main() {
	inputLines := readLinesFromReader()
	// inputLines := readLinesFromFile()
	// fmt.Println(inputLines)

	t, _ := strconv.Atoi(inputLines[0])

	_ = "breakpoint"

	for caseNo := 1; caseNo <= t; caseNo++ {
		ss := inputLines[caseNo]
		// fmt.Println(ss)

		l := utf8.RuneCountInString(ss)

		if l == 1 {
			v, _ := strconv.Atoi(ss)
			if v < 9 {
				fmt.Println(v + 1)
				continue
			}
			if v == 9 {
				fmt.Println(11)
				continue
			}
		}

		if isPalindrome(ss) {
			tmp := new(big.Int)
			fmt.Sscan(ss, tmp)
			one := big.NewInt(1)
			tmp.Add(one, tmp)
			ss = tmp.String()
			l = utf8.RuneCountInString(ss)
		}

		rs := []rune(ss)
		for j := l / 2; l > -1; j-- {
			if rs[j] != rs[l-j-1] {
				if rs[j] > rs[l-j-1] {
					// the portion to be reversed
					tmp := make([]rune, j)
					for ind := 0; ind < j; ind++ {
						tmp[ind] = rs[j-ind-1]
					}
					ss = string(rs[:l-j-1]) + string(rs[j]) + string(tmp)
					fmt.Println(ss)
					break
				} else {
					var end int
					if l%2 == 0 {
						end = l / 2
					} else {
						end = l/2 + 1
					}

					k := end - 1
					carry := 1
					newTrailing := ""
					for k >= 0 && carry == 1 {
						c := int(rs[k] - '0')
						var nc int
						if c == 9 {
							nc = 0
							carry = 1
						} else {
							nc = c + carry
							carry = 0
						}
						newTrailing = strconv.Itoa(nc) + newTrailing
						k -= 1
					}
					lenNewTrailing := len([]rune(newTrailing))
					st := string(rs[:end-lenNewTrailing]) + newTrailing
					halfLen := l / 2
					tmp := make([]rune, halfLen)
					rsst := []rune(st)
					for ind := 0; ind < l/2; ind++ {
						tmp[ind] = rsst[halfLen-ind-1]
					}
					ss = st + string(tmp)
					_ = "breakpoint"
					fmt.Println(ss)
					break
				}
			}
		}
	}
}
