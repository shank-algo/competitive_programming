if __name__ == '__main__':
    t = int(input().strip())

    alphabets = set('abcdefghijklmnopqrstuvwxyz')
    symbols = set('+-*/^')

    for cn in range(t):
        s = input().strip()

        res = []
        for c in s:
            if c == '(' or c in alphabets or c in symbols:
                res.append(c)
            elif c == ')':
                op2 = res.pop()
                sym = res.pop()
                op1 = res.pop()
                res.pop()   # '('
                res.append("{}{}{}".format(op1, op2, sym))

        print(res[0])
