def max_power(n, d):
    if n % d != 0:
        return 0

    res = 0
    while n >= d:
        if n % d == 0:
            res += 1
            n /= d
        else:
            break
    return res


def Z(n):
    twos, fives = 0, 0
    for i in range(1, n + 1):
        twos += max_power(i, 2)
        fives += max_power(i, 5)
    return min(twos, fives)


def generate_input_for_palindrome():
    from random import randrange
    s = ''
    for i in range(1000000):
        s += str(randrange(0, 10))
    print(s)


if __name__ == '__main__':
    generate_input_for_palindrome()
