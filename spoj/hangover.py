if __name__ == '__main__':
    while True:
        f = float(input().strip())
        if f == 0.00:
            break

        done = False
        c = 0.00
        n = 0
        while not done:
            n += 1
            c += 1 / (n + 1)
            if c >= f:
                print('{} card(s)'.format(n))
                done = True
