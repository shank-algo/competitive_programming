def main():
    n = int(input().strip())
    res = set()
    res.add(n)

    will_it_end = False
    while n > 1:
        if n % 2 == 0:
            n = n // 2
        else:
            n = 3 * n + 3
        if n == 1:
            will_it_end = True
        elif n in res:
            will_it_end = False
            break

    if will_it_end:
        print("TAK")
    else:
        print("NIE")


if __name__ == '__main__':
    main()
