def solve(s):
    ls = len(s)
    maxDec = [0] * ls
    maxDec[0] = 1
    if ls >= 2:
        maxDec[1] = 2 if int(s[:2]) <= 26 and s[1] != "0" else 1
    for i in range(2, ls):
        if s[i] == "0":
            maxDec[i] = maxDec[i-2]
        elif s[i - 1] == "0" or int(s[i - 1: i + 1]) > 26:
            maxDec[i] = maxDec[i - 1]
        else:
            maxDec[i] = maxDec[i - 1] + maxDec[i - 2]
    return maxDec[ls - 1]


def main():
    # f = open("acode.in")
    while True:
        line = input().strip()
        # line = f.readline().strip()
        if line == "0":
            break
        print(solve(line))
    # f.close()

if __name__ == '__main__':
    main()
