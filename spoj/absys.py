def solve(s):
    a, b, c = s.replace(
        "+", ""
    ).replace(
        "=", ""
    ).replace(
        "machula", "x"
    ).split()

    if "x" in a:
        a = int(c) - int(b)
        return "{} + {} = {}".format(a, b, c)
    elif "x" in b:
        b = str(int(c) - int(a))
        return "{} + {} = {}".format(a, b, c)
    else:
        c = str(int(a) + int(b))
        return "{} + {} = {}".format(a, b, c)


def main():
    # f = open("absys.in")
    t = int(input().strip())
    # t = int(f.readline().strip())
    for i in range(t):
        input()
        # f.readline()
        s = input().strip()
        # s = f.readline().strip()
        print(solve(s))
    # f.close()


if __name__ == '__main__':
    main()
