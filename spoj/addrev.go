package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	nCases, _ := strconv.Atoi(sc.Text())

	for cn := 0; cn < nCases; cn++ {
		sc.Scan()
		s := strings.Split(sc.Text(), " ")
		n1, _ := strconv.Atoi(s[0])
		n2, _ := strconv.Atoi(s[1])
		fmt.Println(solve(n1, n2))
	}
}

func solve(n1, n2 int) {

}
