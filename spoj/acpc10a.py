if __name__ == '__main__':
    while True:
        s = [int(v) for v in input().strip().split()]
        if s == [0, 0, 0]:
            break

        a, b, c = s
        if b == (a + c) / 2:
            d = c + (b - a)
            print("AP", d)
        else:
            d = c * c / b
            print("GP", int(d))
