def main():
    t = ri()
    for cn in range(t):
        input()
        n = ri()
        c = []
        for i in range(n):
            c.append(ri())

        if sum(c) % n == 0:
            print("YES")
        else:
            print("NO")


def ri():
    return int(input().strip())

if __name__ == '__main__':
    main()
