if __name__ == '__main__':
    t = int(input().strip())
    for cn in range(t):
        sa, sb = input().strip().split()
        a, b = int(sa), int(sb)

        if a == 0:
            print(0)
            continue

        if b == 0:
            print(1)
            continue

        n = int(str(a)[-1])

        if n in {0, 1, 5, 6}:
            print(n)

        elif n in {2, 3, 7, 8}:
            # 2, 4, 8, 6, 2, 4...
            b = b % 4 if b % 4 != 0 else 4
            print(str(n**b)[-1])

        elif n in {4, 9}:
            # 4, 6, 4, 6, ....
            b = b % 2 if b % 2 != 0 else 2
            print(str(n**b)[-1])
