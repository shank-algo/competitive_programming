from functools import wraps


def ri():
    return int(input().strip())


def memo(f):
    cache = {}

    @wraps(f)
    def wrapper(*args):
        if args not in cache:
            cache[args] = f(*args)
        return cache[args]
    return wrapper


@memo
def max_coins(n):
    # 0 -> 0
    # 1 -> 1
    # 2 -> 2
    # 3 -> 3
    # 4 -> 4
    if n <= 4:
        return n
    return max(n, max_coins(n // 2) + max_coins(n // 3) + max_coins(n // 4))


if __name__ == '__main__':
    while True:
        try:
            n = ri()
            print(max_coins(n))
        except EOFError:
            break
