if __name__ == '__main__':
    n_cases = int(input().strip())

    nos = []
    for i in range(n_cases):
        nos.append(int(input().strip()))

    m = max(nos)

    fact = {}

    fact[1] = 1
    fact[2] = 2
    for i in range(2, m + 1):
        fact[i] = i * fact[i - 1]

    for n in nos:
        print(fact[n])
